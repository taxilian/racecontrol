define([], function() {
    return {
        dateFormat: "MMM d, yyyy",
        shortTimeFormat: "HH:mm",
        timeFormat: "HH:mm:ss",
        datetimeFormat: "MMM d, yyyy HH:mm:ss"
    };
});
