define(['base/BaseView', 'underscore',
        '_tpl!./jst/Grid.jst', '_tpl!./jst/Grid_RunnerEntry.jst', '_tpl!./jst/Grid_StationHeader.jst', '_tpl!./jst/Grid_timeFormat.jst'],
        function(BaseView, _, tpl, runnerEntryTpl, stationHeaderTpl, timeFormatTpl) {

    function ordinalSuffix(intNum) {
        if (intNum == "DNF") { return "DNF"; }
        if (intNum === null || intNum === undefined) { return "N/A"; }
        return (((intNum = Math.abs(intNum) % 100) % 10 == 1 && intNum != 11) ? "st" :
                (intNum % 10 == 2 && intNum != 12) ? "nd" :
                (intNum % 10 == 3 && intNum != 13) ? "rd" : "th");
    }

    var GridView = BaseView.extend({
        event: null,
        runnerList: null,
        stationList: null,
        sortFunc: null,

        initialize: function() {
            this.em = $("#emRuler").width();
            _.bindAll(this, "autoSize");
            $(window).resize(this.autoSize);
        },

        clearData: function() {
            var self = this;
            if (self.event) {
                self.event.unbind("change", self.eventChange);
            }
            if (self.runnerList) {
                self.runnerList.unbind("change", self.rlistChange);
                self.runnerList.unbind("add", self.refresh);
            }
            if (self.stationList) {
                self.stationList.unbind("change", self.refresh);
                self.stationList.unbind("add", self.refresh);
            }
            self.event = self.stationList = self.runnerList = null;
        },

        autoSize: function() {
            if (!this.isActive()) { return; }
            var em = this.em;
            var availSize = $(window).height();

            $("#gridContainer").height(availSize - (15 * em));

            var sum = 20;
            $("#gridHeader li").each(function() { sum += $(this).outerWidth(); });
            $("#gridHolder").css("min-width", sum + "px");
        },

        setData: function(evt, rlist, slist) {
            var self = this;
            self.clearData();
            self.event = evt;
            if (evt) {
                evt.bind("change", self.eventChange, self);
            }
            self.runnerList = rlist;
            if (rlist) {
                rlist.bind("change", self.rlistChange, self);
                rlist.bind("add", self.refresh, self);
            }
            self.stationList = slist;
            if (slist) {
                slist.bind("change", self.refresh, self);
                slist.bind("add", self.refresh, self);
            }

            if (self.isActive()) {
                self.render();
            }
        },

        events: {
            "click #gridHeader li.sort":          "onSortHeaderClicked",
            "click .stationHeader a.sort":        "onStationSortHeaderClicked",
            "click .stationHeader a.edit":        "onStationEditClicked",
            "click #gridContainer ul li.name":    "onRunnerNameClicked",
            "click #gridContainer ul li.station": "onRunnerTimeClicked",
            "click button.speedy":                "onSpeedyEntryClicked",
            "click button.setStart":              "onSetStartClicked"
        },

        onSortHeaderClicked: function(evt) {
            evt.preventDefault();
            var $el = $(evt.currentTarget),
                sortType = $el.attr("class").split(/\s+/).shift();

            this.trigger("setSort", sortType);
        },

        onStationSortHeaderClicked: function(evt) {
            evt.preventDefault();
            var $el = $(evt.currentTarget).closest(".sort"),
                $st = $el.closest(".station"),
                id = $st.attr("id").split("_").pop(),
                type = $el.attr("class").substr(0,1);

            this.trigger("setSort", "s" + type + id);
        },

        setSortFunc: function(sortFunc) {
            this.sortFunc = sortFunc;
            if (this.isActive()) {
                this.render();
            }
        },

        render: function() {
            var self = this;
            var $el = $(self.el);
            $el.addClass("newgrid");
            $el.attr("id", "gridMain");
            if (self.event && self.runnerList && self.stationList) {
                var rlist = self.runnerList.getSortedList(self.sortFunc);
                $el.html(tpl({
                    stations: self.stationList,
                    runners: rlist,
                    evt: self.event,
                    stationHeader: stationHeaderTpl,
                    runnerEntry: runnerEntryTpl,
                    timeFormat: timeFormatTpl,
                    ordinalSuffix: ordinalSuffix
                }));
                self.autoSize();
            } else {
                $el.html("Loading data, please wait...");
            }
            this.$("button").button();
            //$el.html(tpl({events: self.eventList}));
            return self;
        },

        updateProgress: function(progress, total) {
            var prog = $(this.el).find("#loading_progress");
            prog.html("Loading " + progress + " of " + total);
        },

        updatePosition: function(runnerNo, position) {
            if (this.isActive()) {
                var $el = this.$("#p_" + runnerNo), place, suffix;
                if (position === null) {
                    place = "";
                    suffix = "N/A";
                } else if (position === "DNF") {
                    place = "";
                    suffix = "DNF";
                } else {
                    place = position;
                    suffix = ordinalSuffix(place);
                }
                $el.find(".place").html(place);
                $el.find(".suffix").html(suffix);
            }
        },

        refresh: function() {
            if (this.isActive()) {
                this.render();
            }
        },

        deactivate: function() {
            this.clearData();
            this.trigger("deactivate");
        },

        eventChange: function() {
        },
        rlistChange: function(runner) {
            var entryId = "#row_" + runner.id;
            this.$(entryId).html(runnerEntryTpl({
                runner: runner,
                stations: this.stationList,
                evt: this.event,
                timeFormat: timeFormatTpl,
                ordinalSuffix: ordinalSuffix
            }));
        },

        rIdFromEvent: function(evt) {
            var row = $(evt.currentTarget).closest(".participant_row");
            var rId = row.attr("id").split("_").pop();
            return rId;
        },

        onRunnerNameClicked: function(evt) {
            evt.preventDefault();
            var rId = this.rIdFromEvent(evt);

            this.trigger("click", "runnerName", rId);
        },

        onRunnerTimeClicked: function(evt) {
            evt.preventDefault();
            var elId = $(evt.currentTarget).closest(".station").attr("id").split("_"),
                runnerNo = elId[1], stationNo = elId[2];

            this.trigger("click", "runnerTime", runnerNo, stationNo);
        },

        onStationEditClicked: function(evt) {
            evt.preventDefault();
            var sId = $(evt.currentTarget).closest(".stationHeader").attr("id").split("_").pop();
            this.trigger("click", "editStation", sId);
        },

        onSpeedyEntryClicked: function(evt) {
            this.trigger("click", "speedyEntry");
        },

        onSetStartClicked: function(evt) {
            this.trigger("click", "setStart");
        }

    });

    return GridView;
});


