""" This module contains a JSON Client for invoking RIM.
"""

import mechanize
from Bag import Bag

import Logging
Logging.InitModule(globals())

class RIMClientException(Exception):
    def __init__(self, *args, **kwargs):
        super(RIMClientException, self).__init__(*args)
        for key, value in kwargs.items():
            if key == '_rim_error':
                self.code = value
            elif key == '_rim_error_msg' or key == 'msg':
                self.msg = value
            else:
                setattr(self, key, value)
class Encodings:
    JSON = "JSON"
    XML = "XML"
    FORM = "FORM"


class RIMClient(object):
    """ RIMClient 
    """
    def __init__(self, username, password, base_url, msso_token=None, msso_set_cookie=False, encode_type=Encodings.JSON):
        """ Initializes the client with the appropriate username and password.
        @param username - username
        @param password - password
        @param base_url - URL to the base of the RIM path.
        """
        self.username = username
        self.password = password
        self.base_url = base_url.strip('/')
        self.msso_token = msso_token
        self.msso_set_cookie = msso_set_cookie
        self.in_login = False
        self.encode_type = encode_type

    def Login(self):
        """ Login to the sever
        """
        self.in_login = True
        bag = self.Call('/session/login', Bag(username=self.username, password=self.password))
        self.in_login = False
        return bag

    def Logout(self):
        """ Logout of the server
        """
        return self.Call('/session/logout', Bag())

    def Call(self, method, argsBag=None, encode_type=None, msso_set_cookie=None, try_login=True, **kwargs):
        """ Make a RIM Call
        @param method - String path to the rim method to call
        @param args - Bag for the args to pass to the call
        @oparam ... - you can add any additional args you want, these will 
            override values you have set on the argsBag
        """
        #log("Call", method, argsBag)
        encode_type = encode_type or self.encode_type
        if msso_set_cookie == None:
            msso_set_cookie = self.msso_set_cookie
        # First, build the base URL
        if method is None:
            raise Exception('Must specify a method')
        if not method.startswith('/'):
            # insure the method starts with a /
            method = '/' + method

        # Prepare Request              
        req = mechanize.Request(self.base_url + method)

        # Add MSSO Token if set
        if self.msso_token:
            req.add_header('X-Authorization','MSSOToken, %s, %s' % (self.msso_token, str(msso_set_cookie)))

        # Add Packaged Args
        req.add_header('Content-Type','application/json')
        if argsBag is None:
            argsBag = Bag()
        argsBag.update(kwargs)
        req_data = None
        if argsBag:
            if encode_type == Encodings.JSON:
                req_data = argsBag.ToJSON()
            elif encode_type == Encodings.XML:
                req_data = argsBag.ToXML()
            elif encode_type == Encodings.FORM:
                req_data = argsBag.ToForm()
            else:
                # if an unsupported format assume JSON
                req_data = argsBag.ToJSON()

        if req_data:
            req.add_data(req_data)

        # Invoke 
        resp = mechanize.urlopen(req)
        auth_error = False
        resp_read = resp.read().strip()
        #log(resp_read)
        if resp.code != 401:
            if resp.code != 200:
                raise RIMClientException("Request failed with code("+resp.msg+") \n" + resp.read())
        else:
            auth_error = True
        # Unmarshall Results
        if not auth_error:
            if resp_read:
                try:
                    respBag = Bag.FromXML(resp_read) if resp.info().getsubtype() == 'xml' else Bag.FromJSON(resp_read)
                    #print respBag
                except Exception, e:
                    #print e
                    respBag = Bag(resp = resp_read, _resp_msg = repr(e))
            else:
                respBag = Bag()

        # Examine for errors, throw an exception if one occurred
        if auth_error or respBag.has_key('_rim_error') or (respBag.has_key('error') and respBag.error):
            if not (auth_error or getattr(respBag, '_rim_error', None) == 'authorization' or getattr(respBag, 'error', None)) == 1 or self.in_login:
                # Make sure to reset in_login state flag
                self.in_login = False
                raise RIMClientException(getattr(respBag, '_rim_error_msg', None) or getattr(respBag, 'msg', None) or str(respBag), **respBag)
            
            if try_login:
                # Login and Try again
                self.Login()
                respBag = self.Call(method, argsBag=argsBag, msso_set_cookie=msso_set_cookie, try_login=False, **kwargs)

        # Return the resulting Bag        
        return respBag

