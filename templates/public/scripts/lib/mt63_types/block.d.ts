import { LTypes, ControlWord } from './amp';
interface MakeBlockStandardOptions {
    keyword: LTypes.PROG | LTypes.FILE | LTypes.ID | LTypes.SIZE;
    hash: string;
    data: string;
}
interface MakeBlockControlOptions {
    keyword: LTypes.CNTL;
    hash: string;
    controlWord: ControlWord;
}
interface MakeBlockDataOptions {
    keyword: LTypes.DATA;
    hash: string;
    data: string;
    blockNum: number;
}
export declare type MakeBlockOptions = MakeBlockStandardOptions | MakeBlockControlOptions | MakeBlockDataOptions;
export declare class Block {
    keyword: LTypes;
    data: string;
    hash: string;
    controlWord?: ControlWord;
    blockNum?: number;
    constructor(keyword: LTypes, data: string);
    static MakeBlock(opts: MakeBlockOptions): Block;
    readonly checksum: string;
    readonly byteCount: number;
    getHashString(): string;
    toString(): string;
}
export {};
