export declare function encodeSync(str: string, mode?: number): string;
export declare function encode(str: string, mode?: number): Promise<string>;
export declare function decodeSync(stringToDecode: string): string;
export declare function decode(stringToDecode: string): Promise<string>;
