from ..urlDecorator import *
from ..models import *
from django.conf import settings
from django.shortcuts import redirect
from django.http import Http404
from pyzoop import zone, urlVars
from ..JSONUtils import ReturnsJSON, AcceptsJSON
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django import db
import os

Debug = True

pFields = ("firstName", "lastName", "home", "age", "sex", "team", "dnfStation", "dnfReason", "lastModifiedBy")

def errorResponse(content, status=500):
    return HttpResponse(content, status=status)

def extractWantedObj(src):
    del src["uuid"]
    del src["lastNameSx"]
    del src["firstNameSx"]
    return src

@url(r'^/events/([^/]+)/participants/?$', method="GET")
@ReturnsJSON
def participantsByEventId(request, evtId):
    try:
        resp = Participant.objects.filter(raceEvent__uuid=evtId).toArray()
        return (extractWantedObj(o) for o in resp)
    except:
        raise Http404

@url(r'^/events/([^/]+)/participants/swap$', method="POST")
@ReturnsJSON
def swapParticipantsForEventId(request, evtId):
    srcId = request.POST.get("source", False);
    destId = request.POST.get("dest", False);
    raceEvent = RaceEvent.objects.get(pk=evtId)

    print "Source", srcId, "Dest", destId

    if srcId == False or destId == False:
        return errorResponse("Expected source and renameTo")

    save = {}

    source = Participant.objects.get(raceEvent=raceEvent, bibNumber=srcId)
    if not source:
        print "No source!"
        return errorResponse("Unknown source participant")
    dest, created = Participant.objects.get_or_create(raceEvent=raceEvent, bibNumber=destId)

    # back up the old dest
    if not created:
        for f in pFields:
            save[f] = getattr(dest, f)

    # update dest with data from source
    for f in pFields:
        setattr(dest, f, getattr(source, f))

    # remove the data from source (either set it to dest or to blank)
    if not created:
        for f in pFields:
            setattr(source, f, save[f])
    else:
        source.firstName = "Unknown"
        source.lastName = "Participant"
        source.home = ""
        source.age = None
        source.sex = None
        source.team = ""
        source.dnfStation = None
        source.dnfReason = ""

    source.lastModifiedBy = settings.STATION_UUID
    dest.lastModifiedBy = settings.STATION_UUID

    source.save()
    dest.save()

    return "Success"

@url(r'^/events/([^/]+)/participants/([^/]+)/?$', method="GET")
@ReturnsJSON
def participantByEventIdAndparticipantId(request, evtId, bibNo):
    try:
        resp = Participant.objects.get(raceEvent__uuid=evtId, bibNumber=bibNo).toDict()
        return extractWantedObj(resp)
    except:
        raise Http404

@url(r'^/events/([^/]+)/participants/([^/]+)/?$', method="PUT")
@AcceptsJSON
@ReturnsJSON
def updateParticipantByEventIdAndparticipantId(request, evtId, bibNo, p):
    try:
        raceEvent = RaceEvent.objects.get(pk=evtId)
        participant, created = Participant.objects.get_or_create(raceEvent=raceEvent, bibNumber=bibNo)
        if "lastModifiedBy" not in p:
            p["lastModifiedBy"] = settings.STATION_UUID
        for f in pFields:
            if f in p:
                setattr(participant, f, p[f])

        participant.save()
        return participant.toDict()
    except:
        raise Http404

