from ..urlDecorator import *
from django.db.models import Q
from ..models import *
from django.conf import settings
from django.shortcuts import redirect
from django.http import Http404
from pyzoop import zone, urlVars
from ..JSONUtils import ReturnsJSON, AcceptsJSON
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import login, logout_then_login
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
import datetime, json, os
import os
from django import db
from soundex import soundex

Debug = True

import dateutil.parser, dateutil.tz
def parseDateTime(str):
    if not str: return None
    date = dateutil.parser.parse(str)
    return date.astimezone(dateutil.tz.tzlocal()).replace(tzinfo=None)

_TIME_FORMAT = "%m %d %Y %H %M %S %f"

def errorResponse(content, status=500):
    return HttpResponse(content, status=status)

@url(r'^/events/?$')
@ReturnsJSON
def events(request):
    evtList = RaceEvent.objects.all()
    return evtList.toArray(False)

@url(r'^/events/([a-fA-F0-9\-]+)/?$', method="GET")
@ReturnsJSON
def eventById(request, evtId):
    evtList = RaceEvent.objects.all().filter(pk=evtId)
    if len(evtList) == 0:
        raise Http404
    return evtList.toArray(False)[0]
    pass

@url(r'^/events/([a-fA-F0-9\-]+)/updates?$', method="GET")
@ReturnsJSON
def page_getAllUpdates(request, evtId):
    modified = request.GET.get("modified", "1 1 1980 0 0 0 0")
    resp = {}
    resp["lastUpdated"] = datetime.datetime.now().strftime(_TIME_FORMAT)
    updSeq = datetime.datetime.strptime(modified, _TIME_FORMAT)

    events = RaceEvent.objects.filter(modified__gt=updSeq,uuid=evtId).toArray()
    stations = Station.objects.filter(modified__gt=updSeq,raceEvent__uuid=evtId).toArray()
    participants = Participant.objects.filter(modified__gt=updSeq,raceEvent__uuid=evtId).toArray()
    entries = Entry.objects.select_related("station__stationNumber", "participant__bibNumber").filter(modified__gt=updSeq,participant__raceEvent__uuid=evtId).toArray()

    def filterFields(obj, fields):
        for f in fields:
            if f in obj:
                del obj[f]
        return obj

    resp["RaceEvents"]   = events
    resp["Stations"]     = (filterFields(s, ("uuid",)) for s in stations)
    resp["Participants"] = (filterFields(p, ("uuid", "lastNameSx", "firstNameSx")) for p in participants)
    resp["Entrys"]       = (filterFields(e, ("uuid", "participant_id", "station_id", "station_distance", "station_name")) for e in entries)
    return resp

@url(r'^/events/?$', method="POST")
@ReturnsJSON
@AcceptsJSON
def createEvent(request, evt):
    try:
        evtDate = parseDateTime(evt["date"])
        dbEvent = RaceEvent.objects.create(date=evtDate)
        dbEvent.name = evt["name"]
        dbEvent.startStation = evt["startStation"]
        dbEvent.finishStation = evt["finishStation"]
        dbEvent.save()
        db.connection._commit()
        return dbEvent.toDict()
    except:
        if Debug:
            raise
        return errorResponse("Could not create event")

@url(r'^/events/([a-fA-F0-9\-]+)/?$', method="PUT")
@ReturnsJSON
@AcceptsJSON
def updateEvent(request, evtId, evt):
    try:
        dbEvent = RaceEvent.objects.get(uuid=evt["uuid"])
        dbEvent.date = parseDateTime(evt["date"])
        dbEvent.name = evt["name"]
        dbEvent.startStation = evt["startStation"]
        dbEvent.finishStation = evt["finishStation"]
        dbEvent.save()
        db.connection._commit()
        return dbEvent.toDict()
    except RaceEvent.DoesNotExist:
        return errorResponse("Invalid event", 404)
    except:
        if Debug:
            raise
        return errorResponse("Could not update event")

@url(r'^/events/([a-fA-F0-9\-]+)/?$', method="DELETE")
@ReturnsJSON
def deleteEvent(request, evtId):
    try:
        Entry.objects.filter(participant__raceEvent__pk=evtId).delete()
        Participant.objects.filter(raceEvent__pk=evtId).delete()
        Station.objects.filter(raceEvent__pk=evtId).delete()
        RaceEvent.objects.get(uuid=evtId).delete()
        db.connection._commit()
    except RaceEvent.DoesNotExist:
        raise #Http404
    except:
        return errorResponse("Error deleting event", 500);
    return "Success";

