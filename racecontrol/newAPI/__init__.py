from ..urlDecorator import *
from django.db.models import Q
from ..models import *
from django.conf import settings
from django.shortcuts import redirect
from django.http import Http404
from pyzoop import zone, urlVars
from ..JSONUtils import ReturnsJSON, AcceptsJSON
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import login, logout_then_login
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
import datetime, json, os
import os
from django import db
from soundex import soundex

from events import urlpatterns as event_urls
from stations import urlpatterns as station_urls
from participants import urlpatterns as participant_urls
from entrys import urlpatterns as entry_urls

Debug = True

_TIME_FORMAT = "%m %d %Y %H %M %S %f"

urlpatterns = []

@url(r'^/clock$', method="GET")
@ReturnsJSON
def getClock(request):
    updTime = datetime.datetime.now().strftime(_TIME_FORMAT)
    return updTime

urlpatterns += event_urls
urlpatterns += station_urls
urlpatterns += entry_urls
urlpatterns += participant_urls
