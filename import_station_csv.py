if __name__ == "__main__":
    import settings
    from django.core.management import setup_environ
    setup_environ(settings)

from racecontrol.models import *
import sqlite3
from datetime import datetime
from Bag import Bag
import re
import csv, sys

def parseDate(dateStr, year = None):
    if not year:
        year = datetime.now().year
    
    dateStr = dateStr + ' ' + str(year)
    try:
        return datetime.strptime(dateStr, '%H:%M:%S %d %b %Y')
    except:
        return None

def import_file(raceEventId, stationNo, filename):
    if not raceEventId:
        print "Enter the race event id:",
        raceEventId = raw_imput("> ")
    raceEvent = RaceEvent.objects.get(uuid=raceEventId)
    station = Station.objects.filter(raceEvent=raceEvent, stationNumber=stationNo).get()
    if not filename:
        print "Enter name of the csv file to process:",
        filename = raw_input("> ")
    try:
        csvFile = open(filename, 'rb')
        freader = csv.reader(csvFile)

        for line in freader:
            if not line[1]:
                del line[1]
            n = line[0]
            bib = line[1]
            timeIn = parseDate(line[2])
            timeOut = parseDate(line[3])

            print "Bib", bib, "timein", timeIn, "timeout", timeOut
            participant = Participant.objects.filter(raceEvent=raceEvent, bibNumber=bib).get()
            if len(line) > 4:
                participant.dnfStation = stationNo
                participant.dnfReason = line[4]
                print 'dnf', participant.dnfStation, participant.dnfReason
            entry, created = Entry.objects.get_or_create(station=station, participant=participant)
            entry.time_in = timeIn
            entry.time_out = timeOut
            entry.save()
        csvFile.close()
    except Exception, ex:
        print "Failed. we are all going to die.", ex

if __name__ == "__main__":
    evtId = sys.argv[1] if len(sys.argv) > 1 else None
    stationNo = sys.argv[2] if len(sys.argv) > 2 else None
    fname = sys.argv[3] if len(sys.argv) > 3 else None
    import_file(evtId, stationNo, fname)
