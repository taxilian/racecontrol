define(["backbone", "jquery", "underscore", "./GridView", "./GridSort",
       "dialogs/AddEditParticipantPresenter", "dialogs/AddEditEntryPresenter", "dialogs/AddEditStationPresenter", "dialogs/SpeedyEntryPresenter",
       "models/RaceEvent", "models/Participant", "models/Station", "models/Updater", "DateManager", "dialogs/StartTimePresenter"],
function(Backbone, $, _, DefaultView, GridSort, AddEditParticipant, AddEditEntry, AddEditStation, SpeedyEntry,
         RaceEvent, Participant, Station, Updater, DateManager, StartTime) {

    var updateInterval = 5000;

    var updateId = -1;

    var forceRecalcStats = function forceRecalcStats(self) {
        var waitDfd = self.rlist.calculateResults();
        $.when(waitDfd).done(function() {
            self.calculateStationStats();
        });
    };
    var recalcStats = _.debounce(forceRecalcStats, 20000);

    var GridPresenter = Backbone.Router.extend({
        currentSort: null,
        sortDesc: false,
        initialize: function(options) {
            var self = this;
            self.view = new DefaultView();

            recalcStats = _.bind(recalcStats, null, self);
            forceRecalcStats = _.bind(forceRecalcStats, null, self);
            self.view.bind("setSort", self.setSort, self);
            self.view.bind("click", self.onSomethingClicked, self);
            self.view.bind("deactivate", self.stopUpdate, self);
            self.view.bind("recalc", recalcStats);
            self.view.bind("recalcNow", forceRecalcStats);
            self.setSort("num", false);
            _.bindAll(this, "update");
        },

        present: function(container, eventId) {
            var self = this;
            self.stopUpdate();

            var eventDfd = RaceEvent.obj.fetchById(eventId);
            var runnerListDfd = Participant.list.fetchAll(eventId);
            var stationListDfd = Station.list.fetchAll(eventId);
            var loaded = false;
            DateManager.setEvent(eventId);

            $.when(eventDfd, runnerListDfd, stationListDfd)
                .done(function(evt, rlist, slist) {
                self.slist = slist;
                self.setRunnerList(rlist);
                self.event = evt;
                self.view.setData(evt, rlist, slist);
                loaded = true;
                //var dfds = rlist.map(function(runner) {
                    //return runner.getEntrys();
                //});
                var dfd = rlist.getEntrys();
                rlist.bind("entryDownload", self.updateProgress, self);
                $.when(dfd).done(function() {
                    self.onDataLoaded();
                });
            });
            if (!loaded) {
                self.view.clearData();
            }
            container.setView(self.view);
        },

        setRunnerList: function(rlist) {
            var self = this;
            if (self.rlist != rlist && self.rlist) {
                self.rlist.unbind("change", self.onRunnerChange);
            }
            self.rlist = rlist;
            if (rlist) {
                rlist.bind("change", self.onRunnerChange, self);
            }
        },

        onRunnerChange: function(runner, entry) {
            var self = this;
            if (entry && entry.collection && entry.has("station_stationNumber")) {
                var waitDfd = null;
                recalcStats();
            }
        },

        updateProgress: function(progress, total) {
            var self = this;
            this.view.updateProgress(progress, total);
            if(progress == total){
                self.rlist.unbind("entryDownload");
            }
        },

        onDataLoaded: function() {
            var self = this;
            self.rlist.calculateResults().done(function() {
                self.calculateStationStats();
                self.updater = new Updater({eventId: self.event.id});
                self.update();
            });
        },

        stopUpdate: function() {
            try { clearTimeout(updateId); } catch (e) {}
        },

        update: function() {
            this.updater.getUpdates();

            this.stopUpdate();
            updateId = setTimeout(this.update, updateInterval);
        },

        setSort: function(sortId) {
            var self = this;
            if (sortId == self.currentSort) {
                self.sortDesc = !self.sortDesc;
            } else {
                self.currentSort = sortId;
            }
            self.view.setSortFunc(GridSort(sortId, self.sortDesc));
        },

        calculateStationStats: function() {
            console.log("station stats");
            var self = this;
            self.slist.each(function(station) {
                var seen = 0;
                var missed = 0;
                var expected = 0;
                self.rlist.each(function(runner) {
                    seen += runner.wasSeenAt(station.id) ? 1 : 0;
                    missed += runner.wasMissedAt(station.id) ? 1 : 0;
                    expected += runner.isExpectedAt(station.id) ? 1 : 0;
                });
                var thru = seen + missed;
                station.stats = {
                    seen: seen,
                    missed: missed,
                    expected: expected,
                    thru: thru,
                    total: expected + thru
                };
            });
            self.slist.trigger("change");
        },

        //////////////////
        // CRUD methods //
        //////////////////

        onSomethingClicked: function(type, p1, p2) {
            var self = this, evtId = self.event.id;
            if (!self.view.isActive()) {
                return;  // Prevent it from opening anything if the grid isn't actually open, such as w/ hotkeys
            }
            switch(type) {
                case "runnerName":
                    AddEditParticipant.showParticipant(evtId, p1);
                    break;
                case "runnerTime":
                    AddEditEntry.showEntry(evtId, p1, p2);
                    break;
                case "editStation":
                    AddEditStation.showStation(evtId, p1);
                    break;
                case "speedyEntry":
                    SpeedyEntry.showDialog(evtId);
                    break;
                case "setStart":
                    StartTime.showStartTime(evtId);
                    break;
            }
        }
    });

    return new GridPresenter();
});



