var EventMainPage = RB.LWClass();
_p = EventMainPage.prototype;

_p.initialize = function(evtId) {
    var self = this;
    this.seconds = 5;
    this.updateTimerId = -1;
    this.evtId = evtId;
    this.rc = new RaceControl();
    this.rc.getRaceEvent(evtId, this.$("raceEventLoaded"));
    showLoading();
    this.dateField = null;
    this.sort = "num";
    this.desc = true;
    this.sortFunc = this.makeSortFunction("num", this.desc);
    this.em = $("#emRuler").width();

    this.counter = null;

    $("#aSpeedyEntry").click(function(evt) {
        evt.preventDefault();
        self.event.$("speedyEntry")({onSave:function() {
            self.event.updateNow();
        }});
    });
    $("#aSearchReplace").click(function(evt) {
        evt.preventDefault();
        self.event.findAndReplace();
    });

    $(window).resize(this.$("resizeDiv"));
    this.resizeDiv();
};

_p.resizeDiv = function(evt) {
    var em = this.em;
    var availSize = $(window).height();

    $("#gridContainer").height(availSize - (17 * em));
}

_p.raceEventLoaded = function(event) {
    this.event = event;
    this.counter = new RunnerCounter(this.event.stationList);
    this.updateStations();
    this.initGrid();
    this.updateStationCounts();
    this.updatePositions();
    this.event.$("watchForUpdates")(this.$("updatesLoaded"), this.seconds);

    this.dateField.datepicker("setDate", this.event.date.value);
    this.dateSelect(this.event.date.$("toDateString")(), this.dateField);
    hideLoading();
};

_p.updatesLoaded = function updatesLoaded(data) {
    if (data.stations) {
        var foundNew = this.updateStations();
        this.counter.setStationList(this.event.stationList);

        if (!foundNew) {
            return;
        }
        for (var n in this.event.participantList) {
            var rNo = this.event.participantList[n];
            var p = this.event.participants[rNo];
            this.updateParticipant(p);
        }
        this.updateStationCounts();
        return; // This updates the participants too
    }
    this.updateGrid(data);
};

_p.formatPlace = function formatPlace(num) {
    var suffix = "th";

    switch(num % 100) {
    case 11:
    case 12:
    case 13:
        suffix = "th";
        break;
    default:
        switch(num % 10) {
        case 1:
            suffix = "st";
            break;
        case 2:
            suffix = "nd";
            break;
        case 3:
            suffix = "rd";
            break;
        default:
            suffix = "th";
            break;
        }
    }
    return '<span class="place">%s</span>%s'.format(num, suffix);
}

_p.updatePositions = function updatePositions() {
    var runnerList = [];
    var pList = this.event.participantList;
    for (var i = 0; i < pList.length; ++i) {
        var bibNo = pList[i];
        var droppedAt = this.counter.droppedAt[bibNo];
        if (!droppedAt && droppedAt !== 0) {
            runnerList.push(bibNo);
        } else {
            $("#p_%s".format(bibNo)).html("DNF").addClass("disabled");
            $("#row_%s .num, #row_%s .name".format(bibNo, bibNo)).addClass("disabled");
        }
    }

    var fStation = this.event.finishStation.value;
    this.sortFunc = this.makeSortFunction("si%s".format(fStation), false);
    runnerList.sort(this.sortFunc);

    var prevTime = null;
    var prevSeen;
    var curPos = 0;
    var realPos = 0;
    for (var i = 0; i < runnerList.length; ++i) {
        ++realPos;
        var bibNo = runnerList[i];
        var lastSeenAt = this.counter.lastSeenAt[bibNo];
        var ntime = 99999999999999999999;
        var time = this.getTime(bibNo, "o", lastSeenAt);
        if (time == ntime) {
            time = this.getTime(bibNo, "o", lastSeenAt);
        }
        if (lastSeenAt != prevSeen || prevTime != time) {
            prevTime = time;
            prevSeen = lastSeenAt;
            curPos = realPos;
        }
        $("#p_%s".format(bibNo)).html("%s".format(this.formatPlace(curPos))).removeClass("disabled").attr("pos", curPos);
    }
};

_p.setSort = function setSort(sort) {
    if (this.sort == sort) {
        this.desc = !this.desc;
    } else {
        this.sort = sort;
        this.desc = false;
    }
    this.sortGrid(this.sort, this.desc);
};

_p.updateStationCounts = function updateStationCounts() {
    for (var i = 0; i < this.event.stationList.length; i++) {
        var sNo = this.event.stationList[i];
        $("#%s_thru".format(sNo)).html(this.counter.countPassedThruStation(sNo));
        $("#%s_expected".format(sNo)).html(this.counter.countExpectedAtStation(sNo));
        $("#%s_seen".format(sNo)).html(this.counter.countSeenAtStation(sNo));
        $("#%s_missed".format(sNo)).html(this.counter.countMissedAtStation(sNo));
        $("#started_runners").html(this.counter.countStartedRunners());
        $("#dropped_runners").html(this.counter.countDroppedRunners());
        $("#current_runners").html(this.counter.countCurrentRunners());
    }
};

_p.updateStations = function updateStations() {
    var self = this;
    var foundNew = false;
    // first scan to see if there are new items
    for (var n in this.event.stationList) {
        var sNo = this.event.stationList[n];
        if (!this.event.stations[sNo].uuid) {
            continue;
        }
        if (!$("#s_%s".format(sNo)).length) {
            foundNew = true;
        }
    }
    var newheader = $("<ul></ul>");
    newheader.append('<li class="num">#</li>');
    newheader.append('<li class="name">Name</li>');
    newheader.append('<li class="position">Position</li>');
    newheader.find(".num").click(function() {
        self.setSort("num");
    });
    newheader.find(".name").click(function() {
        self.setSort("name");
    });
    newheader.find(".position").click(function() {
        self.setSort("position");
    });

    for (var n in this.event.stationList) {
        var sNo = this.event.stationList[n];
        var station = this.event.stations[sNo];
        var entry = $('<li class="station stationHeader" id="s_%s">%s: %s...</li>'.format(sNo, sNo, station.name.value.substring(0, 6)));
        var infoBox = $('<div class="info ui-state-default ui-corner-all"><div class="full">%s: %s</div></div>'.format(sNo, station.name.value))
        infoBox.append('<div class="leftHalf smaller">Thru: <span id="%s_thru"></span></div><div class="rightHalf smaller">Expected: <span id="%s_expected"></span></div>'.format(sNo, sNo));
        infoBox.append('<div class="leftHalf smaller">Seen: <span id="%s_seen"></span></div><div class="rightHalf smaller">Missed: <span id="%s_missed"></span></div>'.format(sNo, sNo));
        infoBox.append('<div class="full smaller"><a href="#" type="edit">edit</a></div>');
        if (sNo == this.event.startStation.value) {
            infoBox.append('<div class="full smaller"><a href="#" type="out">time out</a></div>');
        }
        else if (sNo == this.event.finishStation.value) {
            infoBox.append('<div class="full smaller"><a href="#" type="in">time in</a></div>');
        }
        else {
            infoBox.append('<div class="leftHalf smaller"><a href="#" type="in">time in</a><br><span class=".timeIn"></span></div><div class="rightHalf smaller"><a href="#" type="out">time out</a></div>');
        }
        entry.prepend(infoBox);
        entry.find("a").click(function(evt) {
            var num = $(evt.currentTarget).parents("li").attr("id").substring(2);
            var type = $(evt.currentTarget).attr("type");
            if (type == "edit") {
                self.event.stations[num].$("edit")(self.event.$("updateNow"));
                return;
            }
            var t = type == "in" ? "i" : "o";

            num = parseInt(num);
            sort = "s%s%s".format(t, num);
            self.setSort(sort);
        });

        newheader.append(entry);
    }
    newheader.find("li:last").addClass("last");

    var header = $("#gridHeader");
    header.find("ul").remove();
    header.append(newheader);

    var num = 2.5 + 2.0 + 11.5 + (6.5 * this.event.stationList.length);
    $("#gridHeader,#gridContainer").css("min-width", "%sem".format(num));

    return foundNew;
};

_p.updateGrid = function updateGrid(updates) {
    var self = this;
    var dataUpdate = false;

    for (var i in updates.participants) {
        var p = this.event.participants[updates.participants[i].bibNumber];
        this.updateParticipant(p);
        this.updateStationCounts();
        dataUpdate = true;
    }

    for (var i in updates.entries) {
        var p = this.event.participants[updates.entries[i].participant_bibNumber];
        this.updateParticipant(p)
        this.updateStationCounts();
        dataUpdate = true;
    }

    if (dataUpdate) {
        this.updatePositions();
    }
};

_p.updateParticipant = function updateParticipant(participant) {
    var p = participant;
    var entry = $("#row_%s".format(p.bibNumber.value));
    if (entry.length) {
        this.updateRow(p);
    } else {
        entry = $( this.createRow(p) );
        var before = RB.findEntryPlace($("#gridContainer ul"), this.sortFunc, entry);
        if (before) {
            entry.insertBefore(before);
        } else {
            $("#gridContainer").append(entry);
        }
    }
    this.counter.setDroppedAt(p.bibNumber.value, p.dnfStation.value);
    if (p.dnfStation.value !== null) {
        var boxes = entry.find("li.station").get();
        for (var i = 0; i < boxes.length; i++) {
            var box = $(boxes[i]);
            var sNo = parseInt(box.attr("sNo"));
            if (sNo == p.dnfStation.value)  {
                box.html("DROPPED");
            }
            if (sNo >= p.dnfStation.value)  {
                box.addClass("dropped");
            }
        }
    } else {
        entry.find("li").removeClass("dropped");
    }
};

_p.updateRow = function updatePartRow(participant) {
    var p = participant;
    var rNo = p.bibNumber.value;

    var row = $("#row_%s".format(rNo));
    row.find(".name").html("%s, %s".format(p.lastName.value, p.firstName.value));

    for (var n in this.event.stationList) {
        var sNo = this.event.stationList[n];

        var find = $("#l_%s_%s".format(rNo, sNo));
        if (find.length) {
            find.html(this.getEntryContents(p, this.event.stations[sNo]));
        } else {
            var compare = function(a,b) {
                var snA = parseInt($(a).attr("sNo"));
                var snB = parseInt(b.attr("sNo"));
                return (snA > snB) ? 1 : (snA < snB) ? -1 : 0;
            }
            var entry = $( this.createEntryItem(rNo, sNo, p, this.event.stations[sNo]) )
            var before = RB.findEntryPlace(row.find("li.station"), compare, entry);
            if (before) {
                entry.insertBefore(before);
            } else {
                row.append(entry);
            }
        }
    }
    row.find("li").removeClass("last");
    row.find("li:last").addClass("last");
};

_p.getEntryContents = function getEntryContents(participant, station) {
    var p = participant;
    var rNo = participant.bibNumber.value;
    var sNo = station.stationNumber.value;

    var tIn = "__:__";
    var tInClass = "inactive";
    var tInDisplay = station.stationNumber.value != this.event.startStation.value;
    var tOut = "__:__";
    var tOutClass = "inactive";
    var tOutDisplay = station.stationNumber.value != this.event.finishStation.value;
    var entry = p.entrys[sNo];
    if (entry) {
        this.counter.addEntry(entry);
        tIn = entry.time_in.$("toString")("HH:mm") || tIn;
        if ( tIn.substring(0,1) != "_" ) {
            tInClass = "";
        }
        tOut = entry.time_out.$("toString")("HH:mm") || tOut;
        if ( tOut.substring(0,1) != "_" ) {
            tOutClass = "";
        }
    }
    var tInString = tInDisplay ? '<span class="%s">%s</span>'.format(tInClass, tIn) : "";
    var tOutString = tOutDisplay ? '<span class="%s">%s</span>'.format(tOutClass, tOut) : "";
    var arrowString = tInString && tOutString ? "->" : "";
    return "%s%s%s".format(tInString, arrowString, tOutString)
};

_p.createEntryItem = function(rNo, sNo, participant, station, entry) {
    return '<li class="station" rNo="%s" sNo="%s" id="l_%s_%s">%s</li>'.format(rNo, sNo, rNo, sNo, this.getEntryContents(participant, station) );
};

_p.createPositionItem = function(rNo, position) {
    return '<li class="position" rNo="%s" id="p_%s">%s</li>'.format(rNo, rNo, position);
};

_p.createRow = function makeRow(participant) {
    var p = participant;
    var rNo = p.bibNumber.value;
    if (!p.uuid.value) {
        return "";
    }
    tpl = [
        ['<ul rNo="%s" id="row_%s">', ["bibNumber", "bibNumber"]],
        ['<li class="num" rNp="%s">%s</li>', ["bibNumber", "bibNumber"]],
        ['<li class="name" rNo="%s">%s, %s</li>', ["bibNumber", "lastName", "firstName"]],
        [ this.createPositionItem(rNo, "-") ],
    ];

    for (var n in this.event.stationList) {
        var sNo = this.event.stationList[n];

        tpl.push(
            [ this.createEntryItem(rNo, sNo, p, this.event.stations[sNo]) ]
        );
    }

    tpl.push(["</ul>"]);
    var row = $(RB.processTemplate(tpl, [p]));
    this.applyClickFuncs(row);
    row.find("li").removeClass("last");
    row.find("li:last").addClass("last");
    return row;
};

_p.initGrid = function initGrid() {
    var self = this;
    var stations = this.event.stations;
    var rows = [];
    $("#gridContainer ul").remove();
    for (var i in this.event.participantList) {
        var rNo = this.event.participantList[i];
        var p = this.event.participants[rNo];

        this.updateParticipant(p);
    }
    var grid = $("#gridContainer");
    for (var i in rows) {
        grid.append(rows[i]);
    }
    var lis = $("#gridContainer ul");
    this.applyClickFuncs(lis);
};

_p.applyClickFuncs = function applyClickFuncs(range) {
    var self = this;
    range.find("li.name, li.num").unbind("click");
    range.find("li.name, li.num").click(function(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        var cur = $(evt.currentTarget);
        var rNo = cur.attr("rNo");
        self.event.participants[rNo].$("edit")(self.event.$("updateNow"));
    });
    range.find("li.station").unbind("click");
    range.find("li.station").click(function(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        var cur = $(evt.currentTarget);
        var rNo = cur.attr("rNo");
        var sNo = cur.attr("sNo");
        var p = self.event.participants[rNo];
        if (p.entrys[sNo]) {
            p.entrys[sNo].$("edit")(self.event.$("updateNow"));
        } else {
            var entry = p.newEntry(sNo);
            entry.$("edit")(self.event.$("updateNow"));
        }
    });
};

_p.dateSelect = function dateSelect(dateText, inst) {
    var tmp = Date.parse(dateText);
    if (tmp != null) {
        RB.DbFields.DateTimeField.setDefaultDate(tmp);
    }
};

_p.setDefaultDateField = function setDefaultDateField(elem) {
    this.dateField = elem;
    this.dateField.datepicker({dateFormat: "MMM d, yyyy", onSelect: this.dateSelect});
};

_p.sortGrid = function sortGrid(target, desc) {
    var self = this;
    var mylist = $("#gridContainer");
    var list = $("#gridContainer ul").get();

    this.sortFunc = this.makeSortFunction(target, desc);
    list.sort(this.sortFunc);
    $.each(list, function(idx, itm) {
        mylist.append(itm);
    });
};

_p.getTime = function getTime(bibNo, type, sNew) {
    var p = this.event.participants[bibNo];
    var dropped = this.counter.droppedAt[bibNo];
    if (dropped === sNew) {
        return 0;
    } else if (dropped || dropped == 0) {
        return 99999999999999999999;
    }
    if (!p.entrys[sNew]) {
        return 99999999999999999999;
    }
    switch(type) {
        case "i":
            if (p.entrys[sNew].time_in.value) {
                return p.entrys[sNew].time_in.value.getTime();
            } else if (p.entrys[sNew].time_out.value) {
                return p.entrys[sNew].time_out.value.getTime();
            } else return 99999999999999999999;
            break;
        case "o":
            if (p.entrys[sNew].time_out.value) {
                return p.entrys[sNew].time_out.value.getTime();
            } else if (p.entrys[sNew].time_in.value) {
                return p.entrys[sNew].time_in.value.getTime();
            } else return 99999999999999999999;
            break;
        default:
            return 99999999999999999999;
    };
}

_p.makeSortFunction = function getSortFn(target, desc) {
    var self = this;
    var sort_desc = false;
    if (desc) sort_desc = true;

    if (target == "num" || target == "name") {
        var getVal = function(src) {
            if (target == "num") {
                return parseInt($(src).attr("rNo"));
            } else if (target == "name") {
                var p = self.event.participants[$(src).attr("rNo")];
                return "%s%s".format(p.lastName.value, p.firstName.value);
            }
                //return $(src).attr("pos");
            //}
        };
        var sortFunc = function sortFuncNumName(a, b) {
            var compA = getVal(a);
            var compB = getVal(b);
            var ret = (compA < compB) ? -1 : (compA > compB) ? 1 : 0;
            if (sort_desc) {
                ret *= -1;
            }
            return ret;
        };
        return sortFunc;
    } else if (target == "position") {

        var sortFunc = function sortFuncPos(a, b) {
            var compA = $(a).find(".position").attr("pos");
            var compB = $(b).find(".position").attr("pos");
            var ret;
            compA = Number(compA);
            if (isNaN(compA)) {
                compA = 9999999999999;
            }
            compB = Number(compB);
            if (isNaN(compB)) {
                compB = 9999999999999;
            }
            ret = (compA < compB) ? -1 : (compA != compB) ? 1 : 0;
            if (sort_desc) {
                ret *= -1;
            }
            return ret;
        };
        return sortFunc;

    }

    if (target.substring(0,1) == "s") {
        var type = target.substring(1,2);
        var sNo = parseInt(target.substring(2));

        var sortFunc = function sortFuncS(a, b) {
            var ba = $(a).attr("rNo");
            if (ba == undefined) {
                ba = a;
            }
            var bb = $(b).attr("rNo");
            if (bb == undefined) {
                bb = b;
            }
            var ta = self.getTime(ba, type, sNo);
            var tb = self.getTime(bb, type, sNo);

            if (ta > tb) return 1;
            else if (ta < tb) return -1;
            else { // if they are equal (even if they are null)

                var seenA = self.counter.lastSeenAt[ba];
                var seenB = self.counter.lastSeenAt[bb];
                if (seenA < seenB) return 1;
                else if (seenA > seenB) return -1;
                else {
                    for (var i = self.event.stationList.indexOf(sNo) - 1; i >= 0; i--) {
                        var sNew = self.event.stationList[i];
                        var ta = self.getTime(ba, type, sNew);
                        var tb = self.getTime(bb, type, sNew);
                        if (ta > tb) return 1;
                        if (ta < tb) return -1;
                    }
                    return 0;
                }
            }
        }
        return function(a,b) { return sort_desc ? sortFunc(a,b) * -1 : sortFunc(a,b); };
    }

}
delete _p;
