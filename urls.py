from django.conf.urls.defaults import *
from racecontrol.views import *
from settings import *
import os

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Example:
    # (r'^racecontrol5/', include('racecontrol5.foo.urls')),

    # Uncomment the admin/doc line below and add 'django.contrib.admindocs'
    # to INSTALLED_APPS to enable admin documentation:
    # (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    (r'^admin/', include(admin.site.urls)),

    (r'^public/(?P<path>.*)$', 'django.views.static.serve',
        {"document_root": os.path.join(PROJECT_PATH, "templates", "public")}
        ),
    (r'^newAPI', include('racecontrol.newAPI')),
    url(r'^(?P<path>.*)', "racecontrol.views.pageHandler", name="rc"),
)
