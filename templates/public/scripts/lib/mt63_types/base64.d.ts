declare function encode64(input: string): string;
declare function decode64(input: string): string;
export { encode64 as encode, decode64 as decode };
