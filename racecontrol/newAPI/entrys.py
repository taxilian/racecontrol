from ..urlDecorator import *
from ..models import *
from django.conf import settings
from django.shortcuts import redirect
from django.http import Http404
from pyzoop import zone, urlVars
from ..JSONUtils import ReturnsJSON, AcceptsJSON
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django import db
import os

Debug = True

import dateutil.parser, dateutil.tz
def parseDateTime(str):
    if not str: return None
    date = dateutil.parser.parse(str)
    return date.astimezone(dateutil.tz.tzlocal()).replace(tzinfo=None)

def extractWantedObj(src):
    return {
        "participant_bibNumber": src["participant_bibNumber"],
        "time_in": src["time_in"],
        "time_out": src["time_out"],
        "station_stationNumber": src["station_stationNumber"],
        "lastModifiedBy": src["lastModifiedBy"]
    }

def errorResponse(content, status=500):
    return HttpResponse(content, status=status)

@url(r'^/events/([^/]+)/entrys/?$', method="GET")
@ReturnsJSON
def entrysByEventId(request, evtId):
    try:
        resp = Entry.objects.filter(participant__raceEvent__uuid=evtId).toArray()
        resp = (extractWantedObj(o) for o in resp)
        return resp
    except:
        raise Http404

@url(r'^/events/([^/]+)/entrys/range$', method="POST")
@AcceptsJSON
@ReturnsJSON
def entrysByEventId(request, evtId, participants):
    try:
        resp = Entry.objects.filter(participant__raceEvent__uuid=evtId, participant__bibNumber__in=participants).toArray()
        resp = (extractWantedObj(o) for o in resp)
        return resp
    except:
        raise Http404

@url(r'^/events/([^/]+)/participants/([^/]+)/entrys?$', method="GET")
@ReturnsJSON
def entrysByEventIdAndparticipantId(request, evtId, bibNo):
    try:
        resp = Entry.objects.filter(participant__raceEvent__uuid=evtId, participant__bibNumber=bibNo).toArray()
        return (extractWantedObj(o) for o in resp)
    except:
        raise Http404

@url(r'^/events/([^/]+)/participants/([^/]+)/entrys/([0-9]+)/?$', method="PUT")
@AcceptsJSON
@ReturnsJSON
def entrysByEventIdAndparticipantId(request, evtId, bibNo, stationNo, e):
    try:
        raceEvent = RaceEvent.objects.get(pk=evtId)
        participant = Participant.objects.filter(raceEvent=raceEvent, bibNumber=bibNo).get()
        station = Station.objects.filter(raceEvent=raceEvent, stationNumber=stationNo).get()

        entry, created = Entry.objects.get_or_create(station=station, participant=participant)
        if "lastModifiedBy" not in e:
            e["lastModifiedBy"] = settings.STATION_UUID

        entry.time_in = parseDateTime(e["time_in"])
        entry.time_out = parseDateTime(e["time_out"])
        entry.save()

        db.connection._commit()
        return entry.toDict()
    except:
        if Debug:
            raise
        return self.error("Malformed JSON update request")

