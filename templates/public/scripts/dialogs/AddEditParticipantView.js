define(['base/BaseView', 'underscore', 'lib/keyProcessor',
        '_tpl!./jst/AddEditParticipant.jst'], function(BaseView, _, keyProcessor, tpl) {

    var AddEditParticipantView = BaseView.extend({
        event: null,

        events: {
            "click a.swapNumber": "onSwapClicked",
            "keyup :input" : "participantKeyUp"
        },

        render: function() {
            var self = this;
            var $el = $(self.el);
            $el.html(tpl({runner: self.runner, stations: self.stations}));
            return self;
        },

        setParticipant: function(runner) {
            this.runner = runner;
        },

        setStations: function(stations) {
            this.stations = stations;
        },

        onSaveClicked: function() {
            var self = this;
            var fieldValues = {};
            _.each(["firstName", "lastName", "home",
                    "age", "sex", "team", "dnfStation", "dnfReason"], function(field) {
                var match = "[name=fld_" + field + "]";
                var f = self.$("input" + match + ",select" + match);
                fieldValues[field] = f.val();
            });
            fieldValues.dnfStation = fieldValues.dnfStation || null;
            this.trigger("save", fieldValues);
        },

        onSwapClicked: function() {
            this.trigger("swapClicked");
        },

        participantKeyUp : function(evt) {
            var self = this,
                desc = keyProcessor.keyDesc(evt);
            // we only care about enter variants
            if (desc.substr(-5) !== "Enter") {
                return;
            }
            var $el = $(evt.currentTarget),
                $index = self.$(":input").index($el),
                $next = self.$(":input:eq(" + ($index + 1) + ")");
            // alt+enter, shift+enter, or last element: auto-submit
            if (desc === "alt+Enter" || desc === "shift+Enter" || !$next.length) {
                self.onSaveClicked();
                return;
            }
            $next.focus();
        }
    });

    return AddEditParticipantView;
});


