define(["backbone", "underscore"], function(Backbone, _) {

    var InputDialog = Backbone.View.extend({
        dfdResult: null,
        initialize: function(options) {
            this.title = options.title;
            this.html = options.html;
            this.dfdResult = options.deferred;
            this.cancel = options.cancel || false;
        },

        render: function() {
            var self = this;
            var $el = $(this.el);
            $el.empty();
            var handlerCalled = false;

            var okClicked = function(evt) {
                evt.preventDefault();
                handlerCalled = true;
                self.dfdResult.resolve(self.$("input").val());
                $(self.el).dialog("close");
            };
            var cancelClicked = function(evt) {
                evt.preventDefault();
                handlerCalled = true;
                self.dfdResult.reject("cancel");
                $(self.el).dialog("close");
            };

            var buttons = {};
            buttons[InputDialog.buttonText[0]] = okClicked;
            buttons[InputDialog.buttonText[1]] = cancelClicked;
            if (self.cancel) {
                buttons[InputDialog.buttonText[2]] = cancelClicked;
            }
            $el.dialog({
                title: this.title,
                autoOpen : true,
                modal: true,
                resizable: false,
                show: "fade",
                hide: "fade",
                minHeight: 40,
                closeOnEscape: true,
                buttons: buttons,
                beforeClose: function() {
                    if (!self.dfdResult.isRejected() && !self.dfdResult.isResolved()) {
                        // Dialog was closed with escape; like cancel or no
                        if (self.cancel) {
                            self.dfdResult.reject(); // esc == cancel
                        } else {
                            self.dfdResult.resolve(false); // esc == no
                        }
                    }
                },
                close: function() {
                    $(self.el).dialog("destroy");
                },
                open: function(evt) {
                    //var btn = $(evt.target).parents(".ui-dialog").find('button:contains("OK")');
                    //btn.button({icons: {primary:'ui-icon-check',secondary:null}});
                    $(evt.target.activeElement).find(".ui-dialog-titlebar-close").hide();
                }
            });
            $el.html(self.html);
            return this;
        }
    }, {
        input: function(title, html, cancel) {
            var dfd = new $.Deferred();
            var confirmDialog = new this({title: title, html: html, deferred: dfd, cancel: cancel});
            confirmDialog.render();
            return dfd.promise();
        },
        buttonText: ["OK", "Cancel"]
    });

    return InputDialog;
});
