var EventSplitsPage = RB.LWClass();
_p = EventSplitsPage.prototype;

_p.initialize = function(evtId) {
    var self = this;
    this.seconds = 5;
    this.updateTimerId = -1;
    this.evtId = evtId;
    this.rc = new RaceControl();
    this.rc.getRaceEvent(evtId, this.$("raceEventLoaded"));
    showLoading();
    this.dateField = null;
    this.sort = "num";
    this.desc = true;
    this.sortFunc = this.makeSortFunction("num", this.desc);
    this.em = $("#emRuler").width();

    this.counter = null;

    $(window).resize(this.$("resizeDiv"));
    this.resizeDiv();
};

_p.resizeDiv = function(evt) {
    var em = this.em;
    var availSize = $(window).height();

    $("#gridContainer").height(availSize - (18 * em));
}

_p.raceEventLoaded = function(event) {
    this.event = event;
    this.counter = new RunnerCounter(this.event.stationList);
    this.updateStations();
    this.initGrid();
    this.event.$("watchForUpdates")(this.$("updatesLoaded"), this.seconds);
    this.setSort("name");

    hideLoading();
}

_p.updatesLoaded = function(data) {
    if (data.stations) {
        var foundNew = this.updateStations();
        this.runner.setStationList(this.event.stationList);

        if (!foundNew) {
            return;
        }
        for (var n in this.event.participantList) {
            var rNo = this.event.participantList[n];
            var p = this.event.participants[rNo];
            this.updateParticipant(p);
        }
        this.updateStationCounts();
        return; // This updates the participants too
    }
    this.updateGrid(data);
}

_p.setSort = function(sort) {
    if (this.sort == sort) {
        this.desc = !this.desc;
    } else {
        this.sort = sort;
        this.desc = false;
    }
    this.sortGrid(this.sort, this.desc);
}

_p.updateStations = function() {
    var self = this;
    var foundNew = false;
    // first scan to see if there are new items
    for (var n in this.event.stationList) {
        var sNo = this.event.stationList[n];
        if (!this.event.stations[sNo].uuid) {
            continue;
        }
        if (!$("#s_%s".format(sNo)).length) {
            foundNew = true;
        }
    }
    var newheader = $("<ul></ul>");
    newheader.append('<li class="num">#</li>');
    newheader.append('<li class="name">Name</li>');

    for (var n in this.event.stationList) {
        var sNo = this.event.stationList[n];
        var station = this.event.stations[sNo];
        var entry = $('<li class="station stationHeader" id="s_%s">%s: %s...</li>'.format(sNo, sNo, station.name.value.substring(0, 6)));
        newheader.append(entry);
    }
    newheader.append('<li class="elapsed">Elapsed Time</li>');

    var header = $("#gridHeader");
    header.find("ul").remove();
    header.append(newheader);

    var num = 2.5 + 2.0 + 11.5 + (6.5 * this.event.stationList.length);
    $("#gridHeader,#gridContainer").css("min-width", "%sem".format(num));

    return foundNew;
};

_p.updateGrid = function(updates) {
    var self = this;

    for (var i in updates.participants) {
        var p = this.event.participants[updates.participants[i].bibNumber];
        this.updateParticipant(p);
    }

    for (var i in updates.entries) {
        var p = this.event.participants[updates.entries[i].participant_bibNumber];
        this.updateParticipant(p)
    }
}

_p.updateParticipant = function(participant) {
    var p = participant;
    var entry = $("#row_%s".format(p.bibNumber.value));
    if (entry.length) {
        this.updateRow(p);
    } else {
        entry = $( this.createRow(p) );
        var before = RB.findEntryPlace($("#gridContainer ul"), this.sortFunc, entry);
        if (before) {
            entry.insertBefore(before);
        } else {
            $("#gridContainer").append(entry);
        }
    }
    this.counter.setDroppedAt(p.bibNumber.value, p.dnfStation.value);
    if (p.dnfStation.value !== null) {
        var boxes = entry.find("li.station").get();
        for (var i = 0; i < boxes.length; i++) {
            var box = $(boxes[i]);
            var sNo = parseInt(box.attr("sNo"));
            if (sNo == p.dnfStation.value) {
                box.html("DROPPED");
            }
            if (sNo >= p.dnfStation.value) {
                box.addClass("dropped");
            }
        }
    } else {
        entry.find("li").removeClass("dropped");
    }
}

_p.updateRow = function(participant) {
    var p = participant;
    var rNo = p.bibNumber.value;

    var row = $("#row_%s".format(rNo));
    row.find(".name").html("%s, %s".format(p.lastName.value, p.firstName.value));

    for (var n in this.event.stationList) {
        var sNo = this.event.stationList[n];
        
        var find = $("#l_%s_%s".format(rNo, sNo));
        if (find.length) {
            find.html(this.getEntryContents(p, this.event.stations[sNo]));
        } else {
            var compare = function(a,b) {
                var snA = a.attr("sNo");
                var snB = b.attr("sNo");
                return (snA > snB) ? 1 : (snA < snB) ? -1 : 0;
            }
            var entry = $( this.createEntryItem(rNo, sNo, p, this.event.stations[sNo]) )
            var before = RB.findEntryPlace(row.find("li"), compare, entry);
            if (before) {
                entry.insertBefore(before);
            } else {
                row.append(entry);
            }
        }
    }
    row.find("li").removeClass("last");
    row.find("li:last").addClass("last");
}

_p.getEntryContents = function(participant, station) {
    var p = participant;
    var rNo = participant.bibNumber.value;
    var sNo = station.stationNumber.value;
    
    var tIn = "__:__";
    var tInClass = "inactive";
    var tInDisplay = station.stationNumber.value != this.event.startStation.value;
    var tOut = "__:__";
    var tOutClass = "inactive";
    var tOutDisplay = station.stationNumber.value != this.event.finishStation.value;
    var entry = p.entrys[sNo];
    if (entry) {
        this.counter.addEntry(entry);
        tIn = entry.time_in.$("toString")("HH:mm") || tIn;
        if ( tIn.substring(0,1) != "_" ) {
            tInClass = "";
        }
        tOut = entry.time_out.$("toString")("HH:mm") || tOut;
        if ( tOut.substring(0,1) != "_" ) {
            tOutClass = "";
        }
    }
    var tInString = tInDisplay ? '<span class="%s">%s</span>'.format(tInClass, tIn) : "";
    var tOutString = tOutDisplay ? '<span class="%s">%s</span>'.format(tOutClass, tOut) : "";
    var arrowString = tInString && tOutString ? "->" : "";
    return "%s%s%s".format(tInString, arrowString, tOutString)
}

_p.createEntryItem = function(rNo, sNo, participant, station, entry) {
    return '<li class="station" rNo="%s" sNo="%s" id="l_%s_%s">%s</li>'.format(rNo, sNo, rNo, sNo, this.getEntryContents(participant, station) );
}

_p.getElapsedItem = function(rNo, participant) {
    var p = participant;
    var start = p.entrys[this.event.startStation.value];
    var finish = p.entrys[this.event.finishStation.value];

    if (p.dnfStation.value !== null) {
    	return '<li class="elapsed dropped" rNo="%s">Dropped</li>'.format(rNo);
    } else if (!start || start.time_out.value === null) {
    	return '<li class="elapsed dropped" rNo="%s">Not started</li>'.format(rNo);
    } else if (!finish || finish.time_in.value === null) {
    	return '<li class="elapsed dropped" rNo="%s">Not finished</li>'.format(rNo);
    } else {
    	return '<li class="elapsed dropped" rNo="%s">%s</li>'.format(rNo, new TimeSpan(finish.time_in.value - start.time_out.value).toString());
    }
}

_p.createRow = function(participant) {
    var p = participant;
    var rNo = p.bibNumber.value;
    if (!p.uuid.value) {
        return "";
    }
    tpl = [
        ['<ul rNo="%s" id="row_%s">', ["bibNumber", "bibNumber"]],
        ['<li class="num" rNp="%s">%s</li>', ["bibNumber", "bibNumber"]],
        ['<li class="name" rNo="%s">%s, %s</li>', ["bibNumber", "lastName", "firstName"]],
    ];

    for (var n in this.event.stationList) {
        var sNo = this.event.stationList[n];
        
        tpl.push(
            [ this.createEntryItem(rNo, sNo, p, this.event.stations[sNo]) ]
        );
    }

    tpl.push([ this.getElapsedItem(rNo, p) ]);
    tpl.push(["</ul>"]);
    var row = $(RB.processTemplate(tpl, [p]));
    row.find("li").removeClass("last");
    row.find("li:last").addClass("last");
    return row;
}

_p.initGrid = function() {
    var self = this;
    var stations = this.event.stations;
    var rows = [];
    $("#gridContainer ul").remove();
    for (var i in this.event.participantList) {
        var rNo = this.event.participantList[i];
        var p = this.event.participants[rNo];
        
        this.updateParticipant(p);
    }
    var grid = $("#gridContainer");
    for (var i in rows) {
        grid.append(rows[i]);
    }
    var lis = $("#gridContainer ul");
}

_p.sortGrid = function(target, desc) {
    var self = this;
    var mylist = $("#gridContainer");
    var list = $("#gridContainer ul").get();

    this.sortFunc = this.makeSortFunction(target, desc);
    list.sort(this.sortFunc);
    $.each(list, function(idx, itm) {
        mylist.append(itm);
    });
}

_p.makeSortFunction = function(target, desc) {
    var self = this;
    var sort_desc = false;
    if (desc) sort_desc = true;

    if (target == "num" || target == "name") {
        var getVal = function(src) {
            if (target == "num") {
                return parseInt($(src).attr("rNo"));
            } else if (target == "name") {
                var p = self.event.participants[$(src).attr("rNo")];
                return "%s%s".format(p.lastName.value, p.firstName.value);
            }
        };
        var sortFunc = function(a, b) {
            var compA = getVal(a);
            var compB = getVal(b);
            var ret = (compA < compB) ? -1 : (compA > compB) ? 1 : 0;
            if (sort_desc) {
                ret *= -1;
            }
            return ret;
        };
        return sortFunc;
    }

    if (target.substring(0,1) == "s") {
        var type = target.substring(1,2);
        var sNo = parseInt(target.substring(2));

        var sortFunc = function(a, b) {
            var getTime = function(bibNo, sNew) {
                var p = self.event.participants[bibNo];
                if (!p.entrys[sNew]) {
                    return 99999999999999999999;
                }
                switch(type) {
                    case "i":
                        if (p.entrys[sNew].time_in.value) {
                            return p.entrys[sNew].time_in.value.getTime();
                        } else if (p.entrys[sNew].time_out.value) {
                            return p.entrys[sNew].time_out.value.getTime();
                        } else return 99999999999999999999;
                        break;
                    case "o":
                        if (p.entrys[sNew].time_out.value) {
                            return p.entrys[sNew].time_out.value.getTime();
                        } else if (p.entrys[sNew].time_in.value) {
                            return p.entrys[sNew].time_in.value.getTime();
                        } else return 99999999999999999999;
                        break;
                    default:
                        return 99999999999999999999;
                };
            }
            var ba = $(a).attr("rNo")
            var bb = $(b).attr("rNo")
            var ta = getTime(ba, sNo);
            var tb = getTime(bb, sNo);

            if (ta > tb) return 1;
            else if (ta < tb) return -1;
            else { // if they are equal (even if they are null)
                var seenA = self.counter.lastSeenAt[ba];
                var seenB = self.counter.lastSeenAt[bb];
                if (seenA < seenB) return 1;
                else if (seenA > seenB) return -1;
                else {
                    for (var i = self.event.stationList.indexOf(sNo) - 1; i >= 0; i--) {
                        var sNew = self.event.stationList[i];
                        var ta = getTime(ba, sNew);
                        var tb = getTime(bb, sNew);
                        if (ta > tb) return 1;
                        if (ta < tb) return -1;
                    }
                    return 0;
                }
            } 
        }
        return sortFunc;
    }

}
delete _p;
