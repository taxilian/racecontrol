from django.db.models import Q
from ..models import *
from django.conf import settings
from django.shortcuts import redirect
from pyzoop import zone
from pyzoop import login_required, ReturnsJSON, AcceptsJSON
from django.contrib.auth.views import login, logout_then_login
from django.contrib.auth import authenticate, login, logout
from zoneWebAPIEvents import *
import datetime, json, os
import os
from django import db
from soundex import soundex

_TIME_FORMAT = "%m %d %Y %H %M %S %f"

def loginRequired(target):
    def wrapper(self, request, path):
        if not request.user.is_authenticated():
            return self.error("Unauthenticated")
        else:
            return target(self, request, path)
    return wrapper

class zoneWebAPI(zone):
    Debug = True
    def __init__(self):
        super(zoneWebAPI,self).__init__(events=zoneWebAPIEvents())

    def initZone(self, request, pathList):
        pass

    def initPages(self, request, path):
        pass

    def error(self, message):
        resp = Bag()
        resp.status = u'failed'
        resp.message = message
        return resp

    def success(self):
        resp = Bag()
        resp.status = u'success'
        return resp

    @ReturnsJSON
    def page_default(self, request, path):
        return self.error("Invalid API")

    @ReturnsJSON
    def page_index(self, request, path):
        return self.error("Invalid API")

    @ReturnsJSON
    def page_isLoggedIn(self, request, path):
        return {"authenticated":request.user.is_authenticated()}

    @ReturnsJSON
    def page_login(self, request, path):
        resp = Bag()
        resp.status = u'success'
        resp.authenticated = True
        user = authenticate(username=request.GET.get("user", ""), password=request.GET.get("pass", ""))
        if user is not None:
            if user.is_active:
                login(request, user)
                return resp
            else:
                return self.error("Account disabled")
        else:
            return self.error("Invalid username or password")

    @ReturnsJSON
    def page_logout(self, request, path):
        logout(request)
        return self.success()

    @ReturnsJSON
    def page_getEventList(self, request, path):
        evtList = RaceEvent.objects.all()
        if (len(path) > 1):
            evtList = evtList.filter(pk=path[1])
        return evtList.toArray(False)

    @ReturnsJSON
    def page_getFullEventList(self, request, path):
        evtList = RaceEvent.objects.all()
        if (len(path) > 1):
            evtList = evtList.filter(pk=path[1])
        return evtList.toArray(True)

    @ReturnsJSON
    def page_getFullEventData(self, request, path):
        evtId = ""
        if len(path) > 1:
            evtId = path[1]
        else:
            return self.error("Invalid event: %s" % path)

        updTime = datetime.datetime.now().strftime(_TIME_FORMAT)
        evt = RaceEvent.objects.get(pk=evtId)
        stations = Station.objects.filter(raceEvent=evt)
        participants = Participant.objects.filter(raceEvent=evt)
        entries = Entry.objects.select_related("participant__bibNumber", "station__stationNumber").filter(participant__in=participants,station__in=stations)
        pout = Bag(participants.toDict())
        for entry in entries:
            bNum = entry.participant.bibNumber
            sNum = entry.station.stationNumber
            if not pout[bNum].has_key("entrys"):
                pout[bNum]["entrys"] = Bag()
            pout[bNum]["entrys"][sNum] = entry.toDict()
        resp = Bag(evt.toDict())
        resp.stations = stations.toDict()
        resp.participants = pout
        resp.LastUpdated = updTime
        return resp

    @ReturnsJSON
    def page_getEventResults(self, request, path):
        evtId = ""
        if len(path) > 1:
            evtId = path[1]
        else:
            return self.error("Invalid event: %s" % path)

        updTime = datetime.datetime.now().strftime(_TIME_FORMAT)
        evt = RaceEvent.objects.get(pk=evtId)
        stations = Station.objects.filter(stationNumber__in=[evt.startStation, evt.finishStation])
        participants = Participant.objects.select_related("dnfStation__stationNumber", "dnfStation__name").filter(raceEvent=evt)
        entries = Entry.objects.select_related("participant__bibNumber", "station__stationNumber").filter(participant__in=participants,station__in=stations)
        pout = Bag(participants.toDict())
        for entry in entries:
            bNum = entry.participant.bibNumber
            sNum = entry.station.stationNumber
            if not pout[bNum].has_key("entrys"):
                pout[bNum]["entrys"] = Bag()
            pout[bNum]["entrys"][sNum] = entry.toDict()
        resp = Bag(evt.toDict())
        resp.stations = stations.toDict()
        resp.participants = pout
        resp.LastUpdated = updTime
        return resp

    @ReturnsJSON
    def page_getParticipant(self, request, path):
        bibNo = request.GET.get("bib", -1)
        evtId = request.GET.get("evtId", "")
        participant = Participant.objects.get(bibNumber=bibNo,raceEvent__pk=evtId)
        entrys = Entry.objects.select_related("station__stationNumber", "station__name", "station__distance").filter(participant=participant)
        outP = participant.toDict()
        outP["entrys"] = entrys.toDict()

        return outP

    @ReturnsJSON
    def page_getParticipantList(self, request, path):
        if len(path) < 2:
            return self.error('No event specified')
        evtId = path[1]
        if len(path) < 3:
            try:
                resp = Participant.objects.filter(raceEvent__uuid=evtId).toDict(True)
            except:
                return self.error("Invalid event")
        else:
            runNo = path[2]
            try:
                resp = Participant.objects.filter(raceEvent=evtId,bibNumber=runNo)[0].toDict(True)
            except:
                if self.Debug:
                    raise
                return self.error("Invalid participant")

        return resp

    @ReturnsJSON
    def page_findByName(self, request, path):
        evtId = request.GET.get("evtId", -1)
        name = request.GET.get("name", "")
        names = name.split(" ")

        num = False
        filter = None
        for cur in names:
            if filter == None:
                filter = Q(firstNameSx__startswith=soundex(cur))
            else:
                filter |= Q(firstNameSx__startswith=soundex(cur))
            filter |= Q(lastNameSx__startswith=soundex(cur))
            filter |= Q(firstName__startswith=cur)
            filter |= Q(lastName__startswith=cur)
            try:
                filter |= Q(bibNumber=int(cur))
            except:
                num = False

        lookup = Participant.objects.filter(Q(raceEvent__pk=evtId) & filter).order_by("lastName", "firstName")
        return lookup.toArray()

    @ReturnsJSON
    def page_getAllUpdates(self, request, path):
        evtId = request.GET.get("evtId", -1)
        if evtId == -1:
            return self.error("Invalid event ID")
        resp = Bag()
        resp.LastUpdated = datetime.datetime.now().strftime(_TIME_FORMAT)
        updSeq = datetime.datetime.strptime(request.GET.get("modified", 0), _TIME_FORMAT)

        resp.RaceEvents = RaceEvent.objects.filter(modified__gt=updSeq,uuid=evtId).toArray()
        resp.Stations = Station.objects.filter(modified__gt=updSeq,raceEvent__uuid=evtId).toArray()
        resp.Participants = Participant.objects.filter(modified__gt=updSeq,raceEvent__uuid=evtId).toArray()
        resp.Entrys = Entry.objects.select_related("station__stationNumber", "participant__bibNumber").filter(modified__gt=updSeq,participant__raceEvent__uuid=evtId).toArray()
        return resp

    @ReturnsJSON
    @loginRequired
    @AcceptsJSON
    def page_updateRaceEvent(self, request, path, req):
        """
            updateEvent expects JSON in the same format it would receive it from getEventList -
            including in an array, even if there is only one item
        """
        output = []
        try:
            for event in req:
                event["date"] = datetime.datetime.strptime(event["date"], "%Y-%m-%d %H:%M:%S")
                dbEvent, created = RaceEvent.objects.get_or_create(uuid=event["uuid"], date=event["date"])
                if "lastModifiedBy" not in event:
                    event["lastModifiedBy"] = settings.STATION_UUID
                dbEvent.fromDict(event)
                output.append(dbEvent.toDict())
            db.connection._commit()
            return output
        except:
            if self.Debug:
                raise
            return self.error("Malformed JSON update request")

    @ReturnsJSON
    @loginRequired
    def page_deleteRaceEvent(self, request, path):
        """
            deleteRaceEvent expects an id get parameter
        """
        uuid = request.GET.get("uuid", -1)
        if uuid == -1:
            return self.error("Invalid ID")
        else:
            try:
                Entry.objects.filter(participant__raceEvent__pk=uuid).delete()
                Participant.objects.filter(raceEvent__pk=uuid).delete()
                Station.objects.filter(raceEvent__pk=uuid).delete()
                RaceEvent.objects.get(uuid=uuid).delete()
                db.connection._commit()
            except:
                raise
                return self.error("Unknown error")
        return self.success()

    @ReturnsJSON
    def page_getStationList(self, request, path):
        evtId = request.GET.get("select", -1)
        if evtId == -1:
            return self.error("Invalid event ID")
        resp = Station.objects.filter(raceEvent__uuid=evtId).toDict()
        return resp

    @ReturnsJSON
    @loginRequired
    @AcceptsJSON
    def page_updateStation(self, request, path, req):
        """
            updateEvent expects JSON in the same format it would receive it from getStationList -
            including in an array, even if there is only one item
        """
        try:
            output = []
            for s in req:
                raceEvent = RaceEvent.objects.get(pk=s["raceEvent_id"])
                print "rid", s["raceEvent_id"]
                station, created = Station.objects.get_or_create(raceEvent=raceEvent, stationNumber=s["stationNumber"])
                if "lastModifiedBy" not in s:
                    s["lastModifiedBy"] = settings.STATION_UUID
                station.fromDict(s)
                output.append(station.toDict())
            db.connection._commit()
            return output
        except:
            if self.Debug:
                raise
            return self.error("Malformed JSON update request")

    @ReturnsJSON
    @loginRequired
    @AcceptsJSON
    def page_updateEntry(self, request, path, req):
        """
            updateEvent expects JSON in the same format it would receive it from getEventList -
            including in an array, even if there is only one item
        """
        try:
            output = []
            for e in req:
                raceEvent = RaceEvent.objects.get(pk=e["raceEvent_id"])
                participant = Participant.objects.filter(raceEvent=raceEvent, bibNumber=e["participant_bibNumber"]).get()
                station = Station.objects.filter(raceEvent=raceEvent, stationNumber=e["station_stationNumber"]).get()
                e["time_in"] = datetime.datetime.strptime(e["time_in"], "%Y-%m-%d %H:%M:%S") if e["time_in"] else None
                e["time_out"] = datetime.datetime.strptime(e["time_out"], "%Y-%m-%d %H:%M:%S") if e["time_out"] else None

                entry, created = Entry.objects.get_or_create(station=station, participant=participant)
                if "lastModifiedBy" not in e:
                    e["lastModifiedBy"] = settings.STATION_UUID
                if "station_id" in e: del e["station_id"]
                if "participant_id" in e: del e["participant_id"]
                entry.fromDict(e)
                output.append(entry.toDict())
            db.connection._commit()
            return output
        except:
            if self.Debug:
                raise
            return self.error("Malformed JSON update request")

    @ReturnsJSON
    @loginRequired
    @AcceptsJSON
    def page_updateParticipant(self, request, path, req):
        """
            updateEvent expects JSON in the same format it would receive it from getEventList -
            including in an array, even if there is only one item
        """
        try:
            output = []
            for p in req:
                raceEvent = RaceEvent.objects.get(pk=p["raceEvent_id"])
                participant, created = Participant.objects.get_or_create(raceEvent=raceEvent, bibNumber=p["bibNumber"])
                if "lastModifiedBy" not in p:
                    p["lastModifiedBy"] = settings.STATION_UUID
                participant.fromDict(p)
                output.append(participant.toDict())
            db.connection._commit()
            return output
        except:
            if self.Debug:
                raise
            return self.error("Malformed JSON update request")

