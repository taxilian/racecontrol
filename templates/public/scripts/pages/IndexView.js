define(['base/BaseView', 'underscore',
        '_tpl!./jst/Index.jst'], function(BaseView, _, tpl) {

    var IndexView = BaseView.extend({
        eventList: null,
        initialize: function(options) {
            _.bindAll(this, "refresh");
        },

        events: {
            "click button.edit": "onEditEvent",
            "click button.del": "onDelEvent",
            "click .event": "onEventClick",
            "click #new_event" :"onNewEvent"
        },

        render: function() {
            var self = this;
            var $el = $(self.el);
            $el.addClass("stdTablePage list");
            $el.html(tpl({events: self.eventList}));
            $el.find(".button").button();
            return self;
        },

        refresh: function() {
            if (this.isActive()) {
                this.render();
            }
        },

        setEventList: function(evtList) {
            var self = this;
            if (evtList != self.eventList && self.eventList) {
                self.eventList.unbind("all", self.refresh);
            }
            self.eventList = evtList;
            if (evtList) {
                evtList.bind("all", self.refresh);
            }
            if (self.isActive()) {
                self.refresh();
            }
        },

        onEditEvent: function(evt) {
            evt.stopPropagation();
            var $c = $(evt.currentTarget).closest(".event");
            var id = $c.attr("id");
            this.trigger("edit", id);
        },

        onDelEvent: function(evt) {
            evt.stopPropagation();
            var $c = $(evt.currentTarget).closest(".event");
            var id = $c.attr("id");
            this.trigger("del", id);
        },

        onNewEvent: function(evt) {
            this.trigger("new");
        },

        onEventClick: function(evt) {
            evt.stopPropagation();
            var $c = $(evt.currentTarget).closest(".event");
            var id = $c.attr("id");
            this.trigger("open", id);
        }
    });

    return IndexView;
});

