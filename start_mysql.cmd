@echo off
set IMG=mysql
set CNAME=mysql_1
docker pull %IMG%
docker stop %CNAME%
docker rm %CNAME%
docker run ^
    -d ^
    --restart=always ^
    --name %CNAME% ^
    -e MYSQL_ROOT_PASSWORD=raceevent ^
    -e TERM=dumb ^
    -p 3306:3306 ^
    -v C:\code\mysql_data:/var/lib/mysql ^
    %IMG%  --default-authentication-plugin=mysql_native_password
