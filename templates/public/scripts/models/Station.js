define(["backbone", "underscore", "jquery", "defaults", "datelib"], function(Backbone, _, $, defaults) {

    var Stations, Station = Backbone.Model.extend({
        idAttribute: "stationNumber",
        url: function() {
            var url = "/newAPI/events/" + this.get("raceEvent_id") + "/stations";
            if (!this.isNew()) {
                url += "/" + this.id;
            }
            return url;
        },
        validate: function(attrs) {
            var self = this;
            var col = this.collection;
            if ("name" in attrs && !attrs.name) {
                return "Please enter a station name";
            }
        }
    }, {
        fetchById: function(eventId, sId) {
            return $.when(Stations.fetchAll(eventId)).pipe(function(stations) {
                return stations.get(sId);
            });
        }
    });

    Stations = Backbone.Collection.extend({
        model: Station,
        url: function() {
            return "/newAPI/events/" + this.eventId + "/stations";
        },
        comparator: function(station) {
            return station.id;
        }
    }, {
        list: null,
        fetchAll: function(eventId) {
            var self = this, cache;
            if (!self.list) { self.list = {}; }
            cache = self.list[eventId];
            if (cache) {
                return cache;
            }
            var tmp = new self();
            tmp.eventId = eventId;
            cache = self.list[eventId] = tmp.fetch().pipe(function() {
                return (self.list[eventId] = tmp);
            });
            return cache;
        }
    });

    return {
        obj: Station,
        list: Stations
    };
});

