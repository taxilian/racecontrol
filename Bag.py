'''
A Bag is a generic data structure, especially useful for data that needs
to be serialized for network transmission or file storage. Bags have
methods so that they can be used much like dictionaries as well.

This code is in the public domain.

JSON, a restricted form of XML, and HTML form formats are supported.

TODO: XML output isn't 100% there yet - we should use something like 4suite to properly convert all characters
    (and maybe use a real XML generation API while we're at it).
'''

import os, json, re, decimal, urllib, datetime
from xml.dom.minidom import parseString as XMLParse
from xml.sax.saxutils import escape as XMLEscape, unescape as XMLUnescape


class Bag(dict):
    XML_TYPES_OPTIONAL = True # so users of this module can probe for the feature includeTypes=False
    def __repr__(self): 
        return 'Bag' + super(Bag, self).__repr__()
    def __setattr__(self, k, v):
        self[k] = v
    def __getattr__(self, k):
        try: return self[k]
        except KeyError: raise AttributeError('No such attribute %r' % k)
    def __delattr__(self,k):
        del self[k]

    # Routines for conversion to/from JSON
    @staticmethod
    def FromJSON(s): return JSONDecode(s)
    def ToJSON(self, pretty=False): return JSONEncode(self, pretty)

    # Routines for conversion to/from XML
    @staticmethod
    def FromXML(s): return XMLDecode(s)
    def ToXML(self, container='msg', includeTypes=True): return XMLEncode(self, container, includeTypes)

    @staticmethod
    def FromForm(s): return FormDecode(s) # It's not really "form" necessarily, but how cool is it to have a method named FromForm?!
    def ToForm(self): return FormEncode(self)

    @staticmethod
    def FromFile(filename, default=None):
        ''' Reads a JSON Bag from a File'''
        try:
            f = open(filename, 'r')
            bag = Bag.FromJSON(f.read())
            f.close()
        except:
            if default:
                return default
            raise
        return bag

    def ToFile(self, filename, pretty=True, create_dirs=False):
        ''' Writes a JSON Bag to a File'''
        path = os.path.dirname(filename)
        if create_dirs and not os.path.isdir(path):
            os.makedirs(path)
        f = open(filename, 'wb')
        f.write(self.ToJSON(pretty))
        f.close()

    def ConvertTypes(self):
        '''Helper function, used prior to serialization'''
        return _ConvertTypes(self)
 
    def GetDict(self): return self.copy()

class Error(Exception): pass
class SerializationError(Error): pass # problem when encoding/decoding

# ---------------------------------------------------------------------------------------
# JSON handling
# ---------------------------------------------------------------------------------------

HAS_UTF8 = re.compile(r'[\x80-\xff]')
def eba(s):
    if isinstance(s, str) and HAS_UTF8.search(s) is not None:
        try:
            s = s.decode('utf-8')
        except:
            # Do old-style replacement
            pass
    def replace(match):
        s = match.group(0)
        try:
            return json.encoder.ESCAPE_DCT[s]
        except KeyError:
            n = ord(s)
            if n < 0x10000:
                return '\\u%04x' % (n,)
            else:
                # surrogate pair
                n -= 0x10000
                s1 = 0xd800 | ((n >> 10) & 0x3ff)
                s2 = 0xdc00 | (n & 0x3ff)
                return '\\u%04x\\u%04x' % (s1, s2)
    return '"' + str(json.encoder.ESCAPE_ASCII.sub(replace, s)) + '"'
json.encoder.encode_basestring_ascii = eba
del eba

def _ForceBags(obj):
    '''Converts any dictionaries to Bags'''
    if type(obj) is dict:
        b = Bag()
        for k,v in obj.items():
            b[str(k)] = _ForceBags(v)
        return b
    elif type(obj) in (list, tuple):
        return [_ForceBags(x) for x in obj]
    else:
        return obj

def JSONEncode(obj, pretty=False):
    '''Returns the given object as a JSON string.'''
    kwargs = {}
    if pretty:
        kwargs = {'sort_keys':True, 'indent':4}
    try:
        return json.dumps(_ConvertTypes(obj), **kwargs)
    except Exception, e:
        raise SerializationError(e)

def JSONDecode(s):
    '''Given a JSON string, returns a corresponding Bag.'''
    try:
        obj = json.loads(s)
        return _ForceBags(obj)
    except Exception, e:
        raise SerializationError(e)

# ---------------------------------------------------------------------------------------
# XML handling
# ---------------------------------------------------------------------------------------

def _XMLNode(obj, container, includeTypes=True):
    '''Returns the given object as an XML string. container tells what name to use for the
    outer container.'''
    attr = None
    v = obj
    if obj is None:
        return '<%s />' % container
 
    objType = type(obj)
    if objType is bool:
        v = str(obj)
        attr = 'bool'
    elif objType is float:
        v = repr(obj)
        attr = 'number'
    elif objType in (int, long):
        v = str(obj)
        attr = 'number'
    elif objType in (list, tuple):
        attr = 'list'
        v = ''.join([_XMLNode(x, 'item', includeTypes) for x in obj])
    elif objType in (dict, Bag):
        attr = 'object'
        v = ''.join([_XMLNode(v, k, includeTypes) for (k,v) in obj.items()])
    else: # assume it's string-like
        v = _ConvertToString(XMLEscape(obj)) # already a string

    extra = ''
    if includeTypes and attr:
        extra = ' type="%s"' % attr
    return '<%s%s>%s</%s>' % (container, extra, v, container)

def _XMLNode2(obj, container, includeTypes=True):
    '''Returns the given object as an XML string. container tells what name to use for the
    outer container.'''
    attr = None
    attributePrefix = ''
    childrenFound = False
    elementPrefix = ''

    v = obj
    if obj is None:
        return '<%s />' % container
 
    objType = type(obj)
    if objType is bool:
        v = str(obj)
        attr = 'bool'
    elif objType is float:
        v = repr(obj)
        attr = 'number'
    elif objType in (int, long):
        v = str(obj)
        attr = 'number'
    elif objType in (list, tuple):
        attr = 'list'
        v = ''.join([_XMLNode2(x, 'item', includeTypes) for x in obj])
        childrenFound = True
    elif objType in (dict, Bag):

        # Fix this so we can get actual attributes 
        attributes  = []
        elements    = []

        # Sort the items based on generalized behavior
        for k,v in obj.items():
            if type(v) in (bool, int, long, float, str, unicode, list):
                attributes.append((k,v))
            else:
                elements.append((k,v))

        if len(elements) > 0:
            childrenFound = True

        # Do what we used to do for true objects (recurse)
        attr = 'object'
        v = ''.join([_XMLNode2(v, k, includeTypes) for (k,v) in elements])

        # Otherwise build an attribute string on the main object (current)
        for key,val in attributes:
            key = _ConvertToString(XMLEscape(key))
            if type(val) in ('str', 'unicode'):
                attributePrefix += ' %s=\"%s\"' % (key, _ConvertToString(XMLEscape(val)))
            elif type(val) == list:
                elementPrefix = ' '.join([_XMLNode2(x, 'item', includeTypes) for x in val])
            elif type(val) in (dict, Bag):
                elementPrefix = ' '.join([_XMLNode(v, k, includeTypes) for (k, v) in obj.items()])
            else:
                attributePrefix += ' %s=\"%s\"' % (key, str(val))

        # If we had sub objects, put them in after our attributes
        if elementPrefix is not None and elementPrefix != '':
            v += ' %s' % elementPrefix

    else: # assume it's string-like
        v = _ConvertToString(XMLEscape(obj)) # already a string

    extra = ''
    if includeTypes and attr:
        extra = ' type="%s"' % attr

    if childrenFound:
        return '<%s%s%s>%s</%s>' % (container, attributePrefix, extra, v, container)
    else:
        return '<%s%s%s%s/>' % (container, attributePrefix, extra, v)

def XMLEncode(obj, container='item', includeTypes=True):
    return '<?xml version="1.0" encoding="UTF-8" ?>\n' + _XMLNode(_ConvertTypes(obj), container, includeTypes)
    #return '<?xml version="1.0" encoding="UTF-8" ?>\n' + _XMLNode2(_ConvertTypes(obj), container, includeTypes)

def _UnXMLNode(node):
    '''Given a parse XML node, returns a Python object.'''
    # Determine the data type - guessing if needed
    textNodes = []
    elementNodes = []
    for n in node.childNodes:
        if n.nodeType in (node.CDATA_SECTION_NODE, node.TEXT_NODE):
            textNodes.append(n.data)
        elif n.nodeType == node.ELEMENT_NODE:
            elementNodes.append(n)
    childText = XMLUnescape(''.join(textNodes))

    # We throw an error if there was non-whitespace text and element nodes as it's not clear how that should be handled
    if childText.strip() and elementNodes:
        raise ValueError('Node %r has both child text nodes and child element nodes' % node)

    dataType = node.getAttribute('type')
    if not dataType and len(elementNodes) > 0:
        # We're supposed to guess the data type. We'll automatically handle it if the data is string or None.
        # Beyond that, all we can guess is list or object, based on the names of child nodes - if they're all the
        # same (whatever the name is), it's a list, otherwise an object. In the ambiguous case of a 1-element list, 
        # we assume it's an object. If that's not what the caller wanted, they should have been explicit!
        names = [x.tagName for x in elementNodes]
        uNames = set(names)

        if len(uNames) > 1 and len(uNames) != len(elementNodes):
            # We can't tell if this should be a list or an object
            raise ValueError('Node %r has %d child nodes but only %d unique names for those nodes' % (node, len(elementNodes), len(uNames)))

        if len(elementNodes) == 1 or len(set(names)) > 1:
            dataType = 'object'
        else:
            dataType = 'list'

    if dataType.startswith('bool'):
        b = childText.lower().strip()
        return (b == 'true') or (b == '1')
    elif dataType.startswith('num'):
        return _ParseNum(childText)
    elif dataType == 'list':
        return [_UnXMLNode(x) for x in elementNodes]
    elif dataType.startswith('obj'):
        b = Bag()
        for e in elementNodes:
            b[str(e.tagName)] = _UnXMLNode(e)
        return b
    elif len(node.childNodes) == 0:
        return None
    else: # assume it's a string
        if dataType:
            raise ValueError('Unknown data type %r for node %r' % (dataType, node))
        return childText

    # if all kid names are the same and len(kids) > 1 - list
    # otherwise, object

def XMLDecode(s):
    '''Given a restricted XML string, returns a corresponding Bag.'''
    try:
        doc = XMLParse(s.strip())
        return _UnXMLNode(doc.childNodes[0])
    except Exception, e:
        raise SerializationError(e)

# ---------------------------------------------------------------------------------------
# HTML form handling
# ---------------------------------------------------------------------------------------
# use :int, etc.
# todo: we could allow for complex objects with stuff like person.name=5 and person.0.age=3
# types are via colons
def FormEncode(obj):
    '''Does not support complex child objects yet'''
    try:
        values = []
        for name, value in _ConvertTypes(obj).items():
            objType = type(value)
            if value is None:
                values.append((name + ':null', ''))
            elif objType is bool:
                values.append((name + ':bool', str(value).lower()))
            elif objType in (int, long, float):
                values.append((name + ':number', value))
            elif objType in (list, tuple):
                # for lists we spit out the same name over and over
                for x in value:
                    # TODO: at least attache a data type here
                    values.append((name, x))
            elif objType in (dict, Bag):
                raise NotImplementedError('FormEncode does not yet support serializing complex child objects')
            else:
                values.append((name, _ConvertToString(value)))
        return urllib.urlencode(values, True) # doseq=True
    except Exception, e:
        raise SerializationError(e)

def FormDecode(s):
    '''Does not support complex child objects yet'''
    s = s.strip()
    if not s:
        return Bag()
    try:
        import cgi # I don't trust the cgi module (lotsa magic), so only import it when we need it
        b = Bag()
        for name, value in cgi.parse_qsl(s, True, True): # True,True = keep blanks, use strict parsing
            # If a type was specified, convert the data now
            i = name.find(':')
            if i != -1:
                objType = name[i+1:]
                name = name[:i]
                if objType == 'null':
                    value = None
                elif objType.startswith('num'):
                    value = _ParseNum(value)
                elif objType.startswith('bool'):
                    value = value.lower().strip()
                    value = (value in ('true', '1', 'on', 'checked'))
                else:
                    value = value.decode('utf8')
            else:
                value = value.decode('utf8')

            if not b.has_key(name):
                b[name] = value
            else:
                # If it already has this item, then the value is a member of a list.
                container = b[name]
                if type(container) is not list:
                    b[name] = container = [container] # had a single value, now it's a list
                container.append(value)
        return b
    except Exception, e:
        raise SerializationError(e)

# ---------------------------------------------------------------------------------------
# Common routines
# ---------------------------------------------------------------------------------------
def _ParseNum(s):
    '''Converts a string to a number. If number appears to be a whole number, returns it as an int (float otherwise)'''
    s = s.strip()
    if s.find('.') == -1:
        return int(s)
    return float(s)

def _ConvertTypes(obj):
    '''Recursively inspects the objects to convert any date, time, or datetime objects to strings.'''
    objType = type(obj)
    if isinstance(obj, (list, tuple, set)):
        return [_ConvertTypes(x) for x in obj]
    elif isinstance(obj, (dict,)):
        d = Bag()
        for k,v in obj.items():
            d[k] = _ConvertTypes(v)
        return d
    elif isinstance(obj, (datetime.date, datetime.time, datetime.datetime)):
        return str(obj)
    elif isinstance(obj, decimal.Decimal):
        return str(obj)
    elif hasattr(obj, 'ToBag'):
        # if we have a ToBag method, use it...
        return _ConvertTypes(obj.ToBag())
    elif hasattr(obj, '__dict__'):
        # Hmm... it appears to be an object of some sort - give it a try!
        d = Bag()
        for k,v in obj.__dict__.items():
            d[k] = _ConvertTypes(v)
        return d
    return obj

def _ConvertToString(value):
    ''' Tries a few methods to convert values to a string. Probably not fool-
        proof, but better than relying on one of the methods alone. '''
    if type(value) is str: return value
    elif type(value) is unicode: return value.encode('utf-8')
    else: return str(value)

if __name__ == '__main__':
    ''' The Poor Man's Unit Test '''

    b = Bag(name='dave', age=3, people=[1,2.7,3.5], url='http://www.google.com/', answer=None)
    b.lastName = 'ma\xf1ana'.decode('latin-1')
    b.test = 'te\xc3\x9ft'
    b.foo = 'yep'
    b.bar = False
    b.hm = Bag(yes=1, no=0)
    print " original Bag"
    print b
    print b.ToJSON()
    print b.ToXML()

    print "\n via Bag.FromJSON(bag.ToJSON())"
    c = Bag.FromJSON(b.ToJSON())
    print c
    print c.ToJSON()
    print c.ToXML()

    print "\n via Bag.FromXML(bag.ToXML())"
    d = Bag.FromXML(b.ToXML())
    print d
    print d.ToJSON()
    print d.ToXML()

    print "\n via Bag.FromForm(bag.ToForm())"
    del b.hm # complex data structures not supported in form serialization
    e = Bag.FromForm(b.ToForm())
    print e
    print e.ToJSON()
    print e.ToXML()
    print e.ToForm()

    print "\n general XML tests"
    print Bag.FromXML('<msg>hi there</msg>')
    print Bag.FromXML('<msg><x>3</x></msg>')
    print Bag.FromXML('<msg><x>4</x><x>5</x><x>6</x></msg>')
    print Bag.FromXML('<msg><x type="num">4</x><y type="num">5</y><z>6</z></msg>')
    failed = False
    try: print Bag.FromXML('<msg><x>4</x><y>5</y><x>6</x></msg>') # should raise an error
    except: failed = True
    assert failed, "Should fail on unexpected XML structure (x-y-x)"

    b = Bag(dates=Bag(today=datetime.date.today(), now=datetime.datetime.utcnow(), lunch=datetime.time(12,52,5)))
    print b.ToJSON()
    print b.ToXML()
    print Bag(today=datetime.date.today(), now=datetime.datetime.utcnow(), lunch=datetime.time(12,52,5)).ToForm()

    b = Bag.FromXML('<?xml version="1.0" encoding="UTF-8" ?><foo><stuff><![CDATA[ 5 < 3 \n boo yeah]]></stuff></foo>')
    print b
    print b.ToXML()
    print b.ToJSON()

    print "\n unicode tests"
    extra = "\x08c\x00\x02\x038\x18\x02\x00>\x8b\x02\x001&\x03\x00\xe3\x18\x02\x009~\x02\x00\xf2\x11\x03\x00\xdc\x18\x02\x00\xc3}\x02\x00p\x14\x03\x00\x92\r\x02\x00yg\x02\x00T\xff\x02\x00\xd7\x0e\x00\x00t\x14\x00\x00M\x19\x00\x00n\x0e\x00\x00E\x13\x00\x00P\x17\x00\x00\r\x19\x00\x00\xc7!\x00\x00\x86)\x00\x00n)\x00\x00\r8\x00\x00#E\x00\x00\x02\x0b\x00\x00\x00\x04p0.v450\x02\r\x00\x00\x00\x04p0.f29.97\x02\n\x00\x00\x00\x04p0.a96\x02\x0b\x00\x00\x00\x04p1.v550\x02\r\x00\x00\x00\x04p1.f29.97\x02\n\x00\x00\x00\x04p1.a96\x02\x0b\x00\x00\x00\x04p2.v700\x02\r\x00\x00\x00\x04p2.f29.97\x02\n\x00\x00\x00\x04p2.a96\x02\x1e\x00\x00\x00\tthumbinfo128|96|2|0|thumbs"
    uxtra = u"\x08c\x00\x02\x038\x18\x02\x00>\x8b\x02\x001&\x03\x00\xe3\x18\x02\x009~\x02\x00\xf2\x11\x03\x00\xdc\x18\x02\x00\xc3}\x02\x00p\x14\x03\x00\x92\r\x02\x00yg\x02\x00T\xff\x02\x00\xd7\x0e\x00\x00t\x14\x00\x00M\x19\x00\x00n\x0e\x00\x00E\x13\x00\x00P\x17\x00\x00\r\x19\x00\x00\xc7!\x00\x00\x86)\x00\x00n)\x00\x00\r8\x00\x00#E\x00\x00\x02\x0b\x00\x00\x00\x04p0.v450\x02\r\x00\x00\x00\x04p0.f29.97\x02\n\x00\x00\x00\x04p0.a96\x02\x0b\x00\x00\x00\x04p1.v550\x02\r\x00\x00\x00\x04p1.f29.97\x02\n\x00\x00\x00\x04p1.a96\x02\x0b\x00\x00\x00\x04p2.v700\x02\r\x00\x00\x00\x04p2.f29.97\x02\n\x00\x00\x00\x04p2.a96\x02\x1e\x00\x00\x00\tthumbinfo128|96|2|0|thumbs"
    y=Bag(extra=extra)
    z=Bag(extra=uxtra)
    assert y.ToJSON() == z.ToJSON(), "Did not appropriately interpret unicode"
    print "  pass"
