import mechanize
from urllib import urlencode
from urllib2 import URLError, HTTPError
from Bag import Bag

cookies = mechanize.LWPCookieJar()
opener = mechanize.build_opener(mechanize.HTTPCookieProcessor(cookies))
mechanize.install_opener(opener)

class RaceControlClientException(Exception):
    def __init__(self, *args, **kwargs):
        super(RaceControlClientException, self).__init__(*args)
        for key, value in kwargs.items():
            setattr(self, key, value)

class RaceControlClient:
    base_url = ""
    username = ""
    password = ""

    def __init__(self, url, username, password):
        """ Initializes the client with the appropriate username and password.
        @param url - URL to the base of the RaceControl instance
        @param username - username
        @param password - password
        """
        self.base_url = url.strip("/")
        self.username = username
        self.password = password

    def Call(self, api, formData=None, jsonData=None):
        auth_error = False
        if not api.startswith('/'):
            # insure the api starts with a /
            api = '/' + api
        api = "/webAPI" + api
        extra = ""
        if formData != None:
            extra = "?%s" % urlencode(formData)
        req = mechanize.Request(self.base_url + api + extra,timeout=600.0)

        if jsonData != None:
            req.add_header("Content-Type", "application/json")
            req.add_data(jsonData)

        resp = mechanize.urlopen(req)
        resp_read = resp.read().strip()
        if resp.code != 401:
            if resp.code != 200:
                raise RaceControlClientException("Request failed with code (%s)\n" % resp_read)
        else:
            auth_error = True

        # get results
        if not auth_error:
            if resp_read:
                try:
                    respBag = Bag.FromJSON(resp_read)
                except Exception, e:
                    respBag = Bag(resp = resp_read, _resp_msg = repr(e))
            else:
                respBag = Bag()

        return respBag

    def Login(self):
        """ Login to the server
        """
        self.in_login = True
        try:
            bag = self.Call("login", {"user": self.username, "pass": self.password})
        except HTTPError as e:
            error_message = e.read()
            print error_message
            raise e
        print "Logging in", bag
