@echo off
set IMG=registry.gitlab.com/taxilian/racecontrol/server
set CNAME=racecontrol
set SCRIPTDIR=%~dp0
docker pull %IMG%

docker stop %CNAME%
docker rm %CNAME%
docker run ^
    -d --restart=always ^
    --name %CNAME% ^
    -e VIRTUAL_HOST=racecontrol.local ^
    -e VIRTUAL_PORT=80 ^
    --link mysql_1:db ^
    -v %SCRIPTDIR%:/home/docker/code/app/racecontrol5 ^
    -v %SCRIPTDIR%/docker_settings.py:/home/docker/code/app/racecontrol5/settings.py ^
    -p 80:80 ^
    %IMG%
