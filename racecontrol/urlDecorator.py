import sys

from django.core.urlresolvers import RegexURLResolver
from django.conf.urls.defaults import patterns
from django.conf.urls.defaults import url as dcud_url
from django.http import Http404

#
# Annotate a view with the URL that points to it
#

def url(pattern, *args, **kwargs):
    """
    Usage:
    @url(r'^users$')
    def get_user_list(request):
        ...

    @url(r'^info/(.*)/$', name="url-name")
    @render_to('wiki.html')
    def wiki(request, title=''):
        ...
    """
    if "method" in kwargs:
        req_method = kwargs["method"]
        del kwargs["method"]
    else:
        req_method = "GET"
    caller_filename = sys._getframe(1).f_code.co_filename
    module = None
    for m in sys.modules.values():
        if m and '__file__' in m.__dict__ and m.__file__.startswith(caller_filename):
            module = m
            break
    def _wrapper(f):
        if module:
            if 'urlpatterns' not in module.__dict__:
                module.urlpatterns = []
            if '__urlpattern_helpers' not in module.__dict__:
                module.__urlpattern_helpers = {}
            if pattern not in module.__urlpattern_helpers:
                module.__urlpattern_helpers[pattern] = {}
            module.__urlpattern_helpers[pattern][req_method] = f
            def new_f(request, *args, **kwargs):
                if request.method in module.__urlpattern_helpers[pattern]:
                    rf = module.__urlpattern_helpers[pattern][request.method]
                elif "GET" in module.__urlpattern_helpers[pattern]:
                    rf = module.__urlpattern_helpers[pattern]["GET"]
                else:
                    raise Http404
                return rf(request, *args, **kwargs)

            module.urlpatterns += patterns('', dcud_url(pattern, new_f, *args, **kwargs))
            return new_f
        return f
    return _wrapper

#
# Continue the @url decorator pattern into sub-modules, if desired
#

def include_urlpatterns(regex, module):
    """
    Usage:

    # in top-level module code:
    urlpatterns = include_urlpatterns(r'^profile/', 'apps.myapp.views.profile')
    """
    return [RegexURLResolver(regex, module)]

