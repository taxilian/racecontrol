define(["backbone", "underscore", "jquery", "defaults", "datelib"], function(Backbone, _, $, defaults) {

    var Entry = Backbone.Model.extend({
        idAttribute: "station_stationNumber",
        url: function() {
            var url = "/newAPI/events/" + this.collection.eventId + "/participants/" + this.collection.runnerNo + "/entrys";
            if (!this.isNew()) {
                url += "/" + this.id;
            }
            return url;
        },
        getDateTimeIn: function(withS) {
            var time_in = this.get("time_in");
            return time_in && time_in.toString(defaults.dateFormat);
        },
        getGridTimeIn: function(withS) {
            var time_in = this.get("time_in");
            if (withS) {
                return time_in && time_in.toString(defaults.timeFormat);
            } else {
                return time_in && time_in.toString(defaults.shortTimeFormat);
            }
        },
        getGridTimeOut: function(withS) {
            var time_out = this.get("time_out");
            if (withS) {
                return time_out && time_out.toString(defaults.timeFormat);
            } else {
                return time_out && time_out.toString(defaults.shortTimeFormat);
            }
        },
        getDateTimeOut: function(withS) {
            var time_out = this.get("time_out");
            return time_out && time_out.toString(defaults.dateFormat);
        },
        hasValidTime: function() {
            return ((this.has("time_in") && this.get("time_in")) || (this.has("time_out") && this.get("time_out")));
        },
        getValidTime: function() {
            if ((this.has("time_in") && this.get("time_in"))) {
                return this.get("time_in");
            } else if ((this.has("time_out") && this.get("time_out"))) {
                return this.get("time_out");
            }
        },
        parse: function(raw_entry) {
            if (raw_entry.time_in) { raw_entry.time_in = Date.parse(raw_entry.time_in); }
            if (raw_entry.time_out) { raw_entry.time_out = Date.parse(raw_entry.time_out); }
        }
    });

    var Entrys = Backbone.Collection.extend({
        model: Entry,
        url: function() {
            return "/newAPI/events/" + this.eventId + "/participants/" + this.runnerNo + "/entrys";
        },
        parse: function(arr) {
            _.each(arr, Entry.prototype.parse);
            return arr;
        }
    }, {
        list: null,
        fetchAll: function(eventId, runnerNo) {
            var self = this, cache;
            if (!self.list) { self.list = {}; }
            if (!self.list[eventId]) { self.list[eventId] = {}; }
            cache = self.list[eventId][runnerNo];
            if (cache) {
                return cache;
            }
            var tmp = new self();
            tmp.eventId = eventId;
            tmp.runnerNo = runnerNo;
            cache = self.list[eventId][runnerNo] = tmp.fetch().pipe(function() {
                return (self.list[eventId][runnerNo] = tmp);
            });
            return cache;
        }
    });
    Entrys.parse = Entrys.prototype.parse;

    return {
        obj: Entry,
        list: Entrys
    };
});

