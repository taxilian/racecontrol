define(["jquery", "base/AcceptsOneView"], function($, AcceptsOneView) {

    var layout = {
        main: new AcceptsOneView($("#main_content")),
        rightHeader: new AcceptsOneView($("#head-right")),
        centerHeader: new AcceptsOneView($("#head-center"))
    };

    return layout;

});
