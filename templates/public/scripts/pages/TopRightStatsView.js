define(['base/BaseView', 'underscore',
        '_tpl!./jst/TopRightStats.jst'], function(BaseView, _, tpl) {

    var TopRightStatsView = BaseView.extend({
        stats: {
            dropped: 0, total: 0, finished: 0, onCourse: 0
        },
        setStats: function(stats) {
            this.stats = stats;
            this.refresh();
        },

        render: function() {
            var self = this;
            var $el = $(self.el);
            $el.html(tpl({stats: self.stats}));
            return self;
        },

        refresh: function() {
            if (this.isActive()) {
                this.render();
            }
        }
    });

    return TopRightStatsView;
});


