define(["backbone", "jquery", "./AddEditEventView", "models/RaceEvent", "models/Station", "base/AcceptsOneViewDialog"],
function(Backbone, $, AddEditEventView, RaceEvent, Station, Dialog) {

    var AddEditEventPresenter = Backbone.Router.extend({
        initialize: function(options) {
            var self = this;
            self.view = new AddEditEventView();
            self.eventId = options.eventId;
            self.dfd = options.dfd;
            self.view.bind("save", self.onSaveEvent, self);
        },

        present: function() {
            var self = this, eventDfd, stationsDfd;

            if (self.eventId) {
                eventDfd = RaceEvent.obj.fetchById(self.eventId);
                stationsDfd = Station.list.fetchAll(self.eventId);
            } else {
                eventDfd = new RaceEvent.obj({date: new Date()});
                stationsDfd = new Station.list();
            }
            // We set the eventlist to null; when it becomes available we'll update it
            self.view.setEvent(null);
            self.view.setStations(null);
            $.when(eventDfd, stationsDfd).done(function(raceEvent, stations) {
                console.log(raceEvent);
                self.raceEvent = raceEvent;
                self.stations = stations;
                self.view.setEvent(raceEvent);
                self.view.setStations(stations);

                // render!
                var dlg = self.dlg = new Dialog({
                    title: (raceEvent) ? "Edit Event" : "New Event",
                    view: self.view,
                    buttons: ["OK", "Cancel"]
                });
                dlg.doDialog();
                dlg.bind("onButtonClick", function(button) {
                    if (button == "OK") {
                        self.view.saveEvent();
                    } else {
                        dlg.close();
                        self.cancel();
                    }
                });
            });
        },

        onSaveEvent: function(values) {
            var self = this;
            self.raceEvent.set(values);
            self.raceEvent.save().done(function() {
                if (!self.raceEvent.collection) {
                    RaceEvent.list.fetchAll().done(function(list) {
                        if (!list.get(self.raceEvent.id)) {
                            list.add(self.raceEvent);
                        }
                        self.dfd.resolve(self.raceEvent);
                        self.dlg.close();
                    });
                } else {
                    self.dfd.resolve(self.raceEvent);
                    self.dlg.close();
                }
            });
        },

        cancel: function() {
            this.dfd.reject(this.raceEvent);
        }
    }, {
        showEvent: function(eventId) {
            var dfd = new $.Deferred();
            var obj = new this({eventId:eventId, dfd:dfd});
            obj.present();
            return dfd.promise();
        }
    });

    return AddEditEventPresenter;
});



