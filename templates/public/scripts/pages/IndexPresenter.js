define(["backbone", "jquery", "./IndexView", "models/RaceEvent", "dialogs/AddEditEventPresenter"],
function(Backbone, $, DefaultView, RaceEvent, AddEditEventPresenter) {

    var IndexPresenter = Backbone.Router.extend({
        initialize: function(options) {
            var self = this;
            self.view = new DefaultView();
            self.view.bind("edit", self.onEdit, self);
            self.view.bind("del", self.onDel, self);
            self.view.bind("open", self.onOpenEvent, self);
            self.view.bind("new", self.onNew, self);

            self.eventListDfd = RaceEvent.list.fetchAll();
        },

        present: function(container) {
            var self = this;

            // We set the eventlist to null; when it becomes available we'll update it
            self.view.setEventList(null);
            $.when(self.eventListDfd).done(function(evtList) {
                self.view.setEventList(evtList);
            });

            // If the eventlist was already loaded it will already be set
            container.setView(self.view);
        },

        onEdit: function(eventId) {
            var dfd = AddEditEventPresenter.showEvent(eventId);
            $.when(dfd).done(function(raceEvent) { /* NOOP */ }).fail(function(){ /* Cancelled */ });
        },

        onDel: function(eventId) {
            console.log("delete");
        },

        onNew: function() {
            AddEditEventPresenter.showEvent(null);
        },

        onOpenEvent: function(eventId) {
            this.navigate("grid/" + eventId, true);
        }
    });

    return new IndexPresenter();
});


