define(["underscore", "jquery", '_tpl!./jst/AlertWithButtons.jst', "ext/jquery.pnotify" ],
    function(_, $, alertTpl) {

    var stack_topleft = {"dir1": "down", "dir2": "right", "push": "top", "firstpos1": 0, "firstpos2": 15};
    var animation = {
        effect_in: "drop",
        options_in: {easing: "easeOutElastic"},
        effect_out: "fade"
    };
    var notify = {
        clearAll: function() {
            $.pnotify_remove_all();
        },

        // text - compiled template; Note that the title, content, etc., 
        // is preset in your template
        // buttonArray - optional .. will default to sensible defaults
        alertWithButtons: function(title, paragraphsArray, buttonsArray) {
            notify.clearAll();
            var dfd = new $.Deferred();
            var content = alertTpl({title:title, paragraphs: paragraphsArray,
                                buttons: buttonsArray});
            var notice =
                $.pnotify({
                    pnotify_text: content,
                    pnotify_notice_icon: '',
                    pnotify_width: 'auto',
                    pnotify_animation: animation,
                    pnotify_hide: false,
                    pnotify_closer: false,
                    pnotify_sticker: false,
                    pnotify_insert_brs: false,
                    pnotify_stack: stack_topleft,
                    pnotify_addclass: "stack-topleft"
                 });
            
            notice.find("button").button().click(function(evt){
                var self = this;
                evt.preventDefault();
                var buttonValue = $(this).attr("value");
                notice.pnotify_remove();
                dfd.resolve(buttonValue);
            });
            return dfd.promise();
        },
        info: function(title, text) {
            var dfd = new $.Deferred();
            $.pnotify({
                pnotify_after_close: function() { dfd.resolve(); },
                pnotify_title: title,
                pnotify_text: text,
                pnotify_type: "info",
                //pnotify_animation: animation,
                pnotify_shadow: true,
                pnotify_stack: stack_topleft,
                pnotify_addclass: "stack-topleft"
            });
            return dfd.promise();
        }, warn: function(text) {
            var dfd = new $.Deferred();
            $.pnotify({
                pnotify_after_close: function() { dfd.resolve(); },
                pnotify_title: "Warning",
                pnotify_text: text,
                pnotify_animation: animation,
                pnotify_shadow: true,
                pnotify_stack: stack_topleft,
                pnotify_addclass: "stack-topleft"
            });
            return dfd.promise();
        }, error: function(text) {
            var dfd = new $.Deferred();
            $.pnotify({
                pnotify_after_close: function() { dfd.resolve(); },
                pnotify_title: "Error",
                pnotify_text: text,
                pnotify_animation: animation,
                pnotify_type: "error",
                pnotify_shadow: true,
                pnotify_stack: stack_topleft,
                pnotify_addclass: "stack-topleft"
            });
            return dfd.promise();
        }, debugMsg: function(text) {
            var dfd = new $.Deferred();
            $.pnotify({
                pnotify_after_close: function() { dfd.resolve(); },
                pnotify_title: "Debug Message",
                pnotify_text: text,
                pnotify_animation: animation,
                pnotify_type: "info",
                pnotify_shadow: true,
                pnotify_stack: stack_topleft,
                pnotify_addclass: "stack-topleft"
            });
            return dfd.promise();
        }, debugObj: function(obj) {
            var dfd = new $.Deferred();
            $.pnotify({
                pnotify_after_close: function() { dfd.resolve(); },
                pnotify_title: "Debug Message",
                pnotify_text: JSON.stringify(obj),
                pnotify_animation: animation,
                pnotify_type: "info",
                pnotify_shadow: true,
                pnotify_stack: stack_topleft,
                pnotify_addclass: "stack-topleft"
            });
            return dfd.promise();
        }
    };

    // Make this global
    window.notify = notify;
    window.alert = function(msg) {
        $.pnotify({
            pnotify_title: "Alert",
            pnotify_text: msg,
            pnotify_animation: animation,
            pnotify_shadow: true
        });
    };
    if (!window.live) {
        if (!window.console) {
            window.console = {};
        }
        if (!window.console.log) {
            window.console.log = notify.debugMsg;
            window.console.dir = notify.debugObj;
        }
    } else {
        notify.debugMsg = function() {};
    }
    return notify;
});
