from ..models import *
from django.db.models import Q
from zoneWebAPI import zoneWebAPI
from django.conf import settings
from django.shortcuts import redirect
from django.http import HttpResponse
from pyzoop import zone
from pyzoop import login_required, ReturnsJSON
from django.contrib.auth.views import login, logout_then_login
from datetime import datetime, timedelta
import json
import os
import csv

attrs = ['years', 'months', 'days', 'hours', 'minutes', 'seconds']
human_readable = lambda delta: [
    '%d %s' % (getattr(delta, attr), getattr(delta, attr) > 1 and attr or attr[:-1])
        for attr in attrs if getattr(delta, attr)
]

from dateutil.relativedelta import relativedelta

def calcTimeStr(startTime, finishTime, short):
    delta = finishTime-startTime
    total_duration_in_seconds = delta.days * 24 * 60 * 60 + delta.seconds
    seconds = int(total_duration_in_seconds)
    hours = seconds / 60 / 60
    minutes = str(seconds / 60 % 60).rjust(2,'0')
    seconds = str(seconds % 60).rjust(2,'0')

    if short:
        return "%s:%s:%s" % (hours, minutes, seconds)
    else:
        return "%s hour%s, %s minute%s, %s second%s" % (hours, '' if hours == 1 else 's', minutes, '' if minutes == 1 else 's', seconds, '' if seconds == 1 else 's')

def getTimeFromNow(dt):
    delta = datetime.now()-dt
    parts = human_readable(relativedelta(seconds=delta.total_seconds()))
    return ", ".join(parts)

class zoneDefault(zone):
    def __init__(self):
        super(zoneDefault,self).__init__(webAPI=zoneWebAPI())

    def initZone(self, request, pathList):
        pass

    def initPages(self, request, path):
        pass

    def page_index(self, request, pathList):
        return self.page_default(request, ["index"])

    def page_default(self, request, pathList):
        pathList[-1] = "%s.html" % pathList[-1]

        filename = ""
        for segment in pathList:
            filename = os.path.join(filename, segment)
        return self.render_to_response(filename, {})

    def page_viewers(self, request, pathList):
        events = RaceEvent.objects.all();
        return self.render_to_response("viewers.html", {"events": events})

    def page_lookup(self, request, pathList):
        evtId = request.GET.get("evt", -1)
        name = request.GET.get("name", "")
        print name
        names = name.split(" ")
        print "Names", names, "Name", name
        lookup = None
        num = False
        filter = None
        if names:
            for cur in names:
                if filter == None:
                    filter = Q(firstNameSx__startswith=soundex(cur))
                else:
                    filter |= Q(firstNameSx__startswith=soundex(cur))
                filter |= Q(lastNameSx__startswith=soundex(cur))
                filter |= Q(firstName__startswith=cur)
                filter |= Q(lastName__startswith=cur)
                try:
                    filter |= Q(bibNumber=int(cur))
                except:
                    num = False
            lookup = Participant.objects.filter(Q(raceEvent__pk=evtId) & filter).order_by("lastName", "firstName")

        print lookup
        return self.render_to_response("event/html_lookup.html", {"lookup": lookup, "evt": evtId, "name": name})

    def page_ulstrasignupcsv(self, request, pathList):
        evtId = request.GET.get("evt", "")
        participants = Participant.objects.filter(raceEvent__pk=evtId).toDict()
        raceEvent = RaceEvent.objects.get(pk=evtId)
        entries = Entry.objects.select_related("participant__bibNumber", "station__stationNumber").filter(participant__raceEvent__uuid=evtId)

        pDict = Bag(participants)
        for e in entries:
            if e.station.stationNumber == raceEvent.startStation:
                pDict[e.participant.bibNumber]["startTime"] = e.time_out
            elif e.station.stationNumber == raceEvent.finishStation:
                pDict[e.participant.bibNumber]["finishTime"] = e.time_in

        for (k, item) in pDict.iteritems():
            if "startTime" in item and item["startTime"] and "finishTime" in item and item["finishTime"]:
                item["totalShort"] = calcTimeStr(item["startTime"], item["finishTime"], True)
                item["duration"] = item["finishTime"] - item["startTime"]
            else:
                item["duration"] = timedelta(days=7)
                item["totalShort"] = 0

        items = (p for (k, p) in pDict.iteritems() if p["lastName"] != 'Participant')

        items = sorted(items, key=lambda i:i["duration"], reverse=False)

        response = HttpResponse(content_type='text/csv')
        #response['Content-Disposition'] = 'attachment; filename="somefilename.csv"'

        writer = csv.writer(response)
        writer.writerow(['place', 'first', 'last', 'gender', 'age', 'city', 'state', 'bib', 'time', 'status'])
        place = 1
        skip = 0
        lastDuration = None
        for i in items:
            city, state = i["home"].split(',')
            status = 1
            if i['totalShort'] == 0:
                status = 2 if i['dnfStation'] == raceEvent.startStation else 3
            writer.writerow([place if i['totalShort'] else '', i['firstName'], i['lastName'], i['sex'], i['age'], city, state, i['bibNumber'], i["totalShort"], status])
            if lastDuration != i["duration"]:
                place += 1 + skip
                skip = 0
            else:
                skip += 1
            lastDuration = i["duration"]

        return response

    def page_resultcsv(self, request, pathList):
        evtId = request.GET.get("evt", "")
        participants = Participant.objects.filter(raceEvent__pk=evtId).toDict()
        raceEvent = RaceEvent.objects.get(pk=evtId)
        entries = Entry.objects.select_related("participant__bibNumber", "station__stationNumber").filter(
            Q(station__stationNumber=raceEvent.startStation) | Q(station__stationNumber = raceEvent.finishStation), participant__raceEvent__uuid=evtId)

        pDict = Bag(participants)
        for e in entries:
            if e.station.stationNumber == raceEvent.startStation:
                pDict[e.participant.bibNumber]["startTime"] = e.time_out
            else:
                pDict[e.participant.bibNumber]["finishTime"] = e.time_in

        for (k, item) in pDict.iteritems():
            if "startTime" in item and item["startTime"] and "finishTime" in item and item["finishTime"]:
                item["totalShort"] = calcTimeStr(item["startTime"], item["finishTime"], True)
                item["duration"] = item["finishTime"] - item["startTime"]

        items = (p for (k, p) in pDict.iteritems() if 'duration' in p and p["duration"])

        items = sorted(items, key=lambda i:i["duration"], reverse=False)

        response = HttpResponse(content_type='text/csv')
        #response['Content-Disposition'] = 'attachment; filename="somefilename.csv"'

        writer = csv.writer(response)
        writer.writerow(['Ranking', 'Time', 'Family name', 'First name', 'Gender', 'Birthdate', 'Nationality', 'Bib number', 'City'])
        place = 1
        skip = 0
        lastDuration = None
        for i in items:
            writer.writerow([place, i["totalShort"], i["lastName"], i["firstName"], i["sex"], '', 'USA', i["bibNumber"], i["home"].split(',')[0]])
            if lastDuration != i["duration"]:
                place += 1 + skip
                skip = 0
            else:
                skip += 1
            lastDuration = i["duration"]

        return response

    def page_viewRunner(self, request, pathList):
        bibNo = request.GET.get("runnerNumber", -1)
        evtId = request.GET.get("evt", "")
        participant = Participant.objects.get(bibNumber=bibNo,raceEvent__pk=evtId)
        entrys = Entry.objects.select_related("station__stationNumber", "station__name", "station__distance").filter(participant=participant)
        outP = participant.toDict()
        entrys = entrys.toDict()
        raceEvent = RaceEvent.objects.get(pk=evtId)
        stations = Station.objects.filter(raceEvent__pk=evtId)
        print stations

        startTime = None
        finishTime = None

        outEntries = {}
        for s in stations:
            entry = entrys[s.stationNumber] if s.stationNumber in entrys.keys() else None
            e = {}
            e["distance"] = s.distance
            e["stationNumber"] = s.stationNumber
            e["name"] = s.name
            if s.stationNumber == raceEvent.startStation:
                if entry and entry["time_out"]:
                    startTime = entry["time_out"]
            else:
                e["time_in"] = entry["time_in"].strftime("%H:%M:%S") if entry and entry["time_in"] else "No Record"
                if entry and entry["time_in"]:
                    outP["lastAction"] = "entering" if s.stationNumber != raceEvent.finishStation else "finishing the race at"
                    outP["lastStation"] = s.stationNumber
                    outP["lastActionTime"] = e["time_in"]
                    outP["lastActionDelta"] = getTimeFromNow(entry["time_in"])
            if s.stationNumber == raceEvent.finishStation:
                if entry and entry["time_in"]:
                    finishTime = entry["time_in"]
            else:
                e["time_out"] = entry["time_out"].strftime("%H:%M:%S") if entry and entry["time_out"] else "No Record"
                if entry and outP["dnfStation"] == s.stationNumber:
                    e["time_out"] = "DROPPED"
                if entry and entry["time_out"]:
                    outP["lastAction"] = "leaving"
                    outP["lastStation"] = s.stationNumber
                    outP["lastActionTime"] = e["time_out"]
                    outP["lastActionDelta"] = getTimeFromNow(entry["time_out"])
            outEntries[s.stationNumber] = e

        outP["entrys"] = outEntries

        if startTime and finishTime:
            delta = finishTime-startTime
            total_duration_in_seconds = delta.days * 24 * 60 * 60 + delta.seconds
            seconds = int(total_duration_in_seconds)
            hours = seconds / 60 / 60
            minutes = str(seconds / 60 % 60).rjust(2,'0')
            seconds = str(seconds % 60).rjust(2,'0')

            outP["totalShort"] = calcTimeStr(startTime, finishTime, True)
            outP["totalLong"] = calcTimeStr(startTime, finishTime, False)


        return self.render_to_response("event/html_lookup_results.html", {"participant": outP})
