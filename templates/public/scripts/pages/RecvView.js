define(['base/BaseView', 'underscore',
        '_tpl!./jst/Recv.jst', '_tpl!./jst/RecvEntries.jst'],
        function(BaseView, _, tpl, entriesTpl) {

    var RecvView = BaseView.extend({
        event: null,
        runnerList: null,
        stationList: null,
        sortFunc: null,
        listening: false,

        chunkSize: 10,

        deAmp: null,

        last80Chars: "",

        initialize: function() {
            this.em = $("#emRuler").width();
            _.bindAll(this, "refresh", "onFileUpdate", "getRunnerInfo");
            $(window).resize(this.autoSize);
        },

        rxText: function(rxString) {
            var self = this;
            self.last80Chars = (self.last80Chars + rxString).replace(/[\r\n]/g, ' ').substr(-80);

            $(self.el).find(".mt63Buffer").text(self.last80Chars);
        },

        clearData: function() {
            var self = this;
            if (self.runnerList) {
                // self.runnerList.unbind("change", self.rlistChange);
                // self.runnerList.unbind("add", self.refresh);
            }
        },

        autoSize: function() {
            if (!this.isActive()) { return; }
            var em = this.em;
            // var availSize = $(window).height();

            // $("#gridContainer").height(availSize - (15 * em));

            // var sum = 20;
            // $("#gridHeader li").each(function() { sum += $(this).outerWidth(); });
            // $("#gridHolder").css("min-width", sum + "px");
        },

        setData: function(evt, rlist, files, stationAmp) {
            var self = this;
            self.rlist = rlist;
            if (self.files != files) {
                files.unbind(self.onFileUpdate);
            }
            if (self.evt != evt) {

            }
            this.evt = evt;
            this.files = files;
            this.stationAmp = stationAmp;

            files.bind("update", function() {
                self.refresh();
            }); 
            files.bind("listenState", function(state) {
                self.listening = state;
                self.refresh();
            });
            if (self.isActive()) {
                self.render();
            }
        },

        onFileUpdate: function() {
            this.refresh();
        },

        events: {
            "click #startStop":                      "onStartStopClicked",
            "click .import_file":                    "onFileImport",
            "click .transmitAmp":                    "onTransmitAmp"
        },

        onStartStopClicked: function(evt) {
            var self = this;
            evt.preventDefault();

            self.trigger("startStop");
        },

        onTransmitAmp: function(evt) {
            evt.preventDefault();

            var $target = $(evt.target).closest("button");
            var blocks = [];
            var chunk = $target.attr('chunk');
            if (chunk == "custom") {
                blocks = $(this.el)
                    .find("#blocksToTransmit")
                    .val()
                    .split(',')
                    .map(function(val) { return val.trim(); });
            } else {
                chunk = parseInt(chunk, 10);
                var blockStart = this.chunkSize * chunk + 1;
                var blockMax = Math.min(this.stationAmp.getDataBlockCount(), blockStart + this.chunkSize - 1); 
                for (var i = blockStart; i <= blockMax; ++i) {
                    blocks.push(i);
                }
            }

            this.trigger("transmitAmp", blocks);
        },

        onFileImport: function(evt) {
            evt.preventDefault();
            var $target = $(evt.target).closest("button");
            var cid = $target.attr('cid');

            var file = this.files.getByCid(cid);
            if (!file) {
                alert("Couldn't find associated file!");
                return;
            }

            var targetStation = parseInt($(this.el).find("#import_to_" + cid).val().trim(), 10);
            if (isNaN(targetStation)) {
                alert("Invalid station!" + targetStation);
                return;
            }

            if (targetStation != file.get('station')) {
                if (!confirm("Are you sure you want to import to a different station?")) {
                    return;
                }
            }

            this.trigger("importFile", {
                file: file,
                targetStation: targetStation
            });
        },

        getRunnerInfo: function(bibNo) {
            /**
             * @type {Backbone.Model}
             */
            var runner = this.rlist.find(function(r) {
                return r.get('bibNumber') == bibNo;
            });

            if (runner) {
                return runner.toJSON();
            } else {
                return {
                    raceEvent_id: self.eventId,
                    bibNumber: bibNo,
                    firstName: "Unknown",
                    lastName: "Participant"
                };
            }
        },

        render: function() {
            var self = this;
            var $el = $(self.el);
            // $el.addClass("newgrid");
            // $el.attr("id", "gridMain");
            if (!self.files) {
                return self; // not ready to render yet
            }
            $el.html(tpl({
                last80Chars: self.last80Chars,
                files: self.files,
                listening: self.listening,
                entriesTpl: entriesTpl,
                rlist: self.rlist,
                getRunnerInfo: self.getRunnerInfo,
                stationAmp: self.stationAmp,
                chunkSize: self.chunkSize
            }));
            self.$("button").button();
            //$el.html(tpl({events: self.eventList}));
            return self;
        },

        refresh: function() {
            if (this.isActive()) {
                this.render();
            }
        },

        deactivate: function() {
            this.clearData();
            this.trigger("deactivate");
        },

        eventChange: function() {
        },
        rlistChange: function(runner) {
            // var entryId = "#row_" + runner.id;
            // this.$(entryId).html(runnerEntryTpl({
            //     runner: runner,
            //     stations: this.stationList,
            //     evt: this.event,
            //     timeFormat: timeFormatTpl,
            //     ordinalSuffix: ordinalSuffix
            // }));
        },

    });

    return RecvView;
});


