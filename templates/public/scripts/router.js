define(["./base/BaseRouter", "./MainLayout",
       "pages/IndexPresenter", "pages/GridPresenter", "pages/TopRightStatsPresenter",
       "pages/RecvPresenter"],
       function(BaseRouter, MainLayout,
                IndexPresenter, GridPresenter, TopRightStatsPresenter, RecvPresenter) {

    var MainRouter = BaseRouter.extend({

        routes : {
            "": "index",
            "index": "index",
            "grid/:eventId": "grid",
            "ampRecv/:eventId": "ampReceive"
        },

        initialize: function() {
        
        },

        beforeRoute: function() {
            // Added by BaseRotuer, executes before each route
        },
        afterRoute: function() {
            // Added by BaseRouter, executes after each route
        },

        index: function() {
            IndexPresenter.present(MainLayout.main);
            MainLayout.rightHeader.setView(null);
        },

        grid: function(eventId) {
            GridPresenter.present(MainLayout.main, eventId);
            TopRightStatsPresenter.present(MainLayout.rightHeader, eventId);
        },

        ampReceive: function(eventId) {
            RecvPresenter.present(MainLayout.main, eventId);
        }
    });

    return new MainRouter();

});
