define(["backbone", "jquery", "./StartTimeView", "models/Participant", "models/RaceEvent", "base/AcceptsOneViewDialog", "DateManager"],
function(Backbone, $, StartTimeView, Participant, RaceEvent, Dialog, DateManager) {

    var StartTimePresenter = Backbone.Router.extend({
        initialize: function(options) {
            var self = this;
            self.view = new StartTimeView();
            self.eventId = options.eventId;
            self.dfd = options.dfd;
            self.view.bind("save", self.onSave, self);
        },

        present: function() {
            var self = this,
                eventDfd = RaceEvent.obj.fetchById(self.eventId);

            $.when(eventDfd).pipe(function(evt) {
                self.evt = evt;

                // render!
                var dlg = self.dlg = new Dialog({
                    title: "Set start time",
                    view: self.view,
                    buttons: ["OK", "Cancel"]
                });
                dlg.doDialog();
                dlg.bind("onButtonClick", function(button) {
                    if (button == "OK") {
                        self.view.onSaveClicked();
                    } else {
                        dlg.close();
                        self.cancel();
                    }
                });
            });
        },

        onSave: function(newTime, oldTime) {
            var self = this;
            var curDate = DateManager.getCurrentDate();
            var oldTimeOut = oldTime ? Date.parse(curDate + " " + oldTime) : null;
            var newTimeOut = newTime ? Date.parse(curDate + " " + newTime) : null;

            $.when(Participant.list.fetchAll(self.eventId)).pipe(function(rlist) {
                var startStation = self.evt.get("startStation");
                if (!startStation && startStation !== 0) {
                    notify.error("No start station");
                    return;
                }
                var updateDfds = rlist.map(function(runner) {
                    return runner.getOrCreateEntry(startStation).pipe(function(entry) {
                        if (!oldTimeOut || !oldTimeOut.getTime() || (entry.get("time_out") && entry.get("time_out").getTime() == oldTimeOut.getTime())) {
                            entry.set({time_out: newTimeOut});
                            return entry.save().done(function() {
                                notify.info("Updated start time for runner #" + runner.id);
                            });
                        }
                        return $.when(null);
                    });
                });
                return $.when.apply(null, updateDfds).done(function() {
                    self.dfd.resolve();
                    self.dlg.close();
                });
            }).fail(function() {
                notify.error("Error updating runner times");
                console.log(arguments);
            });
        },

        cancel: function() {
            this.dfd.reject(this.event);
        }
    }, {
        showStartTime: function(eventId, rId) {
            var dfd = new $.Deferred();
            var obj = new this({eventId: eventId, rId:rId, dfd:dfd});
            obj.present();
            return dfd.promise();
        }
    });

    return StartTimePresenter;
});




