import os, sys
sys.path.append("/home/docker/code/app/")
sys.path.append("/home/docker/code/app/racecontrol5/")
sys.path.append("/home/docker/code/app/racecontrol5/racecontrol/")
os.environ["DJANGO_SETTINGS_MODULE"] = "racecontrol5.settings"

import django.core.handlers.wsgi

application = django.core.handlers.wsgi.WSGIHandler()
