define(['backbone'], function(Backbone) {

    var BaseView = Backbone.View.extend({
        divList: {},
        _lastClass: null,
        _isActive: false,

        _setActive: function(active) {
            this._isActive = active;
        },
        isActive: function() { return this._isActive; },

        setClass: function(cls) {
            var $el = $(this.el);
            if (this._lastClass !== null) {
                $el.removeClass(this._lastClass);
            }
            if (cls !== null) {
                $el.addClass(cls);
            }
            this._lastClass = cls;
        },

        refresh: function() {
            // No-op by default. This is called when the page is "rendered" but the view is
            // already in place and most likely doesn't need to be re-rendered.
        },
        deactivate: function() {
            // No-op by default. This is called before the view is removed from the page
        }
    });
    return BaseView;
});
