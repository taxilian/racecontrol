if __name__ == "__main__":
    import settings
    from django.core.management import setup_environ
    setup_environ(settings)

from racecontrol.models import *
import sqlite3
from Bag import Bag
import datetime
import re
import xlrd, sys

kFirst = 0
#kMiddle = 5
kLast = 1
kSex = 2
kAge = 3
kCity = 7
kState = 8
kBib = 13

# Expects cells in the following order:
# first, middle, last, sex, age,
def sanitize(row, n):
    return str(row[n].value).strip()
def sanNum(row, n):
    return int(row[n].value)

def checkUTF8sex(sex):
    if sex == 'M' or sex == u'M':
        return 'M'
    if sex == 'F' or sex == u'F':
        return 'F'

def importRunnerData(data, event):
    patternRunnerNo = '(\d+)\ *([a-zA-Z]?)'
    for rowNum in range(1, data.nrows):
        row = data.row(rowNum)
        try:
            first = sanitize(row, kFirst)
            middle = None #sanitize(row, kMiddle)
            first = first if not middle else "%s %s" % (first, middle)
            last = sanitize(row, kLast)
            sex = sanitize(row, kSex)[0:1].upper()
            try:
                age = sanNum(row, kAge)
            except:
                age = -1
            city = sanitize(row, kCity)
            state = sanitize(row, kState)[0:2].upper()
            home = "%s, %s" % (city, state)

            bib = sanitize(row, kBib)
            print "Bib:", bib
            matches = re.search(patternRunnerNo, bib)
            if matches:
                bib = int(matches.group(1))
                team = matches.group(2)
            else:
                print "entries:", row[15], row[0:4]
                raise ValueError("Runner Withdrew")

            participant, existed = Participant.objects.get_or_create(raceEvent=event, bibNumber=bib)
            participant.firstName = first
            participant.lastName = last
            participant.home = home
            participant.age = age
            participant.team = team
            if participant.age < 1:
                participant.age = None
            participant.sex = checkUTF8sex(sex)
            if participant.sex != "M" and participant.sex != "F":
                print "No sex found for the row:", row
                participant.sex = None
            participant.save()
        except Exception, ex:
            print "Error processing participant: ", ex
            for idx in range(len(row)):
                print idx,": ",row[idx]
            raise ex

def run(raceEventId, filename):
    if not raceEventId:
        print "Enter the name of the race event id:"
        raceEventId = raw_input("> ")
    raceEvent = RaceEvent.objects.get(uuid=raceEventId)

    if not filename:
        print "Enter name of excel file to process:"
        filename = raw_input("> ")
    try:
        workbook = xlrd.open_workbook(filename)
        worksheet = workbook.sheet_by_index(0)  # grab the first worksheet
        importRunnerData(worksheet, raceEvent)
    except Exception, ex:
        print "Error processing Excel file.", ex

if __name__ == "__main__":
    evtId = sys.argv[1] if len(sys.argv) > 1 else None
    fname = sys.argv[2] if len(sys.argv) > 2 else None
    run(evtId, fname)
