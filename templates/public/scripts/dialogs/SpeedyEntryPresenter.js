define(["backbone", "jquery", "./SpeedyEntryView", "DateManager",
       "models/Participant", "models/Station", "models/RaceEvent", "base/AcceptsOneViewDialog",
       "dialogs/AddEditStationPresenter", "lib/ConfirmDialog"],
function(Backbone, $, SpeedyEntryView, DateManager, Participant, Station, RaceEvent, Dialog, AddEditStation, Confirm) {

    // Initialize the hotkey list
    var hotkeyMap = [];
    _.times(10, function() { hotkeyMap.push(null); });

    function bindHotkey(obj) {
        for(var i = 0; i < hotkeyMap.length; ++i) {
            if (!hotkeyMap[i]) {
                // We have found a blank hotkey
                hotkeyMap[i] = obj;
                return i+1;
            }
        }
        return "N/A";
    }
    function releaseHotkey(key) {
        if (key == "N/A") return;
        hotkeyMap[key-1] = null;
    }

    SpeedyEntryView.registerHotkeyHandler(function(key) {
        if (key == "N") {
            require("pages/GridPresenter").onSomethingClicked("speedyEntry");
            return true;
        }
        var numIdx = Number(key);
        if (!isNaN(numIdx)) {
            if (hotkeyMap[numIdx-1])
                hotkeyMap[numIdx-1].activate();
            return true;
        }
    });

    var SpeedyEntryPresenter = Backbone.Router.extend({
        initialize: function(options) {
            var self = this;
            self.view = new SpeedyEntryView();
            self.eventId = options.eventId;
            self.stationNo = options.stationNo;
            self.dfd = options.dfd;
            self.view.bind("stationSet", self.handleStationSet, self);
            self.view.bind("runnerSelected", self.handleRunnerSet, self);
            self.view.bind("saveRunnerToList", self.handleRunnerList, self);
            self.view.bind("revertEntry", self.revertEntry, self);
            self.view.bind("cancelRunner", self.cancelRunner, self);
            self.key = bindHotkey(self);
        },

        present: function() {
            var self = this;
            self.historyList = [];
            self.view.historyList = self.historyList; // both now point to the same list
            var title = "Speedy Entry Form: Choose station";
            title += " (Alt+" + (self.key) + ")";
            var dlg = self.dlg = new Dialog({
                title: title,
                view: self.view,
                buttons: ["Close"],
                closeOnEscape: false,
                modal: false
            });
            dlg.doDialog();
            dlg.bind("onButtonClick", function(button) {
                dlg.close();
            });
            dlg.bind("close", function() {
                releaseHotkey(self.key);
            });
        },
        activate: function() {
            this.view.focus();
        },

        handleStationSet: function(stationNum) {
            var self = this;
            self.stationNo = stationNum;
            var stationDfd = Station.obj.fetchById(self.eventId, self.stationNo);
            var eventDfd = RaceEvent.obj.fetchById(self.eventId);
            $.when(stationDfd, eventDfd).done(function(station, event) {
                self.event = event;
                if(station) {
                    var title = "Speedy Entry Form for " + stationNum + ": " + station.get("name");
                    title += " (Alt+" + self.key + ")";
                    self.dlg.updateDialogTitle(title);
                    // Update view and re-render
                    self.view.stationId = Number(stationNum);
                    self.view.showTimeOut = !event.isFinishStation(station.id);
                    self.view.showTimeIn = !event.isStartStation(station.id);
                    self.view.render();
                } else {
                    AddEditStation.showStation(event.id, stationNum).done(function() {
                        self.handleStationSet(stationNum);
                }).fail(function() {
                        // Do nothing
                    });
                }
            });
        },

        handleRunnerSet: function(runnerNum) {
            var self = this;
            if (runnerNum === "") { return false; }
            var runnerDfd = Participant.obj.fetchById(self.eventId, runnerNum);
            $.when(runnerDfd).done(function(runner) {
                // Update view and re-render
                if (!runner) {
                    runner = new Participant.obj({
                        raceEvent_id: self.eventId,
                        bibNumber: runnerNum,
                        firstName: "Unknown",
                        lastName: "Participant"
                    });
                }
                // show times for runner, put runner in view
                self.view.updateRowWithRunner(runner);
            });
        },

        handleRunnerList: function(data) {
            var self = this, runner = data.runner;
            var curDate = DateManager.getCurrentDate();
            var newTimeIn = data.time_in ? Date.parse(curDate + " " + data.time_in) : null;
            var newTimeOut = data.time_out ? Date.parse(curDate + " " + data.time_out) : null;
            var record = _.find(self.historyList, function(item) { return item.runner.id == runner.id; });

            var runnerReadyDfd = runner;
            var runnerListDfd = Participant.list.fetchAll(self.eventId);
            if (!runner.collection) {
                $.when(runnerListDfd).done(function(rlist) { rlist.add(runner); });
                runnerReadyDfd = runner.save().pipe(function() { return runner; });
            }

            $.when(runnerReadyDfd, runnerListDfd).done(function(runner, runnerList) {
                runner.getOrCreateEntry(data.stationId).done(function(entry) {
                    if (!record) {
                        record = {
                            runner: runner,
                            entry: entry,
                            original: runner.getTimes(data.stationId)
                        };
                    }
                    entry.set({
                        time_in: newTimeIn,
                        time_out: newTimeOut
                    });
                    entry.save().fail(function() {
                        notify.warn("Did not save the change to " + runner.id);
                    });
                    self.historyList.unshift(record);
                    self.historyList = _.uniq(self.historyList, false, function(r) {
                        return r.runner.id;
                    });
                    self.view.historyList = self.historyList; // both now point to the same list
                    self.view.refresh(); // update
                });
            });
        },
        
        cancelRunner: function() {
            this.view.refresh();
        },

        revertEntry: function(id) {
            var self = this;
            var item = _.find(self.historyList, function(i) { return i.runner.id == id; });
            if (!item) { return false; }

            var text = "<p>The entry for Runner #" + id + " (" + item.runner.formatName() + ")";
            text += " at station #" + item.stationId + " will be reverted back to the following: <p>";
            var orig_tin = item.original[0] || "__:__:__";
            if (orig_tin instanceof Date) { orig_tin = orig_tin.toString("HH:mm:ss"); }
            var orig_tout = item.original[1] || "__:__:__";
            if (orig_tout instanceof Date) { orig_tout = orig_tout.toString("HH:mm:ss"); }
            text += "<p>Time in: " + orig_tin + ", Time out: " + orig_tout + "</p>";
            text += "<p>Continue?</p>";

            Confirm.confirm("Confirm", text, false).done(function(resp) {
                if (resp) {
                    item.runner.getOrCreateEntry(self.stationNo).done(function(entry) {
                        entry.set({
                            time_in: item.original[0],
                            time_out: item.original[1]
                        });
                        entry.save().done(function() {
                            self.historyList = _.without(self.historyList, item);
                            self.view.historyList = self.historyList; // both now point to the same list
                            self.view.refresh(); // update
                        });
                    });
                }
            });
        }
    }, {
        showDialog: function(eventId, sId) {
            var dfd = new $.Deferred();
            var obj = new this({eventId: eventId, dfd:dfd});
            obj.present();
            return dfd.promise();
        }
    });

    return SpeedyEntryPresenter;
});




