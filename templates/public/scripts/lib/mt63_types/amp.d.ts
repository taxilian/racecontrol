export declare const lzmaCompressedPrefix = "\u0001LZMA";
export declare const MODIFIED_TIME_FORMAT = "YYYYMMDDHHmmss";
export declare enum LTypes {
    FILE = "FILE",
    ID = "ID",
    DTTM = "DTTM",
    SIZE = "SIZE",
    DESC = "DESC",
    DATA = "DATA",
    PROG = "PROG",
    CNTL = "CNTL"
}
export declare enum ControlWord {
    EOF = "EOF",
    EOT = "EOT"
}
export declare enum BaseEncode {
    b64 = "base64",
    b91 = "base91"
}
export declare enum CompressionType {
    LZMA = "LZMA"
}
export interface IOptions {
    compression?: CompressionType | false;
    forceCompress?: boolean;
    base?: BaseEncode;
    fromCallsign?: string;
    toCallsign?: string;
    filename: string;
    fileModifiedTime: Date;
    inputBuffer: string;
    blkSize: number;
    skipProgram?: boolean;
    useEOF?: boolean;
    useEOT?: boolean;
}
export declare class Amp {
    static getHash(filename: string, modified: Date, compressed: boolean, baseConversion: BaseEncode | '', blockSize: number): string;
    private fromCallsign;
    private toCallsign;
    private filename;
    private fileModifiedTime;
    private inputBuffer;
    private blkSize;
    private PROGRAM;
    private VERSION;
    private base;
    private compression;
    private forceCompress;
    private blocks;
    hash: string;
    private dataBlockCount;
    private skipProgram;
    private useEOF;
    private useEOT;
    private receivedFiles;
    constructor(opts: IOptions);
    toString(blockList?: number[], includeHeaders?: boolean): string;
    getDataBlockCount(): number;
    /**
     * The base to use for transmitting the data, if any
     * @param base base64 or base91
     */
    setBase(base: '' | BaseEncode): void;
    quantizeMessage(): number;
}
