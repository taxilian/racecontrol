define([], function() {
    function keyDesc(evt) {
        var text = "";

        if (evt.ctrlKey) {
            text += "ctrl+";
        }
        if (evt.altKey) {
            text += "alt+";
        }
        if (evt.shiftKey) {
            text += "shift+";
        }
        switch(evt.keyCode) {
            case 191:
                text += '/';
                break;
            case 189:
                text += '-';
                break;
            case 187:
                text += '+';
                break;
            case 0:
            case 3:
            case 13:
                text += 'Enter';
                break;
            case 8:
                text += 'Backspace';
                break;
            case 46:
                text += 'Del';
                break;
            case 33:
                text += 'PgUp';
                break;
            case 34:
                text += 'PgDn';
                break;
            case 35:
                text += 'End';
                break;
            case 36:
                text += 'Home';
                break;
            case 37:
                text += 'Left';
                break;
            case 38:
                text += 'Up';
                break;
            case 39:
                text += 'Right';
                break;
            case 40:
                text += 'Down';
                break;
            case 27:
                text += 'Esc';
                break;
            default:
                if (evt.keyCode >= 32 && evt.keyCode <= 96) {
                    text += String.fromCharCode(evt.keyCode);
                } else if (evt.keyCode >= 112 && evt.keyCode <= 123) {
                    text += "F" + (evt.keyCode - 111);
                }
                break;
        }
        return text;
    }

    function isNumeric(evt) {
        return evt.keyCode < 32 || (evt.keyCode >= 48 && evt.keyCode <= 57);
    }

    return {
        keyDesc: keyDesc,
        isNumeric: isNumeric
    };
});
