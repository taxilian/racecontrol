if __name__ == "__main__":
    import settings
    from django.core.management import setup_environ
    setup_environ(settings)

from django.conf import settings
from urllib2 import URLError
from racecontrol.models import *
from racecontrol.client import *
from django import db
import os, sys
import datetime
import json
import time

_TIME_FORMAT = "%m %d %Y %H %M %S %f"

class Synchronizer:
    client = None
    wins = ""
    evtId = ""
    lastPull = datetime.datetime(2000, 1, 1)
    lastPush = datetime.datetime(2000, 1, 1)

    def __init__(self, serverAddr, evtId, wins):
        self.wins = wins
        self.evtId = evtId
        self.client = RaceControlClient(serverAddr, "n7bsa", "1973")
        self.client.Login()

    def PushDataSince(self):
        updSeq = self.lastPush
        self.lastPush = datetime.datetime.now()
        print "Sending updates since: ", updSeq
        raceEvents = RaceEvent.objects.filter(modified__gt=updSeq,uuid=self.evtId).toArray()
        print [r.__dict__ for r in RaceEvent.objects.filter(modified__gt=updSeq,uuid=self.evtId)]

        stations = Station.objects.filter(modified__gt=updSeq,raceEvent__uuid=self.evtId).toArray()

        participants = Participant.objects.filter(modified__gt=updSeq,raceEvent__uuid=self.evtId).toArray()

        entrys = Entry.objects.select_related("station__stationNumber", "participant__bibNumber"
            ).filter(modified__gt=updSeq,participant__raceEvent__uuid=self.evtId
            ).toArray()
        for entry in entrys:
            entry["raceEvent_id"] = self.evtId
            del entry["participant_id"]
            del entry["station_id"]
            del entry["uuid"]

        dthandler = lambda obj: obj.isoformat() if isinstance(obj, datetime.datetime) or isinstance(obj, datetime.date) else None
        dthandlerA = lambda obj: obj.strftime("%Y-%m-%d %H:%M:%S") if isinstance(obj, datetime.datetime) or isinstance(obj, datetime.date) else None

        print "Update Race Events:", self.client.Call("updateRaceEvent", None, json.dumps(raceEvents, default=dthandlerA))
        print "Update Stations:", self.client.Call("updateStation", None, json.dumps(stations))
        print "Update Participants:", self.client.Call("updateParticipant", None, json.dumps(participants))
        print "Update Entries:", self.client.Call("updateEntry", None, json.dumps(entrys, default=dthandlerA))
        db.connection._commit()

    def GetDataSince(self):
        modified = datetime.datetime.strftime(self.lastPull, _TIME_FORMAT)
        newData = self.client.Call("getAllUpdates", {"evtId": self.evtId, "modified": modified})
        self.lastPull = datetime.datetime.strptime(newData.LastUpdated, _TIME_FORMAT)

        print "Updating %s events" % len(newData.RaceEvents)
        for event in newData.RaceEvents:
            event["date"] = datetime.datetime.strptime(event["date"], "%Y-%m-%d %H:%M:%S")
            dbEvent, created = RaceEvent.objects.get_or_create(uuid=event["uuid"], date=event["date"])
            print "Original:", dbEvent.__dict__
            if event.lastModifiedBy != settings.STATION_UUID:
                if dbEvent.fromDict(event):
                    print "Something changed...?", dbEvent.__dict__
                    dbEvent.modifiedBy = event.lastModifiedBy
                    dbEvent.save()

        raceEvent = RaceEvent.objects.get(pk=evtId)

        print "Updating %s stations" % len(newData.Stations)
        for station in newData.Stations:
            dbStation, created = Station.objects.get_or_create(raceEvent=raceEvent, stationNumber=station["stationNumber"])
            if station.lastModifiedBy != settings.STATION_UUID:
                if dbStation.fromDict(station):
                    dbStation.modifiedBy = station.lastModifiedBy
                    dbStation.save();

        print "Updating %s participants" % len(newData.Participants)
        for participant in newData.Participants:
            dbParticipant, created = Participant.objects.get_or_create(raceEvent=raceEvent, bibNumber=participant["bibNumber"])
            if participant.lastModifiedBy != settings.STATION_UUID:
                if dbParticipant.fromDict(participant):
                    dbParticipant.modifiedBy = participant.lastModifiedBy
                    dbParticipant.save()

        print "Updating %s entries" % len(newData.Entrys)
        for entry in newData.Entrys:
            participant = Participant.objects.filter(raceEvent=raceEvent, bibNumber=entry["participant_bibNumber"]).get()
            station = Station.objects.filter(raceEvent=raceEvent, stationNumber=entry["station_stationNumber"]).get()
            dbEntry, created = Entry.objects.get_or_create(participant=participant, station=station)
            del entry["participant_bibNumber"]
            del entry["participant_id"]
            del entry["station_stationNumber"]
            del entry["station_id"]
            del entry["station_distance"]
            del entry["station_name"]
            print "Original:", dbEntry.__dict__
            entry["time_in"] = datetime.datetime.strptime(entry["time_in"], "%Y-%m-%d %H:%M:%S") if entry["time_in"] else None
            entry["time_out"] = datetime.datetime.strptime(entry["time_out"], "%Y-%m-%d %H:%M:%S") if entry["time_out"] else None
            if entry.lastModifiedBy != settings.STATION_UUID:
                if dbEntry.fromDict(entry):
                    dbEntry.modifiedBy = entry.lastModifiedBy
                    dbEntry.save()
                    print "Changed:", dbEntry.__dict__
        db.connection._commit()

    def run(self):
        try:
            while True:
                try:
                    if (self.wins == "server"):
                        self.GetDataSince()
                        self.PushDataSince()
                    else:
                        self.PushDataSince()
                        self.GetDataSince()
                    print "Sleeping for 15 seconds before next synchronization..."
                    time.sleep(15)
                except URLError, e:
                    print e.code
                    f = open("outlog.html", "w")
                    f.write(e.read())
                    print "Written to outlog.html"
        except KeyboardInterrupt:
            pass


def run(serverAddr = None, evtId = None, wins = "client"):
    if not serverAddr:
        print "Enter the http address of the server:"
        serverAddr = raw_input(">")

    if not evtId:
        print "Enter the id of the race event:"
        evtId = raw_input(">")

    while wins != "server" and wins != "client":
        print "Which wins, client or server? "
        wins = raw_input(">")

    s = Synchronizer(serverAddr, evtId, wins)
    s.run()

if __name__ == "__main__":
    addr = sys.argv[1] if len(sys.argv) > 1 else None
    evtId = sys.argv[2] if len(sys.argv) > 2 else None
    wins = sys.argv[3] if len(sys.argv) > 3 else "client"
    run(addr, evtId, wins)
