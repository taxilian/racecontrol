import { Block } from './block';
import { crc16 as crc16JS } from './crc16';
import { TypedEvent } from './TypedEvent';
declare let crc16: typeof crc16JS;
export interface NewFileEvent {
    /** This is the only thing guaranteed on this event */
    hash: string;
}
/**
 * Fired every time a new block is received;
 * Fields which aren't known will be undefined.
 * If you don't have filename or blockCount then
 * the header block(s) haven't been received yet
 * or were missed
 */
export interface FileUpdateEvent {
    hash: string;
    /** The filename, if we know it */
    filename?: string;
    /** An array of block numbers which we haven't seen yet (if known) */
    blocksNeeded?: number[];
    /**
     * An array of block numbers we've seen; this is knowable even
     * if we don't know how many there are total, so we're including
     * it. Until we have a valid "SIZE" record we can't say how many
     * there are overall, but we can start collecting information about it
     */
    blocksSeen?: number[];
    /** The total number of blocks expected (if known) */
    blockCount?: number;
    /** The size of each block (if known) */
    blockSize?: number;
}
/**
 * Fired when a file is complete and ready to
 * read. In order for this to fire we need to receive:
 *   * The filename (FILE record)
 *   * The file size (SIZE record)
 *   * All data blocks
 *
 * After you have processed the file you should call
 * deleteFile(hash) to free the memory used by the file
 */
export interface FileCompleteEvent {
    filename: string;
    hash: string;
}
export declare class File {
    fromCallsign: string | null;
    headerBlocks: Block[];
    dataBlock: {
        [blockNum: number]: Block | undefined;
    };
    name?: string;
    size?: number;
    blockCount?: number;
    blockSize?: number;
    hash: string;
    modified?: Date;
    completeFired: boolean;
    constructor(firstBlock: Block);
    getOrderedDataBlocks(): (Block | undefined)[];
    getNeededBlocks(): number[];
    getRawContent(): string;
    getContent(): string | null;
    toBlob(): Blob;
    getUpdateRecord(): FileUpdateEvent;
    /**
     * Adds a received block to the file; returns true if the block
     * is new (has not been received already for this file)
     * @param inBlock
     */
    addBlock(inBlock: Block): boolean;
    addDataBlock(inBlock: Block): boolean;
    isComplete(): boolean;
}
export interface Files {
    [K: string]: File;
}
export declare class Deamp {
    private PROGRAM;
    private VERSION;
    private parserState;
    private parserData;
    newFileEvent: TypedEvent<NewFileEvent>;
    fileUpdateEvent: TypedEvent<FileUpdateEvent>;
    fileCompleteEvent: TypedEvent<FileCompleteEvent>;
    private receivedFiles;
    private inputBuffer;
    constructor(opts?: {});
    setCrc16(crc: typeof crc16): void;
    clearBuffer(): void;
    pruneInputBuffer(): void;
    /**
     * This should only be called for unit tests
     */
    __getInputBuffer(): string;
    ingestString(inString: string): void;
    _processInput(oneChar: string): Block | undefined;
    addBlockToFiles(inBlock: Block): void;
    getFilesEntries(): string[];
    /**
     * Gets the file but leaves it in memory
     * @param fileHash
     */
    getFile(fileHash: string): File;
    /**
     * Retrieves the file and frees all related memory
     * @param fileHash
     */
    popFile(fileHash: string): File;
    /**
     * Gets the file contents by hash
     * @param fileHash
     */
    getFileContents(fileHash: string): string | null;
}
export {};
