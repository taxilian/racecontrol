define(["jquery"], function(jQuery) {
    jQuery.fn.selectText = function(start, len) {
        var self = this.get(0);
        var end = len+start;
        if (self.createTextRange) {
            var selRange = self.createTextRange();
            selRange.collapse(true);
            selRange.moveStart("character", start);
            selRange.moveEnd("character", len);
            selRange.select();
        } else if (self.setSelectionRange) {
            self.setSelectionRange(start, end);
        } else if (self.selectionStart) {
            self.selectionStart = start;
            self.selectionEnd = len;
        }
    };
});
