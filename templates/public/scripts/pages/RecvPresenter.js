/// <reference path="../lib/mt63_types/index.d.ts" />

define(["backbone", "jquery", "underscore", "./RecvView",
       "dialogs/AddEditParticipantPresenter", "dialogs/AddEditEntryPresenter", "dialogs/AddEditStationPresenter", "dialogs/SpeedyEntryPresenter",
       "models/RaceEvent", "models/Participant", "models/Station", "models/Updater", "DateManager", "../ext/flampTools", "../lib/mt63Listener"],
/**
 * 
 * @param {typeof Backbone} Backbone 
 * @param {typeof JQuery} $ 
 * @param {typeof import("underscore")} _ 
 * @param {*} DefaultView 
 * @param {*} AddEditParticipant 
 * @param {*} AddEditEntry 
 * @param {*} AddEditStation 
 * @param {*} SpeedyEntry 
 * @param {*} RaceEvent 
 * @param {*} Participant 
 * @param {*} Station 
 * @param {*} Updater 
 * @param {*} DateManager 
 * @param {typeof FLAmp} FlampTools 
 * @param {{listen: ()=> void, events: Backbone.Model, isListening: () => boolean, stopListening: () => void, deamp: FLAmp.Deamp}} mt63Listener
 */
function(Backbone, $, _, DefaultView, AddEditParticipant, AddEditEntry, AddEditStation, SpeedyEntry,
         RaceEvent, Participant, Station, Updater, DateManager, FlampTools, mt63Listener) {

    var deamp = mt63Listener.deamp;
    /** @type {Backbone.Model & {}} */
    var File = Backbone.Model.extend({
        defaults: {
            filename: "unknown",
            blockCount: -1,
            neededBlocks: null
        }
    });

    /**
     * @type {Backbone.Collection & {}}
     */
    var Files = Backbone.Collection.extend({
        model: File
    });

    var RecvPresenter = Backbone.Router.extend({
        initialize: function(options) {
            var self = this;
            self.view = new DefaultView();
            self.files = new Files();

            mt63Listener.events.bind("listening", function(state) {
                self.files.trigger("listenState", state);
            });

            // Time to start listening!
            mt63Listener.events.bind('rx', function(rxString) {
                self.view.rxText(rxString);
            });

            this.onFileUpdate = this.onFileUpdate.bind(this);
            mt63Listener.deamp.newFileEvent.on(this.onFileUpdate);
            mt63Listener.deamp.fileUpdateEvent.on(this.onFileUpdate);
            mt63Listener.deamp.fileCompleteEvent.on(this.onFileUpdate);
            
            self.view.bind("startStop", function() {
                if (mt63Listener.isListening()) {
                    mt63Listener.stopListening();
                } else {
                    mt63Listener.listen();
                }
            });

            self.view.bind("transmitAmp", function(blocks) {
                var ampText = this.stationAmp.toString(blocks);
                if (!this.mt63) {
                    this.mt63 = new FlampTools.MT63Client();
                }

                /**
                 * @type {FLAmp.MT63Client}
                 */
                var mt63 = this.mt63;

                var res = mt63.encodeString(ampText, 2000, 1, new AudioContext());
                res.source.start();
                self.audioSource = res.source;
            });
            
            self.view.bind("transmitDnf", function() {
                // TODO: make a DNF list and transmit it
            });

            self.view.bind("importFile", self.doImport, self);
        },

        prepareStationList: function() {
            var outputFile = [];
            // race,name,date,_id
            var eventLine = ['race'];
            eventLine.push(this.event.get('name'));
            eventLine.push(this.event.get('date'));
            outputFile.push(eventLine.join(','));

            this.stationList.forEach(function(station) {
                var line = ["s"];
                line.push(station.get('stationNumber'));
                line.push(station.get('name'));
                line.push(station.get('distance'));
                outputFile.push(line.join(','));
            });

            this.runnerList.forEach(function(runner) {
                // r,bibNumber,firstName,lastName,age,sex,note,team,home
                if (typeof(runner.get('dnfStation')) == "number") {
                    // Skip any runners who Did Not Finish
                    return;
                }
                var line = ["r"];
                line.push(runner.get("bibNumber"));
                line.push(runner.get("firstName"));
                line.push(runner.get("lastName"));
                line.push(runner.get("age"));
                line.push(runner.get("sex"));
                outputFile.push(line.join(','));
            });

            var outputFileText = outputFile.join('\n');
            var stationAmp = new FlampTools.Amp({
                compression: "LZMA",
                base: "base64",
                filename: "Race" + this.event.get('name'),
                fileModifiedTime: new Date(),
                blkSize: 96,
                inputBuffer: outputFileText,
                skipProgram: true,
                useEOF: false,
                useEOT: false
            });
            this.stationAmp = stationAmp;
            console.log("Data blocks:", this.stationAmp.getDataBlockCount());
        },

        onFileUpdate: 
        /**
         * 
         * @param {{hash: string}} evt 
         */
        function(evt) {
            var self = this;
            /** @type {Backbone.Collection} */
            var files = this.files;
            var deampFile = deamp.getFile(evt.hash);
            var fileUpdate = deampFile.getUpdateRecord();
            var fileEntry = files.find(function(f) {
                return f.get('hash') == evt.hash;
            });
            if (fileEntry && fileEntry.get('complete') === true) {
                // it's already complete, nothing to see here
                return;
            }
            if (!fileEntry) {
                fileEntry = new File(fileUpdate);
                files.add(fileEntry);
            } else {
                fileEntry.set(fileUpdate);
            }
            
            if (fileUpdate.blocksNeeded && !fileUpdate.blocksNeeded.length && fileUpdate.filename) {
                // We have the whole file!
                fileEntry.set({complete: true});

                var contents = deampFile.getContent();
                fileEntry.set({content: contents});
                if (deampFile.name.toLowerCase().startsWith('station-')) {
                    // It's station data!
                    var stationNo = parseInt(deampFile.name.split('-')[1], 10);
                    var lines = contents.split('\n');
                    var mIdx = -1;
                    var metadataStr = lines.find(function(line, i) {
                        var found = !!line.trim();
                        if (found) { mIdx = i; }
                        return found;
                    });
                    var metadata = {};
                    try {
                        metadata = JSON.parse(metadataStr);
                        if (typeof metadata === 'object' && !Array.isArray(metadata)) {
                            lines.splice(mIdx, 1);
                        }
                    } catch (e) {console.error(e);} // tslint:disable-line no-empty
                    var epoch = metadata.epoch || 0;
                    var entries = lines.filter(function(line) { return !!line.trim(); }).map(function(line) {
                        var columns = line.split(',').map(function(str) { return str.trim(); });
                        var newTimeIn = columns[1] ? new Date((epoch + Number(columns[1])) * 1000 * 60) : null;
                        var newTimeOut = columns[2] ? new Date((epoch + Number(columns[2])) * 1000 * 60) : null;
                        var data = {
                            bib: columns[0],
                            time_in: newTimeIn,
                            time_out: newTimeOut
                        };
                        if (columns.length > 3) {
                            data.dnfStation = stationNo;
                            data.dnfReason = columns[3];
                        }
                        return data;
                    });
                    fileEntry.set({
                        entries: entries,
                        station: stationNo
                    });
                }
            } else {
                fileEntry.set({complete: false});
            }

            self.view.refresh();
        },

        present: function(container, eventId) {
            var self = this;
            self.eventId = eventId;
            // self.stopUpdate();

            var eventDfd = RaceEvent.obj.fetchById(eventId);
            var runnerListDfd = Participant.list.fetchAll(eventId);
            var stationListDfd = Station.list.fetchAll(eventId);
            DateManager.setEvent(eventId);

            $.when(eventDfd, runnerListDfd, stationListDfd)
                .done(function(evt, rlist, slist) {
                self.event = evt;
                self.runnerList = rlist;
                self.stationList = slist;
                self.prepareStationList();
                self.view.setData(evt, rlist, self.files, self.stationAmp);
                // rlist.bind("entryDownload", self.updateProgress, self);
                // $.when(dfd).done(function() {
                //     self.onDataLoaded();
                // });
            });
            container.setView(self.view);
        },

        startListening: function() {
            mt63Listener.listen();
        },
        stopListening: function() {
            mt63Listener.stopListening();
        },

        doImport: function(importJob) {
            var self = this;
            var file = importJob.file;
            var targetStation = importJob.targetStation;

            var curDate = DateManager.getCurrentDate();

            var updateDfds = file.get('entries').forEach(function(fileEntry) {
                var newTimeIn = fileEntry.time_in;
                var newTimeOut = fileEntry.time_out;
                var dnfReason = null;
                var dnfStation = null;
                if ('dnfReason' in fileEntry) {
                    dnfReason = fileEntry.dnfReason;
                    dnfStation = fileEntry.dnfStation;
                }

                var runner = self.runnerList.find(function(r) {
                    return r.get('bibNumber') == fileEntry.bib;
                });

                var newRunner = !runner;
                if (!runner) {
                    runner = new Participant.obj({
                        raceEvent_id: self.eventId,
                        bibNumber: fileEntry.bib,
                        firstName: "Unknown",
                        lastName: "Participant"
                    });
                    self.runnerList.add(runner);
                }
                if (dnfReason !== null) {
                    runner.set({dnfReason: dnfReason, dnfStation: dnfStation});
                }
                return runner.getOrCreateEntry(targetStation).pipe(function(entry) {
                    if (fileEntry.time_in) {
                        entry.set({time_in: newTimeIn});
                    }
                    if (fileEntry.time_out) {
                        entry.set({time_out: newTimeOut});
                    }
                    if (newRunner) {
                        return runner.save().pipe(function() {
                            return entry.save();
                        });
                    } else {
                        return entry.save();
                    }
                });
            });

            $.when.apply(null, updateDfds).fail(function(err) {
                console.warn("Failed to save:", err, arguments);
                alert("Failed!");
            });
        },

        onDataLoaded: function() {

            // this.view.displayResults(results);
        },

        // setRunnerList: function(rlist) {
        //     var self = this;
        //     if (self.rlist != rlist && self.rlist) {
        //         self.rlist.unbind("change", self.onRunnerChange);
        //     }
        //     self.rlist = rlist;
        //     if (rlist) {
        //         rlist.bind("change", self.onRunnerChange, self);
        //     }
        // },

        // onRunnerChange: function(runner, entry) {
        // }
    });

    return new RecvPresenter();
});



