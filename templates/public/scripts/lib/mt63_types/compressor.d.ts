declare type CompressFn = (str: string) => string;
declare type DecompressFn = (str: string) => string;
interface CompressOption {
    prefix: string;
    compress: CompressFn;
    decompress: DecompressFn;
}
declare class CompressorHolder {
    default: string;
    cMap: {
        [prefix: string]: CompressOption;
    };
    constructor();
    addCompressor(compressor: CompressOption): void;
    getCompressor(prefix?: string): CompressOption;
    getDecompressor(buffer: string): CompressOption | null;
}
export declare const Compressor: CompressorHolder;
export {};
