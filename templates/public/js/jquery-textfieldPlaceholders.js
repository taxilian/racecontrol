/* Textfield Placeholders - jQuery Plugin
 * Copyright (c) 2009 Palle Zingmark
 * Author: Palle Zingmark, www.palleman.nu
 *         Philip Hofstetter, www.gnegg.ch
 * Released with the MIT License: http://www.opensource.org/licenses/mit-license.php
 */

var __placeholderFunc = function(i){
    var $input = $(this);
    var $original_value = $($input).attr('value');
    var $placeholder = $($input).attr('placeholder');
    var $basecolors = ['#000000','rgb(0, 0, 0)'];
    var $color = $($input).css('color');
    var $hascolor = jQuery.inArray($color,$basecolors);
    if(typeof $placeholder == 'undefined' || $placeholder == ''){
        $placeholder = $parent.find('label[for="'+ $($input).attr('id') +'"]').text();
    }
    if(typeof $placeholder == 'string' || $placeholder != '') { //## OR ABORT
        if ($input.attr('type') == 'password'){
            var $proxy = $('<input type="text" />');
            var $original_input = $input;
            $proxy.attr('class', $input.attr('class'));
            $proxy.attr('style', $input.attr('style'));
            $proxy.insertAfter($input);
            if (!$original_value) {
                $input.hide();
                $input = $proxy;
            }
            else {
                $proxy.hide();
            }
        }
        if (!$original_value) {
            $input.attr('value', $placeholder);
            if($hascolor || $hascolor != -1){
                $input.css('color','#aeaeae');
            }
        }
        $input.attr('title', $placeholder);
        ($proxy || $input).bind('focus', function(){
            if($input.attr('value') == $placeholder){
                if ($original_input){
                    $input.hide();
                    $original_input.show();
                    $input = $original_input;
                    $input.focus();
                }
                $input.attr('value','');
                $input.css('color',$color);
            }
        });
        ($original_input || $input).bind('blur', function(){
            if($input.attr('value') == $placeholder || $input.attr('value') == ''){
                if ($original_input){
                    $original_input.hide();
                    $proxy.show();
                    $input = $proxy;
                }
                $input.attr('value',$placeholder);
                if($hascolor || $hascolor != -1){
                    $input.css('color','#aeaeae');
                }
            }
        });
    }
};

(function($){
	$.fn.TextfieldPlaceholders = function(){
		return $(this).each(function() {
			var $parent = $(this);
			$parent.find('input:text,input:password').each(__placeholderFunc);
		});
	};
})(jQuery);
