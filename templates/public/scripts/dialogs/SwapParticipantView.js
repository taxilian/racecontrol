define(['base/BaseView', 'underscore',
        '_tpl!./jst/SwapParticipant.jst'], function(BaseView, _, tpl) {

    var SwapParticipantView = BaseView.extend({
        event: null,

        events: {
        },

        render: function() {
            var self = this;
            var $el = $(self.el);
            $el.html(tpl({runner: self.runner, stations: self.stations}));
            return self;
        },

        setParticipant: function(runner) {
            this.runner = runner;
        },

        setStations: function(stations) {
            this.stations = stations;
        },

        onSaveClicked: function() {
            var self = this;

            var newBibNo = Number(self.$("input[name=fld_bibNumber]").val());
            if (isNaN(newBibNo)) {
                notify.warn("Invalid bib number!");
            } else {
                this.trigger("save", newBibNo);
            }
        }
    });

    return SwapParticipantView;
});


