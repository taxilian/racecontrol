define(["backbone", "jquery", "./AddEditParticipantView", "models/Participant", "models/Station", "base/AcceptsOneViewDialog", "./SwapParticipantPresenter"],
function(Backbone, $, AddEditParticipantView, Participant, Station, Dialog, SwapParticipant) {

    var AddEditParticipantPresenter = Backbone.Router.extend({
        initialize: function(options) {
            var self = this;
            self.view = new AddEditParticipantView();
            self.rId = options.rId;
            self.eventId = options.eventId;
            self.dfd = options.dfd;
            self.view.bind("save", self.onSaveParticipant, self);
            self.view.bind("swapClicked", self.onSwapClicked, self);
        },

        present: function() {
            var self = this,
                runnerDfd = Participant.obj.fetchById(self.eventId, self.rId),
                stationsDfd = Station.list.fetchAll(self.eventId);

            $.when(runnerDfd, stationsDfd).pipe(function(runner, stations) {
                self.runner = runner;
                self.stations = stations;
                self.view.setParticipant(runner);
                self.view.setStations(stations);

                // render!
                var dlg = self.dlg = new Dialog({
                    title: "Edit Participant",
                    view: self.view,
                    buttons: ["OK", "Cancel"]
                });
                dlg.doDialog();
                dlg.bind("onButtonClick", function(button) {
                    if (button == "OK") {
                        self.view.onSaveClicked();
                    } else {
                        dlg.close();
                        self.cancel();
                    }
                });
            });
        },

        onSaveParticipant: function(data) {
            var self = this;
            self.runner.set(data);

            self.runner.save().done(function() {
                self.dfd.resolve(self.runner);
                self.dlg.close();
            });
        },

        cancel: function() {
            var self = this;
            self.dfd.reject(self.event);
            self.dlg.close();
        },

        onSwapClicked: function() {
            var self = this;
            SwapParticipant.showSwap(self.eventId, self.rId).done(function() {
                // If the swap completes then close the dialog
                self.cancel();
            });
        }
    }, {
        showParticipant: function(eventId, rId) {
            var dfd = new $.Deferred();
            var obj = new this({eventId: eventId, rId:rId, dfd:dfd});
            obj.present();
            return dfd.promise();
        }
    });

    return AddEditParticipantPresenter;
});



