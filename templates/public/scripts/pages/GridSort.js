define(["underscore"], function(_) {

    function getTime(runner, type, sNo) {
        return (type == "i" ? runner.getTimeInAt(sNo) : runner.getTimeOutAt(sNo)) || Number.MAX_VALUE;
    }

    return function(target, desc) {
        var sortFunc;
        var self = this;
        var sort_desc = false;
        if (desc) sort_desc = true;

        if (target == "num" || target == "name" || target == "position") {
            var getVal = function(src) {
                if (target == "num") {
                    return src.id;
                } else if (target == "name") {
                    return src.formatName();
                } else if (target == "position") {
                    var place = src.place;
                    if (place == "DNF") { place = 999999; }
                    else if (!place) { place = 99999; }
                    return place;
                }
            };
            sortFunc = function(a, b) {
                var compA = getVal(a);
                var compB = getVal(b);
                var ret = (compA < compB) ? -1 : (compA > compB) ? 1 : 0;
                return sort_desc ? ret*-1 : ret;
            };
            return sortFunc;
        }

        if (target.substring(0,1) == "s") {
            var type = target.substring(1,2);
            var sNo = parseInt(target.substring(2), 10);

            var realSortFunc = function(a, b) {

                var ta = getTime(a, type, sNo);
                var tb = getTime(b, type, sNo);

                if (ta > tb) return 1;
                else if (ta < tb) return -1;
                else { // if they are equal (even if they are null)

                    var seenA = a.getLastSeenAt();
                    var seenB = b.getLastSeenAt();
                    seenA = seenA === null ? Number.MAX_VALUE : seenA;
                    seenB = seenB === null ? Number.MAX_VALUE : seenB;
                    if (seenA < seenB) return 1;
                    else if (seenA > seenB) return -1;
                    else {
                        for (var i = sNo; i >= 0; --i) {
                            ta = getTime(a, type, i);
                            tb = getTime(b, type, i);
                            if (ta === tb) continue;
                            if (!tb || ta > tb) return 1;
                            if (!ta || ta < tb) return -1;
                        }
                        return 0;
                    }
                }
            };
            sortFunc = sort_desc ? function(a,b) { return realSortFunc(a,b) * -1; } : realSortFunc;
            return sortFunc;
        }

    };
});

