define(["backbone", "jquery", "./TopRightStatsView", "models/RaceEvent", "models/Participant"],
function(Backbone, $, DefaultView, RaceEvent, Participant) {

    var TopRightStatsPresenter = Backbone.Router.extend({
        initialize: function(options) {
            var self = this;
            self.view = new DefaultView();
        },

        present: function(container, eventId) {
            var self = this;

            var eventDfd = RaceEvent.obj.fetchById(eventId),
                runnerListDfd = Participant.list.fetchAll(eventId),
                entrysDfd = runnerListDfd.pipe(function(runners) { return runners.getEntrys(); });

            $.when(eventDfd, runnerListDfd, entrysDfd).done(function(event, runnerList) {
                self.event = event;
                self.runnerList = runnerList;

                function refreshStats() {
                    var stats = self.calculateStatistics(event, runnerList);
                    self.view.setStats(stats);
                }
                runnerList.unbind("all");
                runnerList.bind("all", refreshStats);
                refreshStats();

                container.setView(self.view);
            });

        },

        calculateStatistics: function(event, runnerList) {
            var startStation = event.get("startStation") || 0;
            var finishStation = event.get("finishStation") || null;

            var totalRunners = 0,
                droppedRunners = 0,
                finishedRunners = 0,
                onCourseRunners = 0;

            runnerList.each(function(runner) {
                var seen = runner.wasSeenAt(startStation) && !runner.isDroppedAt(startStation),
                    dropped = seen && runner.isDropped(),
                    finished = seen && !dropped && finishStation && runner.wasSeenAt(finishStation),
                    onCourse = seen && !dropped && (!finishStation || !runner.wasSeenAt(finishStation));
                if (seen) { totalRunners++; }
                if (dropped) { droppedRunners++; }
                if (finished) { finishedRunners++; }
                if (onCourse) { onCourseRunners++; }
            });

            return {total: totalRunners, dropped: droppedRunners, finished: finishedRunners, onCourse: onCourseRunners};
        }
    });

    return new TopRightStatsPresenter();
});
