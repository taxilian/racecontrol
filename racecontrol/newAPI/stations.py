from ..urlDecorator import *
from ..models import *
from django.conf import settings
from django.shortcuts import redirect
from django.http import Http404
from pyzoop import zone, urlVars
from ..JSONUtils import ReturnsJSON, AcceptsJSON
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django import db
import os

Debug = True

def errorResponse(content, status=500):
    return HttpResponse(content, status=status)

def extractWantedObj(src):
    del src["uuid"]
    return src

@url(r'^/events/([a-fA-F0-9\-]+)/stations/?$', method="GET")
@ReturnsJSON
def stationsByEventId(request, evtId):
    try:
        resp = Station.objects.filter(raceEvent__uuid=evtId).toArray()
        return (extractWantedObj(o) for o in resp)
    except:
        raise Http404

@url(r'^/events/([^/]+)/stations/([^/]+)/?$', method="GET")
@ReturnsJSON
def stationsByEventIdAndStationId(request, evtId, stationNo):
    try:
        resp = Station.objects.get(raceEvent__uuid=evtId, stationNumber=stationNo).toDict()
        return extractWantedObj(resp)
    except:
        raise Http404

@url(r'^/events/([^/]+)/stations/([^/]+)/?$', method="PUT")
@AcceptsJSON
@ReturnsJSON
def updateStationByEventIdAndStationNumber(request, evtId, stationNo, s):
    """
        updateEvent expects JSON in the same format it would receive it from getStationList -
        including in an array, even if there is only one item
    """
    try:
        raceEvent = RaceEvent.objects.get(pk=evtId)
        print "rid", s["raceEvent_id"]
        station, created = Station.objects.get_or_create(raceEvent=raceEvent, stationNumber=stationNo)
        #TODO: Fix lastModifiedBy stuff on the client
        if "lastModifiedBy" not in s:
            s["lastModifiedBy"] = settings.STATION_UUID

        station.name = s["name"]
        station.distance = s["distance"]
        station.save()

        db.connection._commit()
        return station.toDict()
    except:
        if self.Debug:
            raise
        return self.error("Malformed JSON update request")
