var EventLookupPage = RB.LWClass();
_p = EventLookupPage.prototype;

_p.initialize = function(evtId) {
    var self = this;
    this.seconds = 5;
    this.updateTimerId = -1;
    this.evtId = evtId;
    this.rc = new RaceControl();

    this.rc.getRaceEvents(false, this.$("eventsLoaded"));
};

_p.eventsLoaded = function(eventList) {
    this.event = eventList[this.evtId];
    this.event.quickLookup();
}

delete _p;
