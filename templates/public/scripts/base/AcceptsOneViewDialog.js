define(["./BaseView", "underscore"], function(BaseView, _) {

    var AcceptsOneViewDialog = BaseView.extend({
        initialize: function(options) {
            this.title = options.title;
            this.buttons = options.buttons || AcceptsOneViewDialog.buttonText;
            this.view = options.view;
            this.title = options.title || "";
            this.width = options.width || 420;
            this.closeOnEscape = true;
            if (options.closeOnEscape === false) {
                this.closeOnEscape = false;
            }
            this.minHeight = options.minHeight || 40;
            this.modal = (options.modal !== false);
        },

        doDialog: function() {
            var self = this;
            var $el = $(this.el);
            $el.empty();
            var handlerCalled = false;

            var buttons = {};
            _.each(self.buttons, function(btnText) {
                buttons[btnText] = function() {
                    self.trigger("onButtonClick", btnText);
                };
            });
            $el.empty().append(self.view.render().el);
            $el.dialog({
                title: self.title,
                autoOpen : true,
                modal: self.modal,
                resizable: false,
                show: "fade",
                hide: "fade",
                minHeight: self.minHeight,
                width: self.width,
                closeOnEscape: self.closeOnEscape,
                buttons: buttons,
                close: function() {
                    self.trigger("close");
                    $(self.el).dialog("destroy");
                },
                open: function(evt) {
                    //var btn = $(evt.target).parents(".ui-dialog").find('button:contains("OK")');
                    //btn.button({icons: {primary:'ui-icon-check',secondary:null}});
                    $(evt.target.activeElement).find(".ui-dialog-titlebar-close").hide();
                }
            });
            return self;
        },
        updateDialogTitle: function(title) {
            $(this.el).dialog({ title: title });
        },
        close: function() {
            $(this.el).dialog("close");
        }
    }, {
        buttonText: ["OK", "Cancel"]
    });

    return AcceptsOneViewDialog;
});
