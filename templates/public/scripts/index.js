require.config({
    baseUrl: "/public/scripts/",
    paths: {
        "backbone"     : "ext/backbone",
        "text"         : "ext/text",
        "_tpl"         : "ext/tpl",
        "underscore"   : "ext/underscore",
        "json2"        : "ext/json2",
        "jqueryui"     : "ext/jquery-ui-1.8.19.custom.min",
        "datelib"      : "ext/date",
        "flampWeb"     : "flampWeb/flampWeb"
    }
});
require([
        "./router", "backbone",
        "jquery", "jqueryui", "datelib", "json2", "base/notify"
    ], function(MainRouter, Backbone, $) {
    $.datepicker.formatDate = function(format, date, settings) { return date.toString(format); };
    // --------------------------------------- //
    // Global Ajax Handlers e.g. Success/Error //
    // --------------------------------------- //
    $.ajaxSetup({
        timeout: 20000
    });

    Backbone.history.start();

});
