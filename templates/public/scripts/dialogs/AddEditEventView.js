define(['base/BaseView', 'underscore', '_tpl!./jst/AddEditEvent.jst', "defaults"], function(BaseView, _, tpl, defaults) {

    var AddEditEventView = BaseView.extend({
        raceEvent: null,

        render: function() {
            var self = this;
            var $el = $(self.el);
            console.log(self.raceEvent, self.stations);
            $el.html(tpl({event: self.raceEvent, stations: self.stations}));
            self.$(".timeField").timeField({
                allowNumeric: true,
                allowAlpha: false,
                transform: {".": ":"}
            });
            self.$(".dateField").datepicker({dateFormat: defaults.dateFormat});
            return self;
        },

        setEvent: function(evt) {
            this.raceEvent = evt;
        },

        setStations: function(stations) {
            this.stations = stations;
        },

        saveEvent: function() {
            var self = this;
            var fieldValues = {};
            // todo: date
            _.each(["name", "startStation", "finishStation"], function(field) {
                var match = "[name=fld_" + field + "]";
                var f = self.$("input" + match + ",select" + match);
                fieldValues[field] = f.val();
            });
            var time = self.$("input[name=eventTime]").val();
            var date = self.$("input[name=eventDate]").val();
            fieldValues.date = Date.parse(date + " " + time);
            self.trigger("save", fieldValues);
        }
    });

    return AddEditEventView;
});


