export declare class MT63Client {
    constructor();
    lzmaEncode(input: string): string;
    lzmaDecode(input: string | Uint8Array): string;
    encodeString(str: string, bandwidth: number, interleave: 0 | 1, audioCtx: AudioContext): {
        source: AudioBufferSourceNode;
        buffer: AudioBuffer;
        length: number;
        sampleRate: number;
    };
    getSampleRate(): number;
    processAudio(floatArr: Float32Array, len: number, sampleRate?: number): string;
}
