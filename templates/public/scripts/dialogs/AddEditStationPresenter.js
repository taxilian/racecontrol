define(["backbone", "jquery", "./AddEditStationView", "models/Station", "base/AcceptsOneViewDialog"],
function(Backbone, $, AddEditStationView, Station, Dialog) {

    var AddEditStationPresenter = Backbone.Router.extend({
        initialize: function(options) {
            var self = this;
            self.view = new AddEditStationView();
            self.eventId = options.eventId;
            self.stationNo = options.stationNo;
            self.dfd = options.dfd;
            self.view.bind("save", self.onSaveStation, self);
        },

        present: function() {
            var self = this,
                stationDfd = Station.obj.fetchById(self.eventId, self.stationNo);

            $.when(stationDfd).pipe(function(station) {
                self.station = station;
                self.view.setStation(self.stationNo, station);

                // render!
                var dlg = self.dlg = new Dialog({
                    title: (station ? "Edit" : "Add") + " Station #" + self.stationNo,
                    view: self.view,
                    buttons: ["OK", "Cancel"]
                });
                dlg.doDialog();
                dlg.bind("onButtonClick", function(button) {
                    if (button == "OK") {
                        self.view.onSaveClicked();
                    } else {
                        dlg.close();
                        self.cancel();
                    }
                });
            });
        },

        onSaveStation: function(data) {
            var self = this;
            var station = self.station;
            var stationListDfd = Station.list.fetchAll(self.eventId);
            if (!station) {
                data.raceEvent_id = self.eventId;
                data.stationNumber = self.stationNo;
                station = new Station.obj(data);
                $.when(stationListDfd).done(function(list) { list.add(station); });
            } else {
                station.set(data);
            }

            $.when(stationListDfd, station.save()).done(function() {
                self.dfd.resolve(station);
                self.dlg.close();
            });
        },

        cancel: function() {
            this.dfd.reject(this.event);
        }
    }, {
        showStation: function(eventId, sId) {
            var dfd = new $.Deferred();
            var obj = new this({eventId: eventId, stationNo: sId, dfd:dfd});
            obj.present();
            return dfd.promise();
        }
    });

    return AddEditStationPresenter;
});



