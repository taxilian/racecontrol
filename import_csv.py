if __name__ == "__main__":
    import settings
    from django.core.management import setup_environ
    setup_environ(settings)

from racecontrol.models import *
import sqlite3
from datetime import datetime
from Bag import Bag
import re
import csv, sys

def parseDate(dateStr):
    try:
        return datetime.strptime(dateStr, '%B %d %Y')
    except:
        return None

def import_file(filename, raceEventId):
    if not filename:
        print "Enter name of the csv file to process:",
        filename = raw_input("> ")
    try:
        csvFile = open(filename, 'rb')
        freader = csv.reader(csvFile)
        raceEvent = RaceEvent.objects.get(uuid=raceEventId) if raceEventId else None

        for line in freader:
            n = line[0]
            if n == 'race':
                # race,name,Date,id,
                parsedId = line[3] if len(line) > 3 else None
                name = line[1]
                date = parseDate(line[2])
                raceEvent, created = (raceEvent, false) if raceEvent else RaceEvent.objects.get_or_create(uuid=raceEventId if raceEventId else parsedId, date=date if not raceEventId else None)
                raceEvent.name = name
                raceEvent.save()

            if not raceEvent:
                break
            if n == 's':
                # s,stationNumber,name,distance
                stationNo = line[1]
                stationName = line[2] if len(line) > 1 else None
                distance = line[3] if len(line) > 2 else None

                station, created = Station.objects.get_or_create(raceEvent=raceEvent, stationNumber=stationNo)
                station.name = stationName
                station.distance = distance
                station.save()

            if n == 'r':
                # r,bibNumber,firstName,lastName,age,sex,note,team,home
                bib = line[1]
                first = line[2] if len(line) > 2 else None
                last = line[3] if len(line) > 3 else None
                age = line[4] if len(line) > 4 else None
                sex = line[5] if len(line) > 5 else None
                note = line[6] if len(line) > 6 else None
                team = line[7] if len(line) > 7 else None
                home = line[8] if len(line) > 8 else None
                participant, created = Participant.objects.get_or_create(raceEvent=raceEvent, bibNumber=bib)
                participant.firstName = first
                participant.lastName = last
                participant.age = age
                participant.sex = sex
                participant.note = note
                participant.team = team
                participant.home = home
                participant.save()
        csvFile.close()
    except Exception, ex:
        print "Failed. we are all going to die.", ex

if __name__ == "__main__":
    fname = sys.argv[1] if len(sys.argv) > 1 else None
    evtId = sys.argv[2] if len(sys.argv) > 2 else None
    import_file(fname, evtId)
