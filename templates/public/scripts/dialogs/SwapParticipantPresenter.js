define(["backbone", "jquery", "./SwapParticipantView", "models/Participant", "models/Station", "base/AcceptsOneViewDialog"],
function(Backbone, $, SwapParticipantView, Participant, Station, Dialog) {

    var SwapParticipantPresenter = Backbone.Router.extend({
        initialize: function(options) {
            var self = this;
            self.view = new SwapParticipantView();
            self.rId = options.rId;
            self.eventId = options.eventId;
            self.dfd = options.dfd;
            self.view.bind("save", self.onSaveParticipant, self);
        },

        present: function() {
            var self = this,
                runnerDfd = Participant.obj.fetchById(self.eventId, self.rId);

            $.when(runnerDfd).pipe(function(runner) {
                self.runner = runner;
                self.view.setParticipant(runner);

                // render!
                var dlg = self.dlg = new Dialog({
                    title: "Swap Bib Number",
                    view: self.view,
                    buttons: ["OK", "Cancel"]
                });
                dlg.doDialog();
                dlg.bind("onButtonClick", function(button) {
                    if (button == "OK") {
                        self.view.onSaveClicked();
                    } else {
                        dlg.close();
                        self.cancel();
                    }
                });
            });
        },

        onSaveParticipant: function(newBibNo) {
            var self = this;

            Participant.obj.swap(self.eventId, self.runner.id, newBibNo).done(function() {
                self.dfd.resolve();
                self.dlg.close();
            }).fail(function() {
                notify.error("Could not swap");
                console.log(arguments);
            });
        },

        cancel: function() {
            this.dfd.reject(this.event);
        }
    }, {
        showSwap: function(eventId, rId) {
            var dfd = new $.Deferred();
            var obj = new this({eventId: eventId, rId:rId, dfd:dfd});
            obj.present();
            return dfd.promise();
        }
    });

    return SwapParticipantPresenter;
});



