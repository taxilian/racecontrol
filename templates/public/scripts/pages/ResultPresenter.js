define(["backbone", "jquery", "underscore", "./ResultView", "./ResultSort",
       "dialogs/AddEditParticipantPresenter", "dialogs/AddEditEntryPresenter", "dialogs/AddEditStationPresenter", "dialogs/SpeedyEntryPresenter",
       "models/RaceEvent", "models/Participant", "models/Station", "models/Updater", "DateManager", "dialogs/StartTimePresenter"],
function(Backbone, $, _, DefaultView, ResultSort, AddEditParticipant, AddEditEntry, AddEditStation, SpeedyEntry,
         RaceEvent, Participant, Station, Updater, DateManager, StartTime) {

    function combineFilters() {
        var filters = arguments;
        return function() {
            var args = arguments;
            return _.all(filters, function(a) { return a.apply(null, arguments); });
        };
    }
    function ageLT(age) {
        return function(r) { return r.runner.get("age") <= age; };
    }
    function ageGT(age) {
        return function(r) { return r.runner.get("age") <= age; };
    }
    function isSex(sex) {
        return function(r) { return r.runner.get("sex") == sex; };
    }
    var ResultPresenter = Backbone.Router.extend({
        currentSort: null,
        sortDesc: false,
        initialize: function(options) {
            var self = this;
            self.view = new DefaultView();

            self.view.bind("setSort", self.setSort, self);
            self.view.bind("click", self.onSomethingClicked, self);
            self.view.bind("deactivate", self.stopUpdate, self);
            self.setSort("num", false);
            _.bindAll(this, "update");
        },

        present: function(container, eventId) {
            var self = this;
            self.stopUpdate();

            var eventDfd = RaceEvent.obj.fetchById(eventId);
            var runnerListDfd = Participant.list.fetchAll(eventId);
            DateManager.setEvent(eventId);

            $.when(eventDfd, runnerListDfd)
                .done(function(evt, rlist) {
                self.setRunnerList(rlist);
                self.event = evt;
                self.view.setData(evt, rlist);
                var dfd = rlist.getEntrys();
                rlist.bind("entryDownload", self.updateProgress, self);
                $.when(dfd).done(function() {
                    self.onDataLoaded();
                });
            });
            container.setView(self.view);
        },

        onDataLoaded: function() {
            var results = this.calculateResults();

            this.view.displayResults(results);
        },

        calculateResults: function() {
            var self = this;
            var raceEvent = self.event;
            var startStation = raceEvent.get("startStation");
            var finishStation = raceEvent.get("finishStation");

            // Get a list of all runners who have finished with their time (in seconds)
            var runnersWithTimes = self.rlist.map(function(r) {
                if (!r.has("entrys")) { return null; }
                var start_time = r.getTimeOutAt(startStation);
                var end_time = r.getTimeInAt(finishStation);
                if (!start_time || !end_time) { return null; }
                return {
                    runner: r,
                    time: end_time-start_time
                };
            });

            // Remove null values from the list (those without times)
            runnersWithTimes = _.filter(runnersWithTimes);

            var overall = runnersWithTimes;
            var overallMale = _.filter(overall, isSex("M"));
            var overallFemale = _.filter(overall, isSex("F"));

            var exclude = _.union([overallMale[0], overallMale[1]], [overallFemale[0], overallFemale[1]]);

            var mastersMale = _.filter(overall, combineFilters(isSex("M"), ageGT("39")) );
            mastersMale = _.difference(mastersMale, exclude);
            var mastersFemale = _.filter(overall, combineFilters(isSex("F"), ageGT("39")) );
            mastersFemale = _.difference(mastersFemale, exclude);

            exclude = _.union(exclude, [mastersMale[0], mastersMale[1], mastersFemale[0], mastersFemale[1]]);

            var maleGroupSource = _.difference(overallMale, exclude);
            var femaleGroupSource = _.difference(overallFemale, exclude);

            var mAgeGrp1 = _.filter(maleGroupSource, combineFilters(ageLT(20)));
            var mAgeGrp2 = _.filter(maleGroupSource, combineFilters(ageGT(19), ageLT(30)));
            var mAgeGrp3 = _.filter(maleGroupSource, combineFilters(ageGT(29), ageLT(40)));
            var mAgeGrp4 = _.filter(maleGroupSource, combineFilters(ageGT(39), ageLT(50)));
            var mAgeGrp5 = _.filter(maleGroupSource, combineFilters(ageGT(49), ageLT(60)));
            var mAgeGrp6 = _.filter(maleGroupSource, combineFilters(ageGT(59), ageLT(70)));
            var mAgeGrp7 = _.filter(maleGroupSource, combineFilters(ageGT(69)));

            var fAgeGrp1 = _.filter(femaleGroupSource, combineFilters(ageLT(20)));
            var fAgeGrp2 = _.filter(femaleGroupSource, combineFilters(ageGT(19), ageLT(30)));
            var fAgeGrp3 = _.filter(femaleGroupSource, combineFilters(ageGT(29), ageLT(40)));
            var fAgeGrp4 = _.filter(femaleGroupSource, combineFilters(ageGT(39), ageLT(50)));
            var fAgeGrp5 = _.filter(femaleGroupSource, combineFilters(ageGT(49), ageLT(60)));
            var fAgeGrp6 = _.filter(femaleGroupSource, combineFilters(ageGT(59), ageLT(70)));
            var fAgeGrp7 = _.filter(femaleGroupSource, combineFilters(ageGT(69)));

            var results = {
                overall: overall,
                overallMale: overallMale,
                overallFemale: overallFemale,

                mastersMale: mastersMale,
                mastersFemale: mastersFemale,

                ageGroupMale: {
                    "19 and younger": mAgeGrp1,
                    "20 to 29": mAgeGrp2,
                    "30 to 39": mAgeGrp3,
                    "40 to 49": mAgeGrp4,
                    "50 to 59": mAgeGrp5,
                    "60 to 69": mAgeGrp6,
                    "70 to 79": mAgeGrp7
                },
                ageGroupFemale: {
                    "19 and younger": mAgeGrp1,
                    "20 to 29": mAgeGrp2,
                    "30 to 39": mAgeGrp3,
                    "40 to 49": mAgeGrp4,
                    "50 to 59": mAgeGrp5,
                    "60 to 69": mAgeGrp6,
                    "70 to 79": mAgeGrp7
                }
            };
            return results;
        },

        setRunnerList: function(rlist) {
            var self = this;
            if (self.rlist != rlist && self.rlist) {
                self.rlist.unbind("change", self.onRunnerChange);
            }
            self.rlist = rlist;
            if (rlist) {
                rlist.bind("change", self.onRunnerChange, self);
            }
        },

        onRunnerChange: function(runner, entry) {
            var self = this;
            if (entry && entry.collection && entry.has("station_stationNumber")) {
                if (self.event.isFinishStation(entry.id)) {
                    this.rlist.calculateResults();
                }
                self.calculateStationStats();
            }
        }
    });

    return new ResultPresenter();
});



