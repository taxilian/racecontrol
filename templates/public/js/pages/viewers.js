var ViewersPage = RB.LWClass();
_p = ViewersPage.prototype;

_p.initialize = function() {
    this.rc = new RaceControl();
    this.addInit = false;

    this.rc.getRaceEvents(false, this.$("raceEventsLoaded"));
};

_p.raceEventsLoaded = function(eventList) {
    this.eventList = eventList;
    this.updateEventList();
}

_p.updateEventList = function() {
    var self = this;
    $("#eventContainer ul").remove();

    var lookupFunc = function(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        var id = $(this).parents("li").attr("evtId");
        self.eventList[id].lookup();
    }

    for (var i in this.eventList) {
        var event = this.eventList[i];
        var line = $( event.$("getLine")("ul", "li", {outerClass: "left"}) );
        line.prepend('<li style="width: 10%" evtId="%s"><a class="lookup" style="color: red;">Lookup Runner</a></li>'.format(i));
        line.find(".lookup").click(lookupFunc);
        line.find("li:gt(2)").hide();
        $("#eventContainer").append(line);
    }

    RB.html.addHover($("#eventContainer ol, #eventContainer ul"), "hover-state");
    $("#eventContainer ul").click(function(evt) {
            var event = self.eventList[$(this).attr("id")];
            event.viewResults();
        });
    //$("#eventContainer ol")
}

delete _p;
