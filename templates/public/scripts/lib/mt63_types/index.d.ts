import {
    MT63Client,
} from './MT63Client';

import { Amp } from './amp';
import { Deamp } from './deamp';
export { Amp, Deamp, MT63Client };

export function init(path?: string): Promise<MT63Client>;

export as namespace FLAmp;