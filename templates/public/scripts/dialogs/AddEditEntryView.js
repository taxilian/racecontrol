define(['base/BaseView', 'underscore', 'defaults',
        '_tpl!./jst/AddEditEntry.jst', 'ext/inputField'], function(BaseView, _, defaults, tpl) {

    var AddEditEntryView = BaseView.extend({
        event: null,

        events: {
        },

        render: function() {
            var self = this;
            var $el = $(self.el);
            $el.html(tpl({runner: self.runner, entry: self.entry, evt: self.event}));
            this.$(".dateField").datepicker({dateFormat: defaults.dateFormat});
            this.$(".timeField").timeField({
                allowNumeric: true,
                allowAlpha: false,
                transform: {".": ":"}
            });
            return self;
        },

        setEntry: function(entry) {
            this.entry = entry;
        },

        setRunner: function(runner) {
            this.runner = runner;
        },

        setEvent: function(event) {
            this.event = event;
        },

        onSaveClicked: function() {
            var self = this;

            var timeInTime = self.$("input[name=fld_time_in].timeField").val();
            var timeInDate = self.$("input[name=fld_time_in].dateField").val();
            var timeOutTime = self.$("input[name=fld_time_out].timeField").val();
            var timeOutDate = self.$("input[name=fld_time_out].dateField").val();

            var fieldValues = {
                time_in: Date.parse(timeInDate + " " + timeInTime),
                time_out: Date.parse(timeOutDate + " " + timeOutTime)
            };

            this.trigger("save", fieldValues);
        }
    });

    return AddEditEntryView;
});


