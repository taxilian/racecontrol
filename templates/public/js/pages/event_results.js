var EventResultsPage = RB.LWClass();
_p = EventResultsPage.prototype;

_p.initialize = function(evtId) {
    var self = this;
    this.debug = false;
    this.seconds = 45;
    this.updateTimerId = -1;
    this.evtId = evtId;
    this.rc = new RaceControl();
    this.rc.$("getRaceEventResults")(evtId, this.$("raceEventLoaded"));
    this.dateField = null;

    this.resultBuilder = null;
};

_p.wrapInColumns = function(male, female, klass) {
    var html = '<div class="resultTable %s">'.format(klass);
    html += '<div class="mColumn">%s</div>'.format(male);
    html += '<div class="fColumn">%s</div>'.format(female);
    html += "</div>";
    return html;
}

_p.raceEventLoaded = function(event) {
    this.event = event;
    this.event.$("watchForUpdates")(this.$("updatesLoaded"), this.seconds);
    if (this.debug) {
        console.time("Result Calculation timer");
    }
    this.resultBuilder = new ResultBuilder(this.event.participants, this.event.startStation.value, this.event.finishStation.value, 2);

    var data = this.resultBuilder.$("getTimeSpanList")();
    var html;

    var tmp = new ResultTable("Overall Results", data.overall, "overall");
    $("#resultsMain").html('<div class="resultTable">%s</div>'.format(tmp.getHtml()));

    var mData = this.resultBuilder.$("splitList")(data.male, 7);
    var fData = this.resultBuilder.$("splitList")(data.female, 7);
    var m = new ResultTable("Overall Male", mData.overall, "m_overall");
    var f = new ResultTable("Overall Female", fData.overall, "f_overall");

    $("#resultsMain").append(this.wrapInColumns(m.$("getHtml")(), f.$("getHtml")(), "mfoverall"));

    m = new ResultTable("Masters Male", mData.masters, "m_masters");
    f = new ResultTable("Masters Female", fData.masters, "f_masters");
    $("#resultsMain").append(this.wrapInColumns(m.$("getHtml")(), f.$("getHtml")(), "mfmasters"));

    for (var i = 0; i < mData.categories.length; i++) {
        var title = "Age group: %s to %s".format((10 * i) + 10, (10 * i) + 19);
        m = new ResultTable("%s Male".format(title), mData.categories[i], "m_cat_%s".format(i));
        f = new ResultTable("%s Female".format(title), fData.categories[i], "m_cat_%s".format(i));
        $("#resultsMain").append(this.wrapInColumns(m.$("getHtml")(), f.$("getHtml")(), "mf_cat_%s".format(i)));
    }

    var teamResults = this.resultBuilder.$("getGroupResults")(data.overall);

    for (var size in teamResults) {
        var tmp = new TeamResultTable("%s Person team results".format(size), teamResults[size], "team_%s".format(size));
        $("#resultsMain").append('<div class="teamResultTable">%s</div>'.format(tmp.getHtml()));
    }

    if (this.debug) {
        console.timeEnd("Result Calculation timer");
    }
    $("#pleasewait").hide();
}

_p.updatesLoaded = function(data) {
    if (data.stations || data.event) {
        this.raceEventLoaded(this.event);
    } else {
        for (var i in data.participants) {
            this.raceEventLoaded(this.event);
            return;  // Update once, no need to update again
        }
        for (var i in data.entries) {
            if (data.entries[i].station_stationNumber == this.event.startStation.value
                || data.entries[i].station_stationNumber == this.event.finishStation.value) {
                this.raceEventLoaded(this.event);
                return;
            }
        }
    }
}

delete _p;
