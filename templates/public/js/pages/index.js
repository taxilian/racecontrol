var IndexPage = RB.LWClass();
_p = IndexPage.prototype;

_p.initialize = function() {
    this.rc = new RaceControl();
    this.addInit = false;

    this.rc.getRaceEvents(false, this.$("raceEventsLoaded"));
};

_p.raceEventsLoaded = function(eventList) {
    this.eventList = eventList;
    this.updateEventList();
}

_p.updateEventList = function() {
    var self = this;
    $("#eventContainer ul").remove();

    var editRespHandler = function(success, data) {
        if (success) {
            var id = data.uuid.value;
            self.eventList[id] = data;
            self.updateEventList();
        } else {
            RB.alert("Could not save", data);
        }
    }

    var editFunc = function(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        var id = $(this).parents("li").attr("evtId");
        self.eventList[id].$("edit")(editRespHandler);
    }

    var delFunc = function(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        var id = $(this).parents("li").attr("evtId");
        self.eventList[id].$("deleteRecord")(function(success, data) {
            if (success) {
                delete self.eventList[id];
                self.updateEventList();
            } else {
                RB.alert("Could not delete", data);
            }
        });
    }

    var addFunc = function(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        self.rc.$("newRaceEvent")({}).$("edit")(editRespHandler);
    }

    for (var i in this.eventList) {
        var event = this.eventList[i];
        var line = $( event.$("getLine")("ul", "li", {outerClass: "left"}) );
        line.prepend('<li style="width: 10%" evtId="%s"><a class="edit">edit</a> | <a class="del">del</a></li>'.format(i));
        line.find(".edit").click(editFunc);
        line.find(".del").click(delFunc);
        line.find("li:gt(2)").hide();
        $("#eventContainer").append(line);
    }

    if (!this.addInit) {
        $("#addNew").click(addFunc);
    }

    RB.html.addHover($("#eventContainer ol, #eventContainer ul"), "hover-state");
    $("#eventContainer ul").click(function(evt) {
        var event = self.eventList[$(this).attr("id")];
        event.open();
    });
    //$("#eventContainer ol")
}

delete _p;
