define(["backbone", "underscore", "models/RaceEvent"], function(Backbone, _, RaceEvent) {

    var DateManager = Backbone.Model.extend({
        initialize: function() {
        },

        getCurrentDate: function() {
            return this.get("date").toString("MMM d, yyyy");
        },

        setCurrentDate: function(d) {
            if (typeof(d) == "string") {
                d = Date.parse(d);
            }
            this.set({date: d});
        },

        setEvent: function(eventId) {
            var self = this;
            if (this.evt && this.evt.id == eventId ) {
                return;
            }
            RaceEvent.obj.fetchById(eventId).done(function(evt) {
                self.evt = evt;
                self.setCurrentDate(evt.get("date"));
            });
        }
    });

    return new DateManager();

});
