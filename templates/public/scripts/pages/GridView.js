define(['base/BaseView', 'underscore', 'defaults',
        '_tpl!./jst/Grid.jst', '_tpl!./jst/Grid_RunnerEntry.jst', '_tpl!./jst/Grid_StationHeader.jst', '_tpl!./jst/Grid_timeFormat.jst', '_tpl!./jst/Nav.jst'],
        function(BaseView, _, defaults, tpl, runnerEntryTpl, stationHeaderTpl, timeFormatTpl, navTpl) {

    function ordinalSuffix(intNum) {
        if (intNum == "DNF") { return "DNF"; }
        if (intNum === null || intNum === undefined) { return "N/A"; }
        return (((intNum = Math.abs(intNum) % 100) % 10 == 1 && intNum != 11) ? "st" :
                (intNum % 10 == 2 && intNum != 12) ? "nd" :
                (intNum % 10 == 3 && intNum != 13) ? "rd" : "th");
    }

    var GridView = BaseView.extend({
        event: null,
        runnerList: null,
        stationList: null,
        sortFunc: null,

        initialize: function() {
            this.em = $("#emRuler").width();
            _.bindAll(this, "autoSize");
            $(window).resize(this.autoSize);
        },

        clearData: function() {
            var self = this;
            if (self.event) {
                self.event.unbind("change", self.eventChange);
            }
            if (self.runnerList) {
                self.runnerList.unbind("change", self.rlistChange);
                self.runnerList.unbind("add", self.refresh);
            }
            if (self.stationList) {
                self.stationList.unbind("change", self.refresh);
                self.stationList.unbind("add", self.refresh);
            }
            self.event = self.stationList = self.runnerList = null;
        },

        autoSize: function() {
            if (!this.isActive()) { return; }
            var em = this.em;
            var availSize = $(window).height();

            $("#gridContainer").height(availSize - (15 * em));

            var sum = 20;
            $("#gridHeader li").each(function() { sum += $(this).outerWidth(); });
            $("#gridHolder").css("min-width", sum + "px");
        },

        setData: function(evt, rlist, slist) {
            var self = this;
            self.clearData();
            self.event = evt;
            if (evt) {
                evt.bind("change", self.eventChange, self);
            }
            self.runnerList = rlist;
            if (rlist) {
                rlist.bind("change", self.rlistChange, self);
                rlist.bind("add", self.refresh, self);
            }
            self.stationList = slist;
            if (slist) {
                slist.bind("change", self.refresh, self);
                slist.bind("add", self.refresh, self);
            }

            if (self.isActive()) {
                self.render();
            }
        },

        events: {
            "click #gridHeader li.sort":          "onSortHeaderClicked",
            "click .stationHeader a.sort":        "onStationSortHeaderClicked",
            "click .stationHeader a.edit":        "onStationEditClicked",
            "click #gridContainer ul li.name":    "onRunnerNameClicked",
            "click #gridContainer ul li.station": "onRunnerTimeClicked",
            "click button.speedy":                "onSpeedyEntryClicked",
            "click button.setStart":              "onSetStartClicked",
            "click button#recalc":                "onRecalcStats"
        },

        onSortHeaderClicked: function(evt) {
            evt.preventDefault();
            var $el = $(evt.currentTarget),
                sortType = $el.attr("class").split(/\s+/).shift();

            this.trigger("setSort", sortType);
        },

        onStationSortHeaderClicked: function(evt) {
            evt.preventDefault();
            var $el = $(evt.currentTarget).closest(".sort"),
                $st = $el.closest(".station"),
                id = $st.attr("id").split("_").pop(),
                type = $el.attr("class").substr(0,1);

            this.trigger("setSort", "s" + type + id);
        },

        setSortFunc: function(sortFunc) {
            this.sortFunc = sortFunc;
            if (this.isActive()) {
                this.render();
            }
        },

        onRecalcStats: function(evt) {
            this.trigger("recalcNow");
        },

        showElapsedTime: function(runner, station, prevStation, evt) {
            if (!prevStation) {
                return "N/A";
            } else if (runner.wasSeenAt(station.id)) {
                return "Time from last station: " + runner.getFormattedTimeDelta(station.id) + "(" + runner.getPace(station.id) + "mph)";
            } else if (runner.wasSeenAt(prevStation.id)) {
                var prevPace = runner.getPace(prevStation.id);
                if (!prevPace) { return "N/A"; }
                var distDiff = parseFloat(station.get('distance')) - parseFloat(prevStation.get('distance'));
                if (isNaN(distDiff)) {
                    return "N/A";
                }
                var minPace = prevPace * 0.95;
                var maxPace = prevPace * 1.2;

                var minEta = (distDiff / minPace);
                var maxEta = (distDiff / maxPace);

                var timeOut = runner.getTimeOutAt(prevStation.id) || runner.getTimeInAt(prevStation.id);
                if (!timeOut) { return "N/A"; }
                var hr = 1000 * 60 * 60;
                var minTimeOut = new Date(timeOut.getTime() + maxEta * hr);
                var maxTimeOut = new Date(timeOut.getTime() + minEta * hr);

                var minString = minTimeOut.toString(defaults.timeFormat);
                var maxString = maxTimeOut.toString(defaults.timeFormat);
                return "Est. arrival between " + minString + " and " + maxString;
            } else {
                return "N/A";
            }
        },

        render: function() {
            var self = this;
            var $el = $(self.el);
            $el.addClass("newgrid");
            $el.attr("id", "gridMain");
            if (self.event && self.runnerList && self.stationList) {
                var rlist = self.runnerList.getSortedList(self.sortFunc);
                // console.log("Grid full render");
                var gridScrollTop = $("#gridContainer").scrollTop();
                $el.html(tpl({
                    stations: self.stationList,
                    runners: rlist,
                    evt: self.event,
                    stationHeader: stationHeaderTpl,
                    runnerEntry: runnerEntryTpl,
                    timeFormat: timeFormatTpl,
                    ordinalSuffix: ordinalSuffix,
                    showElapsedTime: self.showElapsedTime
                }));
                self.autoSize();
                $("#head-center").text(self.event.get("name"));
                $("#nav").html(navTpl({evt: self.event}));
                $("#gridContainer").scrollTop(gridScrollTop);
            } else {
                $el.html("Loading data, please wait...");
            }
            this.$("button").button();
            //$el.html(tpl({events: self.eventList}));
            return self;
        },

        updateProgress: function(progress, total) {
            var prog = $(this.el).find("#loading_progress");
            prog.html("Loading " + progress + " of " + total);
        },

        updatePosition: function(runnerNo, position) {
            if (this.isActive()) {
                var $el = this.$("#p_" + runnerNo), place, suffix;
                if (position === null) {
                    place = "";
                    suffix = "N/A";
                } else if (position === "DNF") {
                    place = "";
                    suffix = "DNF";
                } else {
                    place = position;
                    suffix = ordinalSuffix(place);
                }
                $el.find(".place").html(place);
                $el.find(".suffix").html(suffix);
            }
        },

        refresh: function() {
            if (this.isActive()) {
                this.render();
            }
        },

        deactivate: function() {
            this.clearData();
            $("#head-center").text("");
            $("#nav").html(navTpl({evt:null}));
            this.trigger("deactivate");
        },

        eventChange: function() {
        },
        rlistChange: function(runner) {
            // console.log("rlistChange", runner);
            var self = this;
            var entryId = "#row_" + runner.id;
            var $e = self.$(entryId);
            $e.html(runnerEntryTpl({
                runner: runner,
                stations: self.stationList,
                evt: self.event,
                timeFormat: timeFormatTpl,
                ordinalSuffix: ordinalSuffix,
                showElapsedTime: self.showElapsedTime
            }));
            var wasDropped = $e.hasClass("dropped");
            var isDropped = runner.isDropped();
            if (isDropped && !wasDropped) {
                $e.addClass("dropped");
                self.trigger("recalc");
            } else if (!runner.isDropped() && wasDropped) {
                $e.removeClass("dropped");
                self.trigger("recalc");
            }
        },

        rIdFromEvent: function(evt) {
            var row = $(evt.currentTarget).closest(".participant_row");
            var rId = row.attr("id").split("_").pop();
            return rId;
        },

        onRunnerNameClicked: function(evt) {
            evt.preventDefault();
            var rId = this.rIdFromEvent(evt);

            this.trigger("click", "runnerName", rId);
        },

        onRunnerTimeClicked: function(evt) {
            evt.preventDefault();
            var elId = $(evt.currentTarget).closest(".station").attr("id").split("_"),
                runnerNo = elId[1], stationNo = elId[2];

            this.trigger("click", "runnerTime", runnerNo, stationNo);
        },

        onStationEditClicked: function(evt) {
            evt.preventDefault();
            var sId = $(evt.currentTarget).closest(".stationHeader").attr("id").split("_").pop();
            this.trigger("click", "editStation", sId);
        },

        onSpeedyEntryClicked: function(evt) {
            this.trigger("click", "speedyEntry");
        },

        onSetStartClicked: function(evt) {
            this.trigger("click", "setStart");
        }

    });

    return GridView;
});


