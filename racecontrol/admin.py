from racecontrol.models import *
from django.contrib import admin

admin.site.register(RaceEvent)
admin.site.register(Station)
admin.site.register(Participant)
admin.site.register(Entry)
