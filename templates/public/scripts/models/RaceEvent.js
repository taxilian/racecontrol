define(["backbone", "underscore", "jquery", "defaults", "datelib"], function(Backbone, _, $, defaults) {

    var RaceEvent, RaceEvents;

    RaceEvent = Backbone.Model.extend({
        idAttribute: "uuid",
        urlRoot: '/newAPI/events',

        getDateObject: function() {
            var date = this.get("date");
            if (!(date instanceof Date)) 
                date = Date.parse(this.get("date"));
            return date;
        },

        getDate: function() {
            return this.getDateObject().toString(defaults.dateFormat);
        },
        getTime: function() {
            return this.getDateObject().toString(defaults.timeFormat);
        },
        isStartStation: function(stationNo) {
            return stationNo == this.get("startStation");
        },
        isFinishStation: function(stationNo) {
            return stationNo == this.get("finishStation");
        }
    }, {
        list: null,
        fetchById: function(id) {
            return RaceEvents.fetchAll().pipe(function(list) {
                return list.get(id);
            });
        }
    });

    RaceEvents = Backbone.Collection.extend({
        model: RaceEvent,
        url: '/newAPI/events'
    }, {
        list: null,
        fetchAll: function() {
            var self = this;
            if (self.list) {
                return self.list;
            }
            var tmp = new self();
            self.list = tmp.fetch().pipe(function() {
                return tmp;
            });
            return self.list;
        }
    });

    return {
        obj: RaceEvent,
        list: RaceEvents
    };
});
