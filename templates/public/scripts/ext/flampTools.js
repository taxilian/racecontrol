/**
 * 
 * @param {typeof FLAmp} flampWeb 
 */
function flampToolsModule(flampWeb) {
    flampWeb.init("/public/scripts/flampWeb/");
    return flampWeb;
}
define(["flampWeb"], flampToolsModule);