@echo off

echo Make sure the mysql container is fully running and initialized and that the racecontrol container is running

docker exec mysql_1 mysql -u root -praceevent -e "CREATE DATABASE racecontrol"
docker exec mysql_1 mysql -u root -praceevent -e "create user rc@'%' identified with mysql_native_password by 'raceevent'"
docker exec mysql_1 mysql -u root -praceevent -e "grant all privileges on racecontrol.* to rc@'%'"

docker exec -it -w /home/docker/code/app/racecontrol5/ racecontrol /usr/bin/python manage.py syncdb