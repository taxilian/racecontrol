#!/usr/bin/env bash
IMG=registry.gitlab.com/taxilian/racecontrol/server
CNAME=racecontrol
docker pull $IMG

docker stop $CNAME
docker rm $CNAME
docker run \
    -d --restart=always \
    --name $CNAME \
    -e VIRTUAL_HOST=racecontrol.local \
    -e VIRTUAL_PORT=80 \
    --link mysql_1:db \
    -v $(pwd):/home/docker/code/app/racecontrol5 \
    -v $(pwd)/docker_settings.py:/home/docker/code/app/racecontrol5/settings.py \
    -p 80:80 \
    $IMG
