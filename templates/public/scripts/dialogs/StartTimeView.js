define(['base/BaseView', 'underscore',
        '_tpl!./jst/StartTime.jst'], function(BaseView, _, tpl) {

    var StartTimeView = BaseView.extend({
        event: null,

        events: {
        },

        render: function() {
            var self = this;
            var $el = $(self.el);
            $el.html(tpl({}));
            self.$(".time").timeField({
                allowNumeric: true,
                allowAlpha: false,
                transform: {".": ":"}
            });
            return self;
        },

        setParticipant: function(runner) {
            this.runner = runner;
        },

        setStations: function(stations) {
            this.stations = stations;
        },

        onSaveClicked: function() {
            var self = this;

            var newTime = self.$("input[name=fld_timeOut]").val();
            var oldTime = self.$("input[name=fld_timeOut_old]").val();
            this.trigger("save", newTime, oldTime);
        }
    });

    return StartTimeView;
});


