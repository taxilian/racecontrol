define(['backbone'], function(Backbone) {

    var bindRouteFunc = function(n, p, f, r) {
        var retfunc = function() {
            r.closeAllDialogs();
            r.beforeRoute(n, p, arguments);
            f.apply(r, arguments);
            r.afterRoute(n, p, arguments);
        };
        return retfunc;
    };

    var BaseRouter = Backbone.Router.extend({
        _bindRoutes : function() {
            if (!this.routes) {
                return;
            }
            var routes = [];
            for (var route in this.routes) {
                if (this.routes.hasOwnProperty(route)) {
                    routes.unshift([route, this.routes[route]]);
                }
            }
            var self = this;
            for (var i = 0, l = routes.length; i < l; i++) {
                var p = routes[i][0]; // Pattern
                var n = routes[i][1]; // Name (and function name)
                var f = self[n];

                // This will make each route call the beforeRoute and afterRoute methods of the
                // router class, which makes it possible to have things happen on *all* routes
                this.route(p, n, bindRouteFunc(n, p, f, self));
            }
        },
        beforeRoute : function(routeName, routePattern, params) {},
        afterRoute : function(routeName, routePattern, params) {},
        closeAllDialogs : function() { $(".ui-dialog > .ui-dialog-content").dialog("close"); }
    });
    return BaseRouter;
});
