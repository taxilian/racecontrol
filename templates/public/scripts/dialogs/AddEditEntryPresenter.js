define(["backbone", "jquery", "./AddEditEntryView",
       "models/Participant", "models/Entry", "models/RaceEvent", "base/AcceptsOneViewDialog"],
function(Backbone, $, AddEditEntryView, Participant, Entry, RaceEvent, Dialog) {

    var AddEditEntryPresenter = Backbone.Router.extend({
        initialize: function(options) {
            var self = this;
            self.view = new AddEditEntryView();
            self.eventId = options.eventId;
            self.runnerNo = options.participantNo;
            self.stationNo = options.stationNo;
            self.dfd = options.dfd;
            self.view.bind("save", self.onSaveEntry, self);
        },

        present: function() {
            var self = this,
                runnerDfd = Participant.obj.fetchById(self.eventId, self.runnerNo),
                entryDfd = runnerDfd.pipe(function(runner) {
                    return runner.getOrCreateEntry(self.stationNo);
                }),
                eventDfd = RaceEvent.obj.fetchById(self.eventId);

            $.when(runnerDfd, entryDfd, eventDfd).pipe(function(runner, entry, event) {
                self.runner = runner;
                self.entry = entry;
                self.view.setEntry(entry);
                self.view.setRunner(runner);
                self.view.setEvent(event);

                // render!
                var dlg = self.dlg = new Dialog({
                    title: "Edit Entry for " + runner.formatName() + ", Station " + self.entry.id,
                    view: self.view,
                    buttons: ["OK", "Cancel"]
                });
                dlg.doDialog();
                dlg.bind("onButtonClick", function(button) {
                    if (button == "OK") {
                        self.view.onSaveClicked();
                    } else {
                        dlg.close();
                        self.cancel();
                    }
                });
            });
        },

        onSaveEntry: function(data) {
            var self = this;
            self.entry.set(data);

            self.entry.save().done(function() {
                self.dfd.resolve(self.runner);
                self.dlg.close();
            });
        },

        cancel: function() {
            this.dfd.reject(this.event);
        }
    }, {
        showEntry: function(eventId, rId, sId) {
            var dfd = new $.Deferred();
            var obj = new this({eventId: eventId, participantNo:rId, stationNo: sId, dfd:dfd});
            obj.present();
            return dfd.promise();
        }
    });

    return AddEditEntryPresenter;
});



