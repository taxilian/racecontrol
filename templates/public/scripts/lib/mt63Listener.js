/// <reference path="./mt63_types/index.d.ts" />

/**
 * 
 * @param {Backbone} Backbone 
 * @param {typeof FLAmp} flampTools
 * 
 * @returns {listen: (deamp: FLAmp.Deamp)=> void, events: Backbone.Model, stopListening: () => void}
 */
function mt63ListenerModule(Backbone, flampTools) {
    var listening = false;
    var lastReceivedAt = 0;
    /** @type AudioContext */
    var audioCtx;

    /** @type MediaStreamTrack */
    var audioTrack;

    var initialized = false;

    var events = new Backbone.Model();

    /** @type FLAmp.Deamp */
    var deamp = new flampTools.Deamp();

    /** @type FLAmp.MT63Client */
    var mt63;

    function evtHandler(name, evt) {
        events.trigger(name, evt);
    }
    var newFileHandler = evtHandler.bind(null, "newFile");
    var fileUpdateHandler = evtHandler.bind(null, "fileUpdate");
    var fileCompleteHandler = evtHandler.bind(null, "fileComplete");
    deamp.newFileEvent.on(newFileHandler);
    deamp.fileUpdateEvent.on(fileUpdateHandler);
    deamp.fileCompleteEvent.on(fileCompleteHandler);
    events.bind('rx',
    /**
     * @param {string} rxString
     */
     function(rxString) {
        deamp.ingestString(rxString);
    });

    /**
     * @param {FLAmp.Deamp} newDeamp
     */
    function initializeDeamp(newDeamp) {
        if (deamp && deamp != newDeamp) {
            deamp.newFileEvent.off(newFileHandler);
            deamp.fileUpdateEvent.off(fileUpdateHandler);
            deamp.fileCompleteEvent.off(fileCompleteHandler);
        }
        exportObj.deamp = deamp = newDeamp;
        initialized = true;
    }
    function stopListening() {
        if (audioTrack) {
            audioTrack.stop();
        }
    }

    /**
     * 
     * @param {FLAmp.Deamp} newDeamp 
     */
    function listen() {
        if (!mt63) {
            mt63 = new flampTools.MT63Client();
        }
        if (!audioCtx) {
            audioCtx = new (window.AudioContext || window.webkitAudioContext)();
        }
        if (listening) { return; }
        deamp.clearBuffer();
        lastReceivedAt = 0;

        var isLocalhost = window.location.hostname === 'localhost' ||
            window.location.hostname === '127.0.0.1';
        if (window.location.protocol !== 'https:' && !isLocalhost) {
            alert('HTTPS is required for microphone access, and this site has no SSL cert yet. Sorry!');
        }
        navigator.mediaDevices.getUserMedia({audio: true}).then(function gotUserMedia(stream) {
            audioTrack = stream.getAudioTracks()[0];
            var audioInput = audioCtx.createMediaStreamSource(stream);
            var scriptProc = audioCtx.createScriptProcessor(16384, 1, 1);

            stream.addEventListener('inactive', function() {
                listening = false;
                scriptProc.onaudioprocess = null;
                events.trigger("listening", false);
            }, {once: true});

            scriptProc.onaudioprocess = function processAudioEvent(aEvt) {
                var inpBuffer = aEvt.inputBuffer.getChannelData(0);
                var result = mt63.processAudio(inpBuffer, inpBuffer.length, aEvt.inputBuffer.sampleRate);

                if (result) {
                    // Hey, we received something!
                    events.trigger("rx", result);
                }
            };

            audioInput.connect(scriptProc);
            scriptProc.connect(audioCtx.destination);
            
            listening = true;
            events.trigger("listening", true);
        });
    }

    var exportObj = {
        listen: listen,
        stopListening: stopListening,
        isListening: function() { return listening; },
        deamp: deamp,
        events: events
    };
    initializeDeamp(deamp);
    return exportObj;
}
define(["backbone", "../ext/flampTools"], mt63ListenerModule);