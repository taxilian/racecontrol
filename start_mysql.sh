#!/usr/bin/env bash
IMG=mariadb
CNAME=mysql_1
docker pull $IMG
docker stop $CNAME
docker rm $CNAME
docker run \
    -d \
    --restart=always \
    --name $CNAME \
    -e MYSQL_ROOT_PASSWORD='raceevent' \
    -e TERM=dumb \
    -p 3306:3306 \
    -v /Users/richard/data/mysql:/var/lib/mysql \
    $IMG
