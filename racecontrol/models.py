from django.db import models
from django_extensions.db import models as ext_models
from django_extensions.db import fields as ext_fields
from Bag import Bag
from django.conf import settings
from soundex import soundex
import datetime
import uuid

#////////////////////////////////////////
#///         Custom UUIDField         ///
#////////////////////////////////////////
class UUIDField(ext_fields.UUIDField):
    def pre_save(self, model, add):
        if add and not getattr(model, self.attname):
            setattr(model, self.attname, unicode(uuid.uuid1()))
        return super(UUIDField, self).pre_save(model, add)

#////////////////////////////////////////
#///         Station UUIDField        ///
#////////////////////////////////////////
class StationUUIDField(ext_fields.UUIDField):
    def pre_save(self, model, add):
        modifiedBy = getattr(model, "modifiedBy")
        if modifiedBy:
            setattr(model, self.attname, modifiedBy)
        else:
            modifiedBy = settings.STATION_UUID
            setattr(model, self.attname, modifiedBy)
        return modifiedBy


#////////////////////////////////////////
#///      BaseModelManager class      ///
#////////////////////////////////////////
class BaseModelManager(models.Manager):
    def get_query_set(self):
        return self.model.QuerySet(self.model)

    def __getattr__(self, attr, *args):
        return getattr(self.get_query_set(), attr, *args)

#////////////////////////////////////////
#///         BaseModel class          ///
#////////////////////////////////////////
class BaseModel(ext_models.TimeStampedModel):
    class Meta:
        abstract = True

    class QuerySet(models.query.QuerySet):
        def toArray(self, deep=False):
            output = []
            for entry in self:
                output.append(entry.toDict(deep))
            return output
        def toDict(self, deep=False, keyname="__default__"):
            output = {}
            if hasattr(self.model, "MyMeta") and hasattr(self.model.MyMeta, "deep_copy"):
                if self.model.MyMeta.deep_copy == False:
                    deep = False
            if keyname == "__default__":
                if hasattr(self.model, "MyMeta") and hasattr(self.model.MyMeta, "indexField"):
                    keyname = self.model.MyMeta.indexField
                    if keyname.find("__") > -1:
                        field, subfield = keyname.split("__")
                else:
                    keyname = "pk"
            for entry in self:
                if keyname.find("__") > -1:
                    key = getattr(getattr(entry, field), subfield)
                else:
                    key = getattr(entry, keyname)
                output[key] = entry.toDict(deep)
            return output

    def toDict(self, deep = False):
        entry = Bag(self.__dict__)
        if "_state" in entry:
            del entry["_state"]

        if hasattr(self, "MyMeta"):
            if hasattr(self.MyMeta, "exclude_from_dict"):
                for field in self.MyMeta.exclude_from_dict:
                    entry.pop(field, None)
            if hasattr(self.MyMeta, "extraFields"):
                for field in self.MyMeta.extraFields:
                    if field.find("__") > -1:
                        field, subfield = field.split("__")
                        fkField = getattr(self, field)
                        if fkField:
                            entry["%s_%s" % (field, subfield)] = getattr(getattr(self, field), subfield)
                        else:
                            entry["%s_%s" % (field, subfield)] = None
                    else:
                        entry[field] = getattr(self, field)

        if deep:
            for fname in dir(self):
                if fname[-4:] == "_set":
                    field = "%ss" % fname[:-4]
                    entry[field] = getattr(self, fname).toDict(deep)

        return dict(entry)

    @classmethod
    def loadArray(cls, arr):
        output = []
        for inp in arr:
            output.append(cls.loadDict(inp))
        return output

    @classmethod
    def loadDict(cls, inp):
        if inp["uuid"] in (-1, "-1", None):
            obj = cls()
        else:
            search = cls.objects.filter(uuid=inp["uuid"])
            if len(search):
                obj = search[0]
            else:
                obj = cls()
                obj.uuid = inp["uuid"]
        obj.fromDict(inp)
        return obj.toDict()

    def fromDict(self, inp):
        changed = False
        for field in inp:
            if field not in ("uuid", "modified", "created"):
                if hasattr(self, field):
                    oldVal = getattr(self, field)
                    if oldVal != inp[field]:
                        setattr(self, field, inp[field])
                        if oldVal != getattr(self, field):
                            changed = True
        if (changed):
            self.save()
            return True
        else:
            return False

    lastModifiedBy = StationUUIDField(default=settings.STATION_UUID)
    modifiedBy = None

# Create your models here.
#////////////////////////////////////////
#///          RaceEvent class         ///
#////////////////////////////////////////
class RaceEvent(BaseModel):
    objects = BaseModelManager()
    class Meta:
        ordering = ('date',)
    class MyMeta:
        deep_copy = ("Station", "Participant",)
        exclude_from_dict = ("created", "modified")

    uuid = UUIDField(primary_key = True, auto = False)
    date = models.DateTimeField("Event Date")
    name = models.CharField(max_length=200)
    startStation = models.IntegerField(blank=True,null=True,db_index=True)
    finishStation = models.IntegerField(blank=True,null=True,db_index=True)

    def __unicode__(self):
        return u"%s" % self.name

#////////////////////////////////////////
#///          Station class           ///
#////////////////////////////////////////
class Station(BaseModel):
    objects = BaseModelManager()
    class Meta:
        unique_together = (("raceEvent", "stationNumber"),)
        ordering = ("stationNumber",)

    class MyMeta:
        deep_copy = False
        exclude_from_dict = ("created", "modified")
        indexField = "stationNumber"

    uuid = UUIDField(primary_key = True)
    raceEvent = models.ForeignKey("RaceEvent")
    name = models.CharField(max_length=200)
    stationNumber = models.IntegerField(db_index=True)
    distance = models.CharField(max_length=6, blank=True)

    @classmethod
    def loadDict(cls, inp):
        if inp.has_key("stationNumber") and inp.has_key("raceEvent_id"):
            evt = RaceEvent.objects.get(uuid=inp["raceEvent_id"])
            obj, existed = cls.objects.get_or_create(stationNumber=inp["stationNumber"],raceEvent=evt)
            obj.raceEvent_id = inp["raceEvent_id"]
        elif inp["uuid"] in (-1, "-1", None):
            obj = cls()
        else:
            obj = cls.objects.get(uuid=inp["uuid"])
        obj.fromDict(inp)
        return obj.toDict()

    def __unicode__(self):
        return self.name

#////////////////////////////////////////
#///        Participant class         ///
#////////////////////////////////////////
class Participant(BaseModel):
    objects = BaseModelManager()
    class Meta:
        unique_together = (("raceEvent", "bibNumber"),)
        ordering = ("bibNumber",)

    class MyMeta:
        indexField = "bibNumber"
        exclude_from_dict = ("created", "modified")
        extraFields = ()

    SEX_CHOICES = (
        (u'M', u'Male'),
        (u'F', u'Female'),
    )

    uuid = UUIDField(primary_key = True)
    raceEvent = models.ForeignKey("RaceEvent")
    bibNumber = models.IntegerField(db_index=True)
    firstName = models.CharField(max_length=200,db_index=True)
    firstNameSx = models.CharField(max_length=200,blank=True,null=True,db_index=True)
    lastName = models.CharField(max_length=200,db_index=True)
    lastNameSx = models.CharField(max_length=200,blank=True,null=True,db_index=True)
    home = models.CharField(max_length=200,null=True,blank=True)
    age = models.IntegerField(null=True,blank=True)
    sex = models.CharField(max_length=2, choices=SEX_CHOICES, null=True, blank=True)
    team = models.CharField(max_length=5, null=True, blank=True)
    dnfStation = models.IntegerField(blank=True,null=True)
    dnfReason = models.CharField(max_length=20, null=True, blank=True)

    @classmethod
    def loadDict(cls, inp):
        if inp.has_key("bibNumber") and inp.has_key("raceEvent_id"):
            evt = RaceEvent.objects.get(pk=inp["raceEvent_id"])
            obj, existed = cls.objects.get_or_create(bibNumber=inp["bibNumber"],raceEvent=evt)
        elif inp["uuid"] in (-1, "-1", None):
            obj = cls()
        else:
            obj = cls.objects.get(uuid=inp["uuid"])
        obj.fromDict(inp)
        return obj.toDict()

    def __unicode__(self):
        return u"%s: %s %s" % (self.bibNumber, self.firstName, self.lastName)

def ParticipantPreSave(sender, **kwargs):
    obj = kwargs["instance"]
    obj.firstNameSx = soundex(obj.firstName)
    obj.lastNameSx = soundex(obj.lastName)

models.signals.pre_save.connect(ParticipantPreSave, sender=Participant)

#////////////////////////////////////////
#///           Entry class            ///
#////////////////////////////////////////
class Entry(BaseModel):
    objects = BaseModelManager()
    class Meta:
        unique_together = (("participant", "station"),)

    class MyMeta:
        deep_copy = False
        exclude_from_dict = ("_participant_cache", "_station_cache", "created", "modified")
        indexField = "station__stationNumber"
        extraFields = ("station__stationNumber","participant__bibNumber", "station__name", "station__distance")

    uuid = UUIDField(primary_key = True)
    participant = models.ForeignKey("Participant")
    station = models.ForeignKey("Station")
    time_in = models.DateTimeField(null=True,blank=True,db_index=True)
    time_out = models.DateTimeField(null=True,blank=True,db_index=True)

    def __unicode__(self):
        return u"%s in at %s out at %s" % (self.participant, self.time_in, self.time_out)

    @classmethod
    def loadDict(cls, inp):
        if inp.has_key("bibNumber") and inp.has_key("stationNumber") and inp.has_key("raceEvent_id"):
            evt = RaceEvent.objects.get(pk=inp["raceEvent_id"])
            p = Participant.objects.get(bibNumber=inp["bibNumber"],raceEvent=evt)
            s = Station.objects.get(stationNumber=inp["stationNumber"],raceEvent=evt)
            find = cls.objects.filter(participant=p,station=s)
            if len(find):
                obj = find[0]
            else:
                obj = cls()
                obj.participant = p
                obj.station = s
        elif inp["uuid"] in (-1, "-1", None):
            obj = cls()
        else:
            obj = cls.objects.get(uuid=inp["uuid"])
        obj.fromDict(inp)
        return obj.toDict()

