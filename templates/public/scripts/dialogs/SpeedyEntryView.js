define(['base/BaseView', 'underscore', 'lib/keyProcessor', '_tpl!./jst/SpeedyEntry.jst',
       '_tpl!./jst/SpeedyEntryHelp.jst', 'lib/AlertDialog', 'ext/inputField'
    ], function(BaseView, _, keyProcessor, tpl, helpTpl, Alert) {

    var SpeedyEntryView = BaseView.extend({
        event: null,
        stationId: null,
        historyList: [],
        initialize: function(options) {
        },

        events: {
            "blur input.numeric"           : "numericBlur",
            "keyup input[name=station]"    : "stationKeyUp",
            "keyup input[name=runnerId]"   : "runnerKeyUp",
            "keyup input[name=stationIn]"  : "stationInKeyUp",
            "keyup input[name=stationOut]" : "stationOutKeyUp",
            "click .history_row .remove"   : "onRemoveClicked",
            "click .helpLink"              : "onHelpClicked"
        },

        render: function() {
            var self = this;
            var $el = $(self.el);
            $el.html(tpl({
                stationId: self.stationId,
                history:self.historyList,
                showTimeIn: self.showTimeIn,
                showTimeOut: self.showTimeOut
            }));

            if(self.stationId || self.stationId === 0) {
                // Station selected, focus runner number input
                setTimeout(function() {
                    self.$("input[name=runnerId]").focus();
                    console.log("Done it all");
                }, 200);

                self.$(".time").timeField({
                    allowNumeric: true,
                    allowAlpha: false,
                    transform: {".": ":"}
                });
                self.$("button").button();
            } else {
                // No station yet, focus on station num
                _.defer(function() {
                    self.$("input[name=station]").focus();
                });
            }
            return self;
        },

        refresh: function() {
            return this.render();
        },

        focus: function() {
            var self = this, $el = $(self.el);
            $el.parent().dialog("moveToTop");
            if (!self.stationId) {
                $el.find("input[name=station]").select().focus();
            } else if ($el.find("#input_row").hasClass("entering_time")){
                var t1e = $el.find("input[name=stationIn]"), t1=t1e.val();
                var t2e = $el.find("input[name=stationOut]"), t2=t2e.val();
                if ((this.showTimeOut && t1 && !t2) || !this.showTimeIn ) {
                    t2e.select().focus();
                } else {
                    t1e.select().focus();
                }
            } else {
                $el.find("input[name=runnerId]").select().focus();
            }

        },

        updateRowWithRunner: function(runner){
            var self = this;
            self.runner = runner;
            self.$("#input_row").addClass("entering_time");
            self.$("input[name=runnerId]").prop("disabled", true);
            self.$("#runner_name").html(runner.formatName());
            // Determine if we have entry data
            var times = runner.getTimes(self.stationId);
            if(times[0]){
                self.$("input[name=stationIn]").val(times[0] && times[0].toString("HH:mm:ss"));
            }
            if(times[1]){
                self.$("input[name=stationOut]").val(times[1] && times[1].toString("HH:mm:ss"));
            }
            if((times[0] && !times[1] && self.showTimeOut) || !self.showTimeIn) {
                _.defer(function() {
                    self.$("input[name=stationOut]").focus();
                });
            } else {
                _.defer(function() {
                    self.$("input[name=stationIn]").focus();
                });
            }
        },

        numericBlur: function(evt) {
            var $el = $(evt.currentTarget).closest("input.numeric");
            $el.val($el.val().replace(/[\D\s]/g,""));
        },

        stationKeyUp: function(evt) {
            var self = this;
            if (keyProcessor.keyDesc(evt) == "Enter") {
                $(evt.currentTarget).blur();
                self.trigger("stationSet", self.$("input[name=station]").val());
            }
        },

        runnerKeyUp: function(evt) {
            var self = this;
            if (keyProcessor.keyDesc(evt) == "Enter") {
                self.trigger("runnerSelected", self.$("input[name=runnerId]").val());
            }
        },

        stationInKeyUp: function(evt) {
            var self = this;
            var desc = keyProcessor.keyDesc(evt);
            if (desc == "Esc") {
                self.trigger("cancelRunner");
            } else if (keyProcessor.keyDesc(evt) == "Enter") {
                $(evt.currentTarget).blur();
                if (self.showTimeOut) {
                    _.defer(function() {
                        self.$("input[name=stationOut]").focus();
                    });
                } else {
                    self.submitCurrentData();
                }
            }
        },

        stationOutKeyUp: function(evt) {
            var self = this;
            if (keyProcessor.keyDesc(evt) == "Enter") {
                $(evt.currentTarget).blur();
                self.submitCurrentData();
            }
        },

        submitCurrentData: function() {
            var self = this;
            var data = {
                time_in: self.$("input[name=stationIn]").val(),
                time_out: self.$("input[name=stationOut]").val(),
                runner: self.runner,
                stationId: self.stationId
            };

            // After save, push runner onto the list
            self.trigger('saveRunnerToList', data);
        },

        onRemoveClicked: function(evt) {
            var id = $(evt.currentTarget).closest("li").attr("id").split("_").pop();
            this.trigger("revertEntry", id);
        },

        onHelpClicked: function(evt) {
            evt.preventDefault();
            Alert.alert("Help", helpTpl({}));
        }
    }, {
        registerHotkeyHandler: function(callback) {
            $(document).keydown(function(evt) {
                var code = keyProcessor.keyDesc(evt);

                if (code.indexOf("alt+") === 0) {
                    if (callback(code.substr(4))) {
                        evt.preventDefault();
                        evt.stopPropagation();
                    }
                }
            });
        }
    });

    return SpeedyEntryView;
});
