define(["backbone", "underscore"], function(Backbone, _) {

    var AlertDialog = Backbone.View.extend({
        dfdResult: null,
        initialize: function(options) {
            this.title = options.title;
            this.html = options.html;
            this.dfdResult = options.deferred;
        },

        render: function() {
            var self = this;
            var $el = $(this.el);
            $el.empty();
            var handlerCalled = false;

            var okClicked = function(evt) {
                evt.preventDefault();
                handlerCalled = true;
                self.dfdResult.resolve();
                $(self.el).dialog("close");
            };

            var buttons = {};
            buttons[AlertDialog.buttonText[0]] = okClicked;
            $el.dialog({
                title: this.title,
                autoOpen : true,
                modal: true,
                resizable: false,
                show: "fade",
                hide: "fade",
                minHeight: 40,
                closeOnEscape: true,
                buttons: buttons,
                beforeClose: function() {
                    if (!self.dfdResult.isRejected() && !self.dfdResult.isResolved()) {
                        self.dfdResult.resolve(false); // esc == no
                    }
                },
                close: function() {
                    $(self.el).dialog("destroy");
                },
                open: function(evt) {
                    $(evt.target.activeElement).find(".ui-dialog-titlebar-close").hide();
                }
            });
            $el.html(self.html);
            return this;
        }
    }, {
        alert: function(title, html) {
            var dfd = new $.Deferred();
            var confirmDialog = new this({title: title, html: html, deferred: dfd});
            confirmDialog.render();
            return dfd.promise();
        },
        buttonText: ["OK"]
    });

    return AlertDialog;
});
