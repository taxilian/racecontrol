define(["backbone", "underscore", "jquery", "defaults",
        "models/RaceEvent", "models/Participant", "models/Station", "models/Entry"],
        function(Backbone, _, $, defaults, RaceEvent, Participant, Station, Entry) {

    var Updater;

    function getTimeStamp() {
        return $.ajax({url: "/newAPI/clock"});
    }

    // We'll initialize this but keep a deferred in case we try to
    // create the updater a bit too early
    var startTime = getTimeStamp().pipe(function(time) {
        startTime = time;
    });

    Updater = Backbone.Model.extend({
        lastUpdateTime: null,

        initialize: function(options) {
            var self = this;
            self.eventId = options.eventId;
            $.when(startTime).done(function(time) {
                self.lastUpdateTime = startTime;
            });
        },

        getUpdates: function() {
            var self = this;
            var updateDfd = $.ajax({
                url: "/newAPI/events/" + self.eventId + "/update",
                data: {modified: self.lastUpdateTime}
            });

            updateDfd.done(function(res) {
                console.log("Processing updates");
                var entrysByParticipant = {};
                _.each(res.Entrys, function(entry) {
                    if (!entrysByParticipant[entry.participant_bibNumber]) {
                        entrysByParticipant[entry.participant_bibNumber] = [];
                    }
                    entrysByParticipant[entry.participant_bibNumber].push(entry);
                });
                $.when(Participant.list.fetchAll(self.eventId)).done(function(runnerlist) {
                    _.each(res.Participants, function(runner) {
                        var runnerObj = runnerlist.get(runner.bibNumber);
                        if (runnerObj) {
                            // If the runner is already in the backbone collection, update it
                            runnerObj.set(runner);
                        } else {
                            // Otherwise create a new participant model instance
                            runnerlist.add(runner);
                            runnerObj = runnerlist.get(runner.bibNumber);
                        }
                    });
                    _.each(entrysByParticipant, function(entrys, bibNumber) {
                        var entrysForRunner = Entry.list.parse(entrys);
                        var runnerObj = runnerlist.get(bibNumber);
                        if (!runnerObj) { return; }
                        if (runnerObj.has("entrys")) {
                            var entrylist = runnerObj.get("entrys");
                            _.each(entrysForRunner, function(e) {
                                var modelEntry = entrylist.get(e.station_stationNumber);
                                if (modelEntry) { // Update if exists, add if not
                                    modelEntry.set(e);
                                } else {
                                    entrylist.add(e);
                                }
                            });
                        } else {
                            var newEntrys = new Entry.list(entrysForRunner);
                            runnerObj.set({entrys: newEntrys});
                        }
                    });
                });
                $.when(Station.list.fetchAll(self.eventId)).done(function(stationlist) {
                    _.each(res.Stations, function(station) {
                        var stationObj = stationlist.get(station.stationNumber);
                        if (stationObj) {
                            stationObj.set(station);
                        } else {
                            stationlist.add(station);
                        }
                    });
                });
                _.each(res.RaceEvents, function(raceEvent) {
                    RaceEvent.obj.fetchById(raceEvent.uuid).done(function(raceEventObj) {
                        raceEventObj.set(raceEvent);
                    });
                });

                // Set the "last updated" timestamp so we don't get the same updates each time
                self.lastUpdateTime = res.lastUpdated;
            });
        }
    });

    return Updater;
});

