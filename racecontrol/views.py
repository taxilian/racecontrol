from zones import zoneDefault

def pageHandler(request, path):
    zone = zoneDefault()
    return zone.handleRequest(request, path)

