from django.db.models import Q
from ..models import *
from django.conf import settings
from django.shortcuts import redirect
from django.http import Http404
from pyzoop import zone, urlVars
from pyzoop import login_required, ReturnsJSON, AcceptsJSON
from django.contrib.auth.views import login, logout_then_login
from django.contrib.auth import authenticate, login, logout
import datetime, json, os
import os
from django import db
from soundex import soundex

_TIME_FORMAT = "%m %d %Y %H %M %S %f"

def loginRequired(target):
    def wrapper(self, request, path):
        if not request.user.is_authenticated():
            return self.error("Unauthenticated")
        else:
            return target(self, request, path)
    return wrapper

class zoneWebAPIEvents(zone):
    Debug = True
    def __init__(self):
        super(zoneWebAPIEvents,self).__init__()

    def initZone(self, request, pathList):
        pass

    @urlVars(exclude=("index",), vars=("eventId",))
    def initPages(self, request, path):
        pass

    def error(self, message):
        resp = Bag()
        resp.status = u'failed'
        resp.message = message
        return resp

    def success(self):
        resp = Bag()
        resp.status = u'success'
        return resp

    @ReturnsJSON
    def page_default(self, request, path):
        return self.error("Invalid API")

    @ReturnsJSON
    def page_events(self, request, path):
        if len(path) == 1:
            return self.page_getEventList(request, path[1:])
        elif len(path) == 2:
            return self.event_getFullData(request, path[1])
        elif hasattr(self, "event_%s" % path[2]):
            handler = getattr(self, "event_%s" % path[2])
            return handler(request, path[1], path[1:])
        else:
            return path

############################
# New and improved REST api
############################
    @ReturnsJSON
    def page_index(self, request, path):
        if "eventId" in request.zoneVar: # handle /webAPI/events with no path
            evtId = request.zoneVar["eventId"]
            evt = RaceEvent.objects.get(pk=evtId)
            return evt.toDict(False)
        else: # handle /webAPIEvents/<eventID>
            evtList = RaceEvent.objects.all()
            if (len(path) > 1):
                evtList = evtList.filter(pk=path[1])
            return evtList.toArray(False)

    @ReturnsJSON
    @loginRequired
    @AcceptsJSON
    def post_index(self, request, path, req):
        return self.put_index(request, path, req)

    @ReturnsJSON
    @loginRequired
    @AcceptsJSON
    def put_index(self, request, path, req):
        evt = req
        try:
            event["date"] = datetime.datetime.strptime(event["date"], "%Y-%m-%d %H:%M:%S")
            dbEvent, created = RaceEvent.objects.get_or_create(uuid=event["uuid"], date=event["date"])
            if "lastModifiedBy" not in event:
                event["lastModifiedBy"] = settings.STATION_UUID
            dbEvent.fromDict(event)
            db.connection._commit()
            return dbEvent.toDict()
        except:
            if self.Debug:
                raise
            return self.error("Malformed JSON update request")

    @ReturnsJSON
    @loginRequired
    def delete_index(self, request, path):
        if "eventId" in request.zoneVar: # handle /webAPI/events with no path
            uuid = request.zoneVar["eventId"]
            try:
                Entry.objects.filter(participant__raceEvent__pk=uuid).delete()
                Participant.objects.filter(raceEvent__pk=uuid).delete()
                Station.objects.filter(raceEvent__pk=uuid).delete()
                RaceEvent.objects.get(uuid=uuid).delete()
                db.connection._commit()
            except:
                raise
                return self.error("Unknown error")
            return self.success()
        else:
            raise Http404

    @ReturnsJSON
    def page_full(self, request, path):
        evtId = request.zoneVar["eventId"]
        evt = RaceEvent.objects.get(pk=evtId)
        updTime = datetime.datetime.now().strftime(_TIME_FORMAT)
        stations = Station.objects.filter(raceEvent=evt)
        participants = Participant.objects.filter(raceEvent=evt)
        entries = Entry.objects.select_related("participant__bibNumber", "station__stationNumber").filter(participant__in=participants,station__in=stations)
        pout = Bag(participants.toDict())
        for entry in entries:
            bNum = entry.participant.bibNumber
            sNum = entry.station.stationNumber
            if not pout[bNum].has_key("entrys"):
                pout[bNum]["entrys"] = Bag()
            pout[bNum]["entrys"][sNum] = entry.toDict()
        resp = Bag(evt.toDict())
        resp.stations = stations.toDict()
        resp.participants = pout
        resp.LastUpdated = updTime
        return resp


    @ReturnsJSON
    def page_stations(self, request, path):
        evtId = request.zoneVar["eventId"]
        resp = Station.objects.filter(raceEvent__uuid=evtId).toArray()
        return resp

