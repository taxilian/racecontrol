from racecontrol.models import *
import sqlite3
from Bag import Bag
import datetime

def importRunnerData(conn, raceEvent):
    c = conn.cursor()
    c.execute("select runnerno, first, last, age, sex, state, team from RunnerData")
    for row in c:
        runner = Participant()
        runner.raceEvent = raceEvent
        runner.bibNumber = row[0]
        runner.firstName = row[1]
        runner.lastName = row[2]
        runner.home = row[5]
        runner.sex = row[4]
        runner.age = row[3]
        runner.team = row[6]
        runner.save()

def importStations(conn, raceEvent):
    c = conn.cursor()
    c.execute("select distinct stationno from RunnerPosition")
    for row in c:
        station = Station()
        station.raceEvent = raceEvent
        station.stationNumber = row[0]
        station.name = "Station %s" % row[0]
        station.save()

def makeTime(time, date):
    if time == None or time == "":
        return None
    timeParts = time.split(":")
    hour = int(timeParts[0])
    minute = int(timeParts[1])
    second = 0
    if len(timeParts) > 2:
        second = int(timeParts[2])

    dateParts = date.split("-")
    year = int(dateParts[0])
    month = int( dateParts[1] )
    day = int( dateParts[2] )

    return datetime.datetime(year, month, day, hour, minute, second)

def getParticipant(bib, raceEvent):
    search = Participant.objects.filter(bibNumber=bib, raceEvent=raceEvent)
    if len(search) == 0:
        ret = Participant()
        ret.raceEvent = raceEvent
        ret.firstName = "Unknown"
        ret.lastName = "Participant"
        ret.bibNumber = bib
        ret.save()
        return ret
    else:
        return search[0]

def importPositions(conn, raceEvent):
    c = conn.cursor()
    c.execute("select stationno, runnerno, time_in, time_out from RunnerPosition")
    for row in c:
        participant = getParticipant(row[1], raceEvent)
        station = Station.objects.get(stationNumber=row[0],raceEvent=raceEvent)
        entry = Entry()
        entry.participant = participant
        entry.station = station
        entry.time_in = makeTime(row[2], raceEvent.date)
        entry.time_out = makeTime(row[3], raceEvent.date)
        entry.save()

def run():
    print "Enter sqlite db filename:",
    filename = raw_input(">")

    print "Enter the name of the race event:"
    raceevt = raw_input(">")

    print "Enter the date of the race event (YYYY-mm-dd):"
    racedate = raw_input(">")

    r = RaceEvent()
    r.date = racedate
    r.name = raceevt
    r.save()

    conn = sqlite3.connect(filename)
    importRunnerData(conn, r)
    importStations(conn, r)
    importPositions(conn, r)
