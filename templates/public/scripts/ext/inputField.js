define(["jquery", "underscore", "lib/keyProcessor", "./selectText"], function(jQuery, _, keyProcessor) {
    var lastTime;
    function getTime(str, addMinute) {
        var date = Date.parse(str);
        if (date === null) {
            return null;
        }
        date.setTime(date.getTime() + (addMinute * 60 * 1000));
        return date;
    }
    function processKeyPress(evt) {
        var char = keyProcessor.keyDesc(evt),
            curTime = new Date(), tmp, cnt, newTime,
            $el = $(evt.currentTarget).closest("input");
        switch(char) {
            case "+":
            case "shift++":
                tmp = $el.val();
                cnt = tmp.split(":");
                newTime = getTime(tmp, 1);
                if (!newTime) { return true; }
                if (cnt.length == 3) {
                    $el.val(newTime.toString("HH:mm:ss")).selectText(3,5);
                } else {
                    $el.val(newTime.toString("HH:mm")).selectText(3,2);
                }
                break;
            case "-":
                tmp = $el.val();
                cnt = tmp.split(":");
                newTime = getTime(tmp, -1);
                if (!newTime) { return true; }
                if (cnt.length == 3) {
                    $el.val(newTime.toString("HH:mm:ss")).selectText(3,5);
                } else {
                    $el.val(newTime.toString("HH:mm")).selectText(3,2);
                }
                break;
            case "shift+*":
                $el.selectText(3,5);
                break;
            case "/":
                if (lastTime) {
                    $el.val(lastTime.toString("HH:mm:ss"));
                    $el.selectText(3,5);
                }
                break;
            default:
                return false;
        }
        return true;
    }
    function processKeyDown(evt) {
        var char = keyProcessor.keyDesc(evt),
            curTime = new Date(), tmp, cnt, newTime,
            $el = $(evt.currentTarget).closest("input");
        switch(char) {
            case "T":
                $el.val(curTime.toString("HH:mm"));
                break;
            case "shift+T":
                $el.val(curTime.toString("HH:mm:ss"));
                break;
            case "A":
                $el.select();
                break;
            case "M":
                $el.selectText(3,5);
                break;
            case "H":
                $el.selectText(0,2);
                break;
            case "S":
                $el.selectText(6, 2);
                break;
            case "Del":
                $el.val("");
                break;
            case "R":
                if (lastTime) {
                    $el.val(lastTime.toString("HH:mm:ss"));
                    $el.selectText(3,5);
                }
                break;
            default:
                return false;
        }
    }
    function processBlur(evt) {
        var val = $(this).val(), tmp;
        if (val.indexOf(":") != -1)
            tmp = Date.parse(val);

        if (!tmp && val) {
            val = val.replace(/[\D\s]/g,"");
            if (val.length == 3) {
                val = val.substring(0, 1) + ":" + val.substring(1,3);
            } else if (val.length == 4) {
                val = val.substring(0, 2) + ":" + val.substring(2,4);
            } else if (val.length == 5) {
                val = val.substring(0, 1) + ":" + val.substring(1,3) + ":" + val.substring(3,5);
            } else if (val.length == 6) {
                val = val.substring(0, 2) + ":" + val.substring(2,4) + ":" + val.substring(4,6);
            } else {
                val = "";
            }
            tmp = Date.parse(val);
        }
        if (tmp) {
            lastTime = tmp;
            $(this).val(tmp.toString("HH:mm:ss"));
        } else if (val) {
            notify.warn("Invalid time");
            $(this).select().focus();
        }
    }

    jQuery.fn.timeField = function(options) {
        $(this).each(function(i) {
            if ($(this).hasClass("isTimeField")) {
                return;
            }
            var field = this;
            var settings = jQuery.extend({
                allowNumeric: true,
                allowAlpha: true,
                capitalize: false,
                transform: {},
                keyTrigger: null,
                allowSpecialKeys: true,
                allowChars: "",
                disallowChars: ""
            }, options);

            $(this).addClass("isTimeField");

            $(this).keypress(function(evt) {
                if (processKeyPress(evt)) {
                    evt.preventDefault();
                }
            });
            $(this).keydown(function(evt) {
                if (processKeyDown(evt)) {
                    evt.preventDefault();
                }
            });
            $(this).blur(processBlur);

            var isAlpha = function(str) {
                var filter = /[A-Za-z]/;
                return filter.test(str.substring(0,1));
            };
            var isNumeric = function(str) {
                var filter = /[0-9.]/;
                return filter.test(str.substring(0,1));
            };

            var changeKey = function(evt, newKey) {
                if (field.createTextRange) {
                    window.event.keyCode = newKey.charCodeAt(0);
                } else {
                    var startpos = field.selectionStart;
                    var endpos = field.selectionEnd;
                    field.value = field.value.substr(0, startpos) + newKey + field.value.substr(endpos);
                    field.setSelectionRange(startpos + 1, startpos + 1);
                    evt.preventDefault();
                    return false;
                }
            };

            $(this).keypress(function(evt) {
                // Don't catch special chars
                var code = evt.which;
                var char = String.fromCharCode(code);
                if (settings.allowChars.indexOf(char) == -1) {
                    if (!settings.allowAlpha && isAlpha(char)) {
                        evt.preventDefault();
                    } else if (!settings.allowNumeric && isNumeric(char)) {
                        evt.preventDefault();
                    } else if (settings.disallowChars.indexOf(char) !== -1) {
                        evt.preventDefault();
                    }
                }
                var newChar = char;
                if (settings.transform[char]) {
                    newChar = settings.transform[char];
                }
                if (settings.capitalize) {
                    newChar = newChar.toUpperCase();
                }
                if (newChar != char) {
                    changeKey(evt, newChar);
                }
            });
        });
        return this;
    };
    return $;
});
