define(['base/BaseView', 'underscore',
        '_tpl!./jst/AddEditStation.jst'], function(BaseView, _, tpl) {

    var AddEditStationView = BaseView.extend({
        event: null,

        events: {
        },

        render: function() {
            var self = this;
            var $el = $(self.el);
            $el.html(tpl({stationId: self.stationId, station: self.station}));
            return self;
        },

        setStation: function(stationId, station) {
            this.stationId = stationId;
            this.station = station;
        },

        setRunner: function(runner) {
            this.runner = runner;
        },

        onSaveClicked: function() {
            var self = this;
            var fieldValues = {};

            _.each(["name", "distance"], function(field) {
                var match = "[name=fld_" + field + "]";
                var f = self.$("input" + match + ",select" + match);
                fieldValues[field] = f.val();
            });

            this.trigger("save", fieldValues);
        }
    });

    return AddEditStationView;
});


